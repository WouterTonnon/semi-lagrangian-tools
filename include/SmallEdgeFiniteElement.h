/*
Copyright 2021 W.R. Tonnon

This file is part of Semi-Lagrangian Tools
An implementation of discrete differential forms based on work by Francesca
Rapetti and Alain Bossavit.

Copyright (C) <2021>  <W.R. Tonnon>

Semi-Lagrangian Tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Semi-Lagrangian Tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SEMILAGRANGE0FORMS_SMALLEDGEFINITEELEMENT_H
#define SEMILAGRANGE0FORMS_SMALLEDGEFINITEELEMENT_H

#include <mfem.hpp>
#include <numeric>

/// A function to compute all possibilities of putting NumBalls balls in NumBoxes boxes.
/// \param NumBalls input: Number of balls.
/// \param NumBoxes input: Number of boxes.
/// \param data output: all possibilities of putting NumBalls balls in NumBoxes boxes.
void GetPermutations(int NumBalls, int NumBoxes, std::vector<std::vector<int>>& data);

/// A function to compute the factorial of n
/// \param n argument of factorial
/// \return factorial of n
int factorial(int n);

mfem::Vector cross(const mfem::Vector& A, const mfem::Vector& B);

/// \brief A finite element implementation based on small edges/simplices.

/// Based on "Whitney Forms of Higher Degree" by F. Rapetti and A. Bossavit.
template<mfem::Geometry::Type geom>
class Whitney_TriangleElement : public mfem::VectorFiniteElement
{
    // Higher degree elements are constructed from lower degree elements
    mfem::ND_TriangleElement w1_2d;
    mfem::H1_TriangleElement w0_2d;
    mfem::ND_TetrahedronElement w1_3d;
    mfem::H1_TetrahedronElement w0_3d;

    // k associated with k-maps
    int k;

    // Order
    int p;

    // Number of vertices of the element
    int m;

    // Store the precomputed values for indexing
    std::vector<std::vector<int>> indices;
    std::vector<std::vector<int>> rots;
    bool precomputedIndAndRot = false;

    // List of k-integers representing k-maps
    std::vector<std::vector<int>> permutations;

    // List of DoFs (edges or 1-simplices)
    std::vector<std::vector<mfem::Vector>> dofs;

    // Maps vector representation to integrals over DoFs
    mfem::DenseMatrix DofsToEdge;

    // Maps integrals over DoFs to vector representation for big edges for second-order
    mfem::DenseMatrix EdgeToDofs_edge;

    // Maps integrals over DoFs to vector representation for faces for second-order
    mfem::DenseMatrix EdgeToDofs_face;

    // Maps integrals over DoFs to vector representation for faces for second-order
    mfem::DenseMatrix DofsEdgeToFace;

    // All vertices of the element
    std::vector<mfem::Vector> vertices;

    // List of edges of the element (defined by end points)
    std::vector<std::vector<mfem::Vector>> edges;

    // List of faces of the element (defined by vertices0
    std::vector<std::vector<mfem::Vector>> faces;

    // Numbers DoFs associated with the (interior of the tetrahedron). InnerDofs.at(i).at(j) gives the number of the
    // DoF generated from edge i and k-map j
    std::vector<std::vector<int>> InnerDofs;

public:
    /// Constructor
    Whitney_TriangleElement(
            const int p ///< Order of the element.
            )
    : VectorFiniteElement(mfem::Geometry::Constants<geom>::Dimension,
                          geom,
                          3*(mfem::Geometry::Constants<geom>::Dimension-1)*factorial(p+mfem::Geometry::Constants<geom>::Dimension-1)/(factorial(mfem::Geometry::Constants<geom>::Dimension)*factorial(p-1)),
                          p,
                          H_CURL,
                          mfem::FunctionSpace::Pk),
                          w1_2d(1),
                          w0_2d(1),
                          w1_3d(1),
                          w0_3d(1),
                          k(p-1),
                          p(p),
                          vertices(0),
                          edges(0)
    {
        // Define number of vertices
        m = GetDim()+1;

        // Get all possible k-maps
        GetPermutations(k,m,permutations);

        // Store all vertices
        mfem::Geometry geometry;
        const mfem::IntegrationRule *ir = geometry.GetVertices(geom);
        for(int i=0; i<mfem::Geometry::NumVerts[geom]; ++i)
        {
            mfem::Vector v(dim);
            v.Elem(0) = ir->IntPoint(i).x;
            v.Elem(1) = ir->IntPoint(i).y;
            if(dim==3) v.Elem(2) = ir->IntPoint(i).z;
            vertices.push_back(v);
        }

        // Store all edges
        auto edge_vertices = mfem::Geometry::Constants<geom>::Edges;
        for(int i=0; i<mfem::Geometry::NumEdges[geom]; ++i)
        {
            std::vector<mfem::Vector> edge;
            edge.push_back(vertices.at(edge_vertices[i][0]));
            edge.push_back(vertices.at(edge_vertices[i][1]));
            edges.push_back(edge);
        }

        // Store all faces
        if(dim==3) {
            auto face_vertices = mfem::Geometry::Constants<geom>::FaceVert;
            for (int i = 0; i < mfem::Geometry::NumFaces[geom]; ++i) {
                std::vector<mfem::Vector> face;
                face.push_back(vertices.at(face_vertices[i][0]));
                face.push_back(vertices.at(face_vertices[i][1]));
                face.push_back(vertices.at(face_vertices[i][2]));
                faces.push_back(face);
            }
        }

        // Sanity check
        if(permutations.size()*edges.size()!= GetDof()) mfem::mfem_error("Whitney_TriangleElement::Whitney_TriangleElement(): GetDof() does not work.");

        // Number DoFs associated with the (interior of the) tetrahedron
        if(GetDim()==3) {

            // Set size to match number of edges
            InnerDofs.resize(mfem::Geometry::Constants<geom>::NumEdges);

            int num = 0;
            // Loop over all edges
            for(int u=0; u<edges.size(); ++u) {
                // Find vertex indices associated with edge
                int v0, v1;
                v0 = mfem::Geometry::Constants<geom>::Edges[u][0];
                v1 = mfem::Geometry::Constants<geom>::Edges[u][1];

                // Set size inner array to match number of k-maps
                InnerDofs.at(u).resize(permutations.size(), 0);

                // Loop over all k-maps
                for (int q = 0; q < permutations.size(); ++q) {
                    bool MapsToFace = false;

                    // Loop over all faces
                    for (int i = 0; i < mfem::Geometry::NumFaces[geom]; ++i) {
                        std::vector<int> face_vert(0);
                        std::vector<int> found(3, 0);

                        // Check which edge vertices are also vertices of the considered face
                        for (int j = 0; j < 3; ++j) {
                            face_vert.push_back(mfem::Geometry::Constants<mfem::Geometry::TETRAHEDRON>::FaceVert[i][j]);
                            if(face_vert.at(j)==v0 | face_vert.at(j)==v1){
                                found.at(j) = 1;
                            }
                        }

                        // Check if both edge vertices are also vertices of the considered face => edge must be an edge
                        // of the face
                        if(std::accumulate(found.begin(),found.end(),0)==2) {
                            // Sum all k-map integers corresponding to the vertices of the face
                            int sum = 0;
                            for (int j = 0; j < 3; ++j) sum += permutations.at(q).at(face_vert.at(j));

                            // if all the k-map integers corresponding to the vertices are zero, the DoF is on a face
                            if (k > 0 & sum == k) MapsToFace = true;
                        }
                    }

                    // Only if the DoF is not on a face (and thus not on an edge), do we need to enumerate.
                    if (!MapsToFace) {
                        InnerDofs.at(u).at(q) = num;
                        num++;
                    }
                }
            }
        }

        // Find DoFs
        FindDofs(dofs);

        // Find projection matrix
        DofsToEdge.SetSize(GetDof(),GetDof());
        FindProjectionmatrix(DofsToEdge);

        // Save Projection Matrices for second-order small edges manually
        EdgeToDofs_edge.SetSize(2,2);
        EdgeToDofs_edge.Elem(0,0) = 3.;
        EdgeToDofs_edge.Elem(0,1) = -1.;
        EdgeToDofs_edge.Elem(1,0) = -1.;
        EdgeToDofs_edge.Elem(1,1) = 3.;

        EdgeToDofs_face.SetSize(3,3);
        EdgeToDofs_face.Elem(0,0) =  3.555555555555556;
        EdgeToDofs_face.Elem(0,1) = -1.777777777777778;
        EdgeToDofs_face.Elem(0,2) = -1.777777777777778;
        EdgeToDofs_face.Elem(1,0) = -1.777777777777778;
        EdgeToDofs_face.Elem(1,1) =  3.555555555555556;
        EdgeToDofs_face.Elem(1,2) = -1.777777777777778;
        EdgeToDofs_face.Elem(2,0) = -1.777777777777778;
        EdgeToDofs_face.Elem(2,1) = -1.777777777777778;
        EdgeToDofs_face.Elem(2,2) =  3.555555555555556;

        int num_edge_dofs, num_face_dofs;
        if(geom == mfem::Geometry::TETRAHEDRON){
            num_edge_dofs = 2*6;
            num_face_dofs = 3*4;
        }
        else if(geom == mfem::Geometry::TRIANGLE) {
            num_edge_dofs = 2*3;
            num_face_dofs = 3*1;
        }

        DofsEdgeToFace.SetSize(num_face_dofs,num_edge_dofs);
        for(int i=0; i<num_face_dofs; ++i)
            for(int j=0; j<num_edge_dofs; ++j)
                DofsEdgeToFace.Elem(i,j) = DofsToEdge.Elem(i+num_edge_dofs,j);

        // Precompute the DoF to index mapping
        PrecomputeIndAndRot();

    };

    /// Compute the set of degrees of freedom.
    /// \return the set of degrees of freedom (edge), each represented as a vector of two points in space
    std::vector<std::vector<mfem::Vector>> GetDofs() {return dofs;};

    /// \brief Returns the pseudoinverse of the matrix returned by GetEdgeToDofs().

    /// In other words, returns the matrix that maps a vector with tangential values (associated to the edges that represent the DoFs) to a vector with values on those DoFs that realize these average values as close as possible.
    /// \return Pseudoinverse of the matrix returned by GetEdgeToDofs().
    mfem::DenseMatrix* GetDofsToEdge() {return &DofsToEdge; };

    /// Returns k as defined by Rapetti and Bossavit.
    /// \return k as defined by Rapetti and Bossavit
    int GetK(){return k;};

    /// Precompute the indices associated with DoFs, and the orientation of small edges for gluing elements together.
    void PrecomputeIndAndRot();

    /// Get the index associated with a DoF, and the orientation of small edges for gluing elements together. Recall that
    /// DoFs are associated to an edge and a permutation of a k-map (Rapetti and Bossavit).
    /// \param edge input: index of the edge associated with the DoF.
    /// \param perm input: index of the permutation of the k-map associated with the DoF.
    /// \param rot output: Orientation of the DoF. (1 means non-inverted, -1 means inverted).
    /// \return local index.
    int GetIndAndRot(int edge, int perm, int& rot) const;

    /// Perform a local projection based on the given mfem::VectorCoefficient. Outputs an error, since small-edges-based
    /// finite-element spaces are ill-defined, and thus need a global least-squares solution. This means that there is
    /// no such thing as local projection.
    /// \param vc input: Representation of the function to be projected.
    /// \param Trans input: The isogeometric transformation associated with the element.
    /// \param dofs output: Vector with values associated to the DoFs, following the indexing defined by GetIndAndRot().
    virtual void Project(mfem::VectorCoefficient &vc,
                         mfem::ElementTransformation &Trans, mfem::Vector &dofs) const;

    /// Compute the degrees of freedom (small edges), each represented as a vector of two points in space
    /// \param dofs output: the set of degrees of freedom (edge), each represented as a vector of two points in space
    virtual void FindDofs(std::vector<std::vector<mfem::Vector>>& dofs);

    /// Compute the matrix that maps a vector with values of the DoFs to the tangential values associated with the edges that represent these DoFs.
    /// \param EdgeToDofs the matrix that maps a vector with values of the DoFs to the tangential values associated with the edges that represent these DoFs.
    virtual void FindProjectionmatrix(mfem::DenseMatrix& EdgeToDofs);

    /// Compute the values of the local shape functions at point ip.
    /// \param ip point in space where the local shape functions need to be evaluated.
    /// \param shape #Dofs x Dim Matrix with in each row the vector associated with the corresponding local shape function.
    virtual void CalcVShape(const mfem::IntegrationPoint &ip,
                            mfem::DenseMatrix &shape) const override;

    /// Compute the values of the local shape functions at a point described by Trans.
    /// \param Trans Describes point in space where the local shape functions need to be evaluated.
    /// \param shape #Dofs x Dim Matrix with in each row the vector associated with the corresponding local shape function.
    virtual void CalcVShape(mfem::ElementTransformation &Trans,
                            mfem::DenseMatrix &shape) const override;

    /// Compute the values of the curl of the local shape functions at point ip.
    /// \param ip point in space where the curl of the local shape functions need to be evaluated.
    /// \param shape Matrix with in each row the vector associated with the curl of the corresponding local shape function. Shape has #Dofs rows and 3 or 1 columns in dimension 3 and 2, respectively.
    virtual void CalcCurlShape(const mfem::IntegrationPoint &ip,
                               mfem::DenseMatrix &curl_shape) const override;

    virtual void TransformAverageToBasis(const mfem::Vector& in, mfem::Vector& out) const;
};

/// A FiniteElementCollection using Whitney Elements based on small edges.
class WE_FECollection : public mfem::FiniteElementCollection
{
    mfem::FiniteElement * WE_Elements[mfem::Geometry::NumGeom];
    int WE_dof[mfem::Geometry::NumGeom];
    int *SegDofOrd[2], *TriDofOrd[6];

public:
    /// Constructor
    /// \param p Order of the finite element space
    /// \param dim Dimension
    WE_FECollection(const int p, const int dim)
    : FiniteElementCollection(p)
    {

        if(!(dim==2 | dim==3)) mfem::mfem_error("WE_FECollection(): Currently only implemented for 2D and 3D triangular meshes. ");

        // Initialize WE_Elements
        for(int i=0; i<mfem::Geometry::NumGeom; ++i) WE_Elements[i] = NULL;
        WE_Elements[mfem::Geometry::SEGMENT] = new mfem::ND_SegmentElement(p);
        WE_Elements[mfem::Geometry::TRIANGLE] = new Whitney_TriangleElement<mfem::Geometry::TRIANGLE>(p);
        WE_Elements[mfem::Geometry::TETRAHEDRON] = new Whitney_TriangleElement<mfem::Geometry::TETRAHEDRON>(p);

        // Initialize WE_dof
        for(int i=0; i<mfem::Geometry::NumGeom; ++i) WE_dof[i] = 0;
        WE_dof[mfem::Geometry::SEGMENT] = p;
        int z = 0;
        for(int i=0; i<p-1; ++i) z += i+1;
        WE_dof[mfem::Geometry::TRIANGLE] = z*3;
        WE_dof[mfem::Geometry::TETRAHEDRON] = WE_Elements[mfem::Geometry::TETRAHEDRON]->GetDof()-6*WE_dof[mfem::Geometry::SEGMENT] - 4*WE_dof[mfem::Geometry::TRIANGLE];


        // Initialize orientation for gluing together elements
        SegDofOrd[0] = new int[2*p];
        SegDofOrd[1] = SegDofOrd[0] + p;
        for (int i = 0; i < p; i++)
        {
            SegDofOrd[0][i] = i;
            SegDofOrd[1][i] = -1 - (p-1 - i);
        }

        int TriDof = WE_dof[mfem::Geometry::TRIANGLE];
        TriDofOrd[0] = new int[6 * TriDof];
        for (int i = 1; i < 6; i++) {
            TriDofOrd[i] = TriDofOrd[i - 1] + TriDof;
        }

        // see Mesh::GetTriOrientation in mesh/mesh.cpp,
        // the constructor of H1_FECollection
        if(p==1 & dim==3)
        {
            // Nothing to do
        }
        else if(p==2 & dim==3)
        {
            TriDofOrd[0][0] = 0;
            TriDofOrd[0][1] = 1;
            TriDofOrd[0][2] = 2;

            TriDofOrd[5][0] = -3;
            TriDofOrd[5][1] = -2;
            TriDofOrd[5][2] = -1;
        }
        else if(dim==3){
            mfem::mfem_error("WE_FECollection(): Indexing for gluing together elements for orders 3 and higher not implemented in 3D.");
        }
    }

    /// Destructor
    ~WE_FECollection(){
        delete SegDofOrd[0];
        delete TriDofOrd[0];
        for(int i=0; i<mfem::Geometry::NumGeom; ++i)
            if(WE_Elements[i]) delete WE_Elements[i];
    }

    /// Returns the finite element associated with the given geometry type.
    /// \param GeomType Geometry type
    /// \return Finite element type associated with an element of geometry type GeomType.
    virtual const mfem::FiniteElement *FiniteElementForGeometry(mfem::Geometry::Type GeomType) const{
        if(GeomType!=mfem::Geometry::TETRAHEDRON & GeomType!=mfem::Geometry::TRIANGLE & GeomType != mfem::Geometry::SEGMENT){
            mfem::mfem_error("WE_FECollection::FiniteElementForGeometry(): Only triangles and tetrahedra supported");
        }
        return WE_Elements[GeomType];
    };

    /// Returns the number of DoFs associated with the given geometry type.
    /// \param GeomType Geometry type
    /// \return Number of DoFs associated with an element of geometry type GeomType.
    virtual int DofForGeometry(mfem::Geometry::Type	GeomType) const{
        return WE_dof[GeomType];
    };

    /// Returns the appropriate information needed for gluing elements.
    /// \param GeomType geometry type
    /// \param Or Orientation of the element.
    /// \return appropriate local index.
    const int* DofOrderForOrientation(mfem::Geometry::Type GeomType, int Or) const {
        if (GeomType == mfem::Geometry::SEGMENT) {
            return (Or > 0) ? SegDofOrd[0] : SegDofOrd[1];
        }
        else if (GeomType == mfem::Geometry::TRIANGLE)
        {
            if (Or != 0 && Or != 5)
            {
                MFEM_ABORT("triangle face orientation " << Or << " is not supported! "
                                "Use Mesh::ReorientTetMesh to fix it.");
            }
            return TriDofOrd[Or%6];
        }else
        {
            mfem::mfem_error("WE_FECollection::DofOrderForOrientation(): Not impelmented for the provided geometry type.");
            return 0;
        }
    }

    /// Returns the type of conformity. In this case we have tangential continuity.
    /// \return Return a tangential indicator.
    virtual int GetContType() const
    {
        return mfem::FiniteElementCollection::TANGENTIAL;
    };


};



#endif //SEMILAGRANGE0FORMS_SMALLEDGEFINITEELEMENT_H
