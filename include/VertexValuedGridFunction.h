/*
Copyright 2021 W.R. Tonnon

This file is part of Semi-Lagrangian Tools
Definition of smoothening discrete differential forms to make them
pointwise-defined.

Copyright (C) <2021>  <W.R. Tonnon>

Semi-Lagrangian Tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Semi-Lagrangian Tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SEMILAGRANGE0FORMS_PointWiseGridFunction_H
#define SEMILAGRANGE0FORMS_PointWiseGridFunction_H

#include <mfem.hpp>
#include "TraceBackMesh.h"
#include "Parameters.h"

/// A class that extends mfem::GridFunction to allow for pointwise evaluation despite the discontinuous nature of finite-
/// element spaces for 1-forms. (For example, by averaging over a circle with small radius around a point)
class PointWiseGridFunction : public mfem::GridFunction
{
protected:
    PhysicalParameters param;
    const std::function<void(const mfem::Vector&, mfem::Vector&)>& DirichletBoundaryCondition;

public:

    PointWiseGridFunction(const PointWiseGridFunction& orig, PhysicalParameters param)
    : GridFunction(orig), DirichletBoundaryCondition(orig.DirichletBoundaryCondition), param(param)
    {};

    PointWiseGridFunction(
            const std::function<void(const mfem::Vector&, mfem::Vector&)>& DirichletBoundaryCondition,
            PhysicalParameters param
            )
            : GridFunction(), DirichletBoundaryCondition(DirichletBoundaryCondition), param(param)
    {}

    PointWiseGridFunction(const GridFunction &orig,
                          const std::function<void(const mfem::Vector&, mfem::Vector&)>& DirichletBoundaryCondition,
                          PhysicalParameters param
    )
            : GridFunction(orig), DirichletBoundaryCondition(DirichletBoundaryCondition), param(param)
    {}

    PointWiseGridFunction (mfem::FiniteElementSpace *f,
                              const std::function<void(const mfem::Vector&, mfem::Vector&)>& DirichletBoundaryCondition,
                           PhysicalParameters param
    )
            : GridFunction(f), DirichletBoundaryCondition(DirichletBoundaryCondition), param(param)
    {}

    PointWiseGridFunction (mfem::FiniteElementSpace *f,
                              double *data,
                              const std::function<void(const mfem::Vector&, mfem::Vector&)>& DirichletBoundaryCondition,
                           PhysicalParameters param
    )
            : GridFunction(f,data), DirichletBoundaryCondition(DirichletBoundaryCondition), param(param)
    {}

    PointWiseGridFunction (mfem::Mesh *m,
                           std::istream &input,
                              const std::function<void(const mfem::Vector&, mfem::Vector&)>& DirichletBoundaryCondition,
                           PhysicalParameters param
    )
            : GridFunction(m,input), DirichletBoundaryCondition(DirichletBoundaryCondition), param(param)
    {}

    PointWiseGridFunction (mfem::Mesh *m, GridFunction *gf_array[],
                           int num_pieces,
                              const std::function<void(const mfem::Vector&, mfem::Vector&)>& DirichletBoundaryCondition,
                           PhysicalParameters param
    )
            : GridFunction(m, gf_array, num_pieces), DirichletBoundaryCondition(DirichletBoundaryCondition), param(param)
    {}

    /// Evaluate GridFunction at vertex
    /// \param i vertex index
    /// \param tol tolerance
    /// \return vector value at vertex
    virtual mfem::Vector evalAtVertex(int i, double tol = 1e-12) = 0;

    /// Evaluate GridFunction at point in space
    /// \param x point in space
    /// \param ind index element close to point in space
    /// \param tol tolerance
    /// \param vertex obsolete
    /// \return vector value at vertex
    virtual mfem::Vector evalPointWise(mfem::Vector x, int ind, double tol = 1e-12, bool vertex = false);

};

/// Evaluates GridFunction at arbitrary points in the mesh by averaging the individual components of the vector.
/// The x-component is averaged over a line in x-direction with length equal to the meshwidth, the y- and z-component
/// are obtained similarly. This leads to a Lipschitz continuous function.
class SmoothenedPointWiseGridFunction : public PointWiseGridFunction
{
protected:
    TraceBackMesh mesh;
    mfem::Table *vtoel = NULL;
    double h_max = -1;
    int order;
public:
    SmoothenedPointWiseGridFunction(const SmoothenedPointWiseGridFunction& orig, PhysicalParameters param, mfem::Mesh mesh)
            : PointWiseGridFunction(orig, param), mesh(mesh,false)
    {};

    SmoothenedPointWiseGridFunction(
            const std::function<void(const mfem::Vector&, mfem::Vector&)>& DirichletBoundaryCondition,
            PhysicalParameters param,
            mfem::Mesh mesh
    )
            : PointWiseGridFunction(DirichletBoundaryCondition,param), mesh(mesh,false)
    {
        double h_min;
        double kappa_min;
        double kappa_max;
        mesh.GetCharacteristics(h_min,h_max,kappa_min,kappa_max);
    }

    SmoothenedPointWiseGridFunction(const GridFunction &orig,
                          const std::function<void(const mfem::Vector&, mfem::Vector&)>& DirichletBoundaryCondition,
                                    PhysicalParameters param,
                                    mfem::Mesh mesh
    )
            : PointWiseGridFunction(orig, DirichletBoundaryCondition, param), mesh(mesh,false)
            {
                double h_min;
                double kappa_min;
                double kappa_max;
                mesh.GetCharacteristics(h_min,h_max,kappa_min,kappa_max);
            }

    SmoothenedPointWiseGridFunction (mfem::FiniteElementSpace *f,
    const std::function<void(const mfem::Vector&, mfem::Vector&)>& DirichletBoundaryCondition,
                                     PhysicalParameters param,
                                     mfem::Mesh *mesh,
                                     int order = 1
    )
    : PointWiseGridFunction(f, DirichletBoundaryCondition, param), mesh(*mesh,false), order(order)
    {
        double h_min;
        double kappa_min;
        double kappa_max;
        mesh->GetCharacteristics(h_min,h_max,kappa_min,kappa_max);
    }

    SmoothenedPointWiseGridFunction (mfem::FiniteElementSpace *f,
    double *data,
    const std::function<void(const mfem::Vector&, mfem::Vector&)>& DirichletBoundaryCondition,
                                     PhysicalParameters param,
                                     mfem::Mesh *mesh
    )
    : PointWiseGridFunction(f,data, DirichletBoundaryCondition, param), mesh(*mesh,false)
            {
                double h_min;
                double kappa_min;
                double kappa_max;
                mesh->GetCharacteristics(h_min,h_max,kappa_min,kappa_max);
            }

    SmoothenedPointWiseGridFunction (mfem::Mesh *m,
    std::istream &input,
    const std::function<void(const mfem::Vector&, mfem::Vector&)>& DirichletBoundaryCondition,
                                     PhysicalParameters param,
                                     mfem::Mesh *mesh
    )
    : PointWiseGridFunction(m,input, DirichletBoundaryCondition, param), mesh(*mesh,false)
            {
                double h_min;
                double kappa_min;
                double kappa_max;
                mesh->GetCharacteristics(h_min,h_max,kappa_min,kappa_max);
            }

    SmoothenedPointWiseGridFunction (mfem::Mesh *m, GridFunction *gf_array[],
    int num_pieces,
    const std::function<void(const mfem::Vector&, mfem::Vector&)>& DirichletBoundaryCondition,
                                     PhysicalParameters param,
                                     mfem::Mesh *mesh
    )
    : PointWiseGridFunction(m, gf_array, num_pieces, DirichletBoundaryCondition, param), mesh(*mesh,false)
            {
                double h_min;
                double kappa_min;
                double kappa_max;
                mesh->GetCharacteristics(h_min,h_max,kappa_min,kappa_max);
            }

    ~SmoothenedPointWiseGridFunction()
    {
        if(vtoel) delete vtoel;
    }

    mfem::Vector evalAtVertex(int i, double tol) override;

    mfem::Vector evalPointWise(mfem::Vector x, int ind, double tol, bool vertex = false) override;
};

/// Evaluated GridFunction by finding the element to which the physical point in space belongs, then the polynomial
/// representation within this element is used for evaluation. Note that this method assumes that the point in space
/// belongs to a unique elements. This is not the case if you consider edges and vertices, but could give a sufficiently
/// accurate approximation.
class RandomPointWiseGridFunction : public PointWiseGridFunction
{
protected:
    TraceBackMesh mesh;
    mfem::Table *vtoel = NULL;
public:
    RandomPointWiseGridFunction(const SmoothenedPointWiseGridFunction& orig, PhysicalParameters param, mfem::Mesh mesh)
            : PointWiseGridFunction(orig, param), mesh(mesh,false)
    {};

    RandomPointWiseGridFunction(
            const std::function<void(const mfem::Vector&, mfem::Vector&)>& DirichletBoundaryCondition,
            PhysicalParameters param,
            mfem::Mesh mesh
    )
            : PointWiseGridFunction(DirichletBoundaryCondition,param), mesh(mesh,false)
    {
    }

    RandomPointWiseGridFunction(const GridFunction &orig,
                                    const std::function<void(const mfem::Vector&, mfem::Vector&)>& DirichletBoundaryCondition,
                                    PhysicalParameters param,
                                    mfem::Mesh mesh
    )
            : PointWiseGridFunction(orig, DirichletBoundaryCondition, param), mesh(mesh,false)
    {
    }

    RandomPointWiseGridFunction (mfem::FiniteElementSpace *f,
                                     const std::function<void(const mfem::Vector&, mfem::Vector&)>& DirichletBoundaryCondition,
                                     PhysicalParameters param,
                                     mfem::Mesh *mesh,
                                     int order = 1
    )
            : PointWiseGridFunction(f, DirichletBoundaryCondition, param), mesh(*mesh,false)
    {
    }


    ~RandomPointWiseGridFunction()
    {
        if(vtoel) delete vtoel;
    }

    mfem::Vector evalAtVertex(int i, double tol) override;

    mfem::Vector evalPointWise(mfem::Vector x, int ind, double tol, bool vertex = false) override;
};


#endif //SEMILAGRANGE0FORMS_PointWiseGridFunction_H
