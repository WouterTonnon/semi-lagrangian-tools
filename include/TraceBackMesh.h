/*
Copyright 2021 W.R. Tonnon

This file is part of Semi-Lagrangian Tools
Mesh class that allows for splitting lines through the domain into parts that are
contained in single mesh elements.

Copyright (C) <2021>  <W.R. Tonnon>

Semi-Lagrangian Tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Semi-Lagrangian Tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



#ifndef SEMILAGRANGE0FORMS_TRACEBACKMESH_H
#define SEMILAGRANGE0FORMS_TRACEBACKMESH_H

#include "mfem.hpp"

/// A mesh class with extended capabilities compared to mfem::Mesh.
class TraceBackMesh : public mfem::Mesh
{
protected:
    std::vector<std::vector<int>> EdgeToElement;
    double mesh_width;
    double h_min;
    mfem::Table* fac_to_elem=NULL;
    mfem::Table* vtoel = NULL;
    mfem::Table* ftoel = NULL;
public:
    /// Constructor
    /// \param mesh Copy the structure of this mesh.
    /// \param copy_nodes if true, make a hard copy, else a soft copy.
    TraceBackMesh (const Mesh& mesh,
                   bool copy_nodes = true
    );

    /// Empty constructor
    TraceBackMesh()
    : Mesh()
    {}

    /// Destructor
    ~TraceBackMesh()
    {
        if(fac_to_elem) delete fac_to_elem;
        if(vtoel) delete vtoel;
        if(ftoel) delete ftoel;
    }

    /// Given a point in space and an element id, do a local search around the element to find which element the given
    /// point belongs to. Works on convex domains.
    /// \param x input: point to be associated with an element.
    /// \param start_id input: id of element where the search commences
    /// \param final_id output: id of the element x belongs to.
    /// \param tol tolerance in reference space to determine if the point is inside the element.
    int FindPoints(const mfem::Vector& x,
                   const int& start_id,
                   int& final_id,
                   double tol = 1e-12);

    /// Precompute edge to element table, required for mesh-related methods such as FindPoints() and SplitLineInElements3().
    void BuildEdgeToElementTable();

    /// Get edge-to-element table.
    /// \return edge-to-element table.
    const std::vector<std::vector<int>>* GetEdgeToElementTable(){return &EdgeToElement;};

    /// Given two points, split the line between the points in elements that are each contained in unique mesh elements.
    /// The output is in the form of an array of element ids (elem_ids) and an associated array of fractions (fractions).
    /// This means that if we parametrize the line from x0i to x1i, the first fractions[0]*100 \% of the line belongs to
    /// element elem_ids[0], the second fractions[1]*100 \% belongs to element elem_ids[1], etc. If the point lies outside
    /// the mesh, elem_ids[end] will have value -1. Works on domains, where the line crosses a single hole. Either id0i
    /// or id1i must be bigger or equal than zero.
    /// \param x0i input: First point.
    /// \param x01 input: Second point.
    /// \param id0i input: id of the element that contains x0i
    /// \param id1i input: id of the element that contains x1i
    /// \param elem_ids output: list of element ids.
    /// \param fractions output: list of fractions.
    double SplitLineInElements3(const mfem::Vector& x0i,
                                const mfem::Vector& x1i,
                                int id0i,
                                int id1i,
                                mfem::Array<int>& elem_ids,
                                mfem::Array<double>& fractions,
                                double tol = 1e-12);

    /// Given two points, split the line between the points in elements that are each contained in unique mesh elements.
    /// The output is in the form of an array of element ids (elem_ids) and an associated array of fractions (fractions).
    /// This means that if we parametrize the line from x0i to x1i, the first fractions[0]*100 \% of the line belongs to
    /// element elem_ids[0], the second fractions[1]*100 \% belongs to element elem_ids[1], etc. If the point lies outside
    /// the mesh, elem_ids[end] will have value -1. Works on convex domains. id0 needs to be bigger or equal than zero.
    /// \param x0i input: First point.
    /// \param x01 input: Second point.
    /// \param id0 input: id of the element that contains x0i
    /// \param elem_ids output: list of element ids.
    /// \param fractions output: list of fractions.
    double SplitLineInElements3(const mfem::Vector& x0i,
                                const mfem::Vector& x1i,
                                int id0,
                                mfem::Array<int>& elem_ids,
                                mfem::Array<double>& fractions,
                                double tol = 1e-12);


};


#endif //SEMILAGRANGE0FORMS_TRACEBACKMESH_H
