/*
Copyright 2021 W.R. Tonnon

This file is part of Semi-Lagrangian Tools
Definition of test cases.

Copyright (C) <2021>  <W.R. Tonnon>

Semi-Lagrangian Tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Semi-Lagrangian Tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "SmallEdgeFiniteElement.h"


#ifndef SEMILAGRANGE0FORMS_PARAMETERS_H

class PhysicalParameters
{
public:
    int type = -1.;
    double par = 0.;
    std::vector <double> velocity_x;
    std::vector <double> velocity_y;
    int ncols, nrows, nodata_value;
    double xllcorner, yllcorner, cellsize, number;
    mfem::Mesh* InitMesh = NULL;
    mfem::ND_FECollection* InitFec=NULL;
    mfem::FiniteElementSpace* InitFes = NULL;
    mfem::GridFunction* InitGF = NULL;

    PhysicalParameters(int type, double par)
    : type(type), par(par)
    {}

    int GetVectorDim()
    {
        int dim;
        switch(type)
        {
            case 0:
                dim = 2;
                break;
            case 1:
                dim = 2;
                break;
            case 2:
                dim = 2;
                break;
            case 3:
                dim = 2;
                break;
            case 4:
                dim = 2;
                break;
            case 5:
                dim = 3;
                break;
            case 6:
                dim = 3;
                break;
            case 7:
                dim = 3;
                break;
            case 8:
                dim = 3;
                break;
            case 9:
                dim = 3;
                break;
            case 10:
                dim = 3;
                break;
            case 11:
                dim = 3;
                break;
            case 12:
                dim = 3;
                break;
            case 13:
                dim = 3;
                break;
            case 14:
                dim = 3;
                break;
            case 15:
                dim = 3;
                break;
            case 16:
                dim = 2;
                break;
            case 17:
                dim = 3;
                break;
            case 18:
                dim = 3;
                break;
            case 19:
                dim = 3;
                break;
            case 20:
                dim = 3;
                break;
            case 21:
                dim = 2;
                break;
            case 22:
                dim = 2;
                break;
            case 23:
                dim = 3;
                break;
            case 24:
                dim = 3;
                break;
            case 25:
                dim = 3;
                break;
            case 26:
                dim = 2;
                break;
            case 27:
                dim = 2;
                break;
            case 28:
                dim = 2;
                break;
            case 29:
                dim = 2;
                break;
            default:
                mfem::mfem_error("Paraters::GetVectorDim(): Vector dimension not specified for the given type.");
                break;
        }
        return dim;
    }



    double GetTfinal()
    {
        double t_final;
        switch(type)
        {
            case 0:
                t_final=1.;
                break;
            case 1:
                t_final=2;
                break;
            case 2:
                t_final=1;
                break;
            case 3:
                t_final=par;
                break;
            case 4:
                t_final=1.;
                break;
            case 5:
                t_final=1.;
                break;
            case 6:
                t_final=1.;
                break;
            case 7:
                t_final=1;
                break;
            case 8:
                t_final=1.;
                break;
            case 9:
                t_final=1.;
                break;
            case 10:
                t_final=1;
                break;
            case 11:
                t_final=.5;
                break;
            case 12:
                t_final=.4;
                break;
            case 13:
                t_final=1;
                break;
            case 14:
                t_final=1;
                break;
            case 15:
                t_final=1;
                break;
            case 16:
                t_final=1.;
                break;
            case 17:
                t_final=1.;
                break;
            case 18:
                t_final=40.;
                break;
            case 19:
                t_final=4.;
                break;
            case 20:
                t_final=.4;
                break;
            case 21:
                t_final=8.;
                break;
            case 22:
                t_final=.2;
                break;
            case 23:
                t_final=.2;
                break;
            case 24:
                t_final=.4;
                break;
            case 25:
                t_final=.5;
                break;
            case 26:
                t_final=.2;
                break;
            case 27:
                t_final=1000.;
                break;
            case 28:
                t_final=60.;
                break;
            case 29:
                t_final=1;
                break;
            default:
                mfem::mfem_error("Paraters::GetTFinal(): final time not specified for the given type.");
                break;
        }
        return t_final;
    }

    const char* GetMeshFile()
    {
        const char* mesh_file;
        //std::cerr << "type = " << type << std::endl;
        switch(type)
        {
            case 0:
                mesh_file = "./../data/original-mesh/Box2D.msh";
                break;
            case 1:
                mesh_file = "./../data/original-mesh/cylinderInBox.msh";
                break;
            case 2:
                mesh_file = "./../data/original-mesh/OneByOneSquare.msh";
                break;
            case 3:
                mesh_file = "./../data/original-mesh/OneByOneSquare.msh";
                break;
            case 4:
                mesh_file = "./../data/original-mesh/Box2D.msh";
                break;
            case 5:
                mesh_file = "./../data/original-mesh/OneByOneBox.mesh";
                break;
            case 6:
                mesh_file = "./../data/original-mesh/OneByOneBox.mesh";
                break;
            case 7:
                mesh_file = "./../data/original-mesh/OneByOneBox.mesh";
                break;
            case 8:
                mesh_file = "./../data/original-mesh/OneByOneBox.mesh";
                break;
            case 9:
                mesh_file = "./../data/original-mesh/OneByOneBox.mesh";
                break;
            case 10:
                mesh_file = "./../data/original-mesh/OneByOneBox.mesh";
                break;
            case 11:
                mesh_file = "./../data/original-mesh/OneByOneBox.mesh";
                break;
            case 12:
                mesh_file = "./../data/original-mesh/OneByOneBox.mesh";
                break;
            case 13:
                mesh_file = "./../data/original-mesh/OneByOneBox.mesh";
                break;
            case 14:
                mesh_file = "./../data/original-mesh/OneByOneBox.mesh";
                break;
            case 15:
                mesh_file = "./../data/original-mesh/OneByOneBox.mesh";
                break;
            case 16:
                mesh_file = "./../data/original-mesh/Box2D.msh";
                break;
            case 17:
                mesh_file = "./../data/original-mesh/OneByOneBox.mesh";
                break;
            case 18:
                mesh_file = "./../data/original-mesh/OneByOneBox.mesh";
                break;
            case 19:
                mesh_file = "./../data/original-mesh/OneByOneBox.mesh";
                break;
            case 20:
                mesh_file = "./../data/original-mesh/Box2D.msh";
                break;
            case 21:
                mesh_file = "./../data/original-mesh/OneByOneSquare.msh";
                break;
            case 22:
                mesh_file = "./../data/original-mesh/OneByOneSquare.msh";
                break;
            case 23:
                mesh_file = "./../data/original-mesh/OneByOneBox.mesh";
                break;
            case 24:
                mesh_file = "./../data/original-mesh/OneByOneBox.mesh";
                break;
            case 25:
                mesh_file = "./../data/original-mesh/OneByOneBox.mesh";
                break;
            case 26:
                mesh_file = "./../data/original-mesh/OneByOneSquare.msh";
                break;
            case 27:
                mesh_file = "../data/original-mesh/complexGeometry2.msh";
                break;
            case 28:
                mesh_file = "../data/original-mesh/complexGeometry2.msh";
                break;
            case 29:
                mesh_file = "./../data/original-mesh/OneByOneSquare.msh";
                break;
            default:
                mfem::mfem_error("Paraters::GetMeshFile(): mesh file not specified for the given type.");
                break;
        }
        return mesh_file;
    }

    double GetEpsilon()
    {
        double epsilon;
        switch(type)
        {
            case 0:
                epsilon=par;
                break;
            case 1:
                epsilon=0.;
                break;
            case 2: {
                double nu = par;
                epsilon = nu / M_PI / M_PI;
                break;
            }
            case 3:
                epsilon=0.;
                break;
            case 4:
                epsilon=0.;
                break;
            case 5:
                epsilon=0.;
                break;
            case 6:
                epsilon=0.;
                break;
            case 7:
                epsilon=0.;
                break;
            case 8:
                epsilon=0.;
                break;
            case 9:
                epsilon=0.;
                break;
            case 10:
                epsilon=0.;
                break;
            case 11:
                epsilon=par;
                break;
            case 12:
                epsilon=par;
                break;
            case 13: {
                double nu = 1.;
                epsilon = nu / M_PI / M_PI;
                break;
            }
            case 14: {
                epsilon = 1.;
                break;
            }
            case 15: {
                epsilon = par;
                break;
            }
            case 16:
                epsilon=1.;
                break;
            case 17: {
                epsilon = par;
                break;
            }
            case 18: {
                epsilon = 1./100.;
                break;
            }
            case 19: {
                epsilon = 0.;
                break;
            }
            case 20: {
                epsilon = 0.;
                break;
            }
            case 21: {
                epsilon = 0.;
                break;
            }
            case 22:
                epsilon=par;
                break;
            case 23:
                epsilon=par;
                break;
            case 24:
                epsilon=0.;
                break;
            case 25:
                epsilon=0.;
                break;
            case 26:
                epsilon=par;
                break;
            case 27:
                epsilon=0.;
                break;
            case 28:
                epsilon=0.;
                break;
            case 29: {
                double nu = par;
                epsilon = nu / M_PI / M_PI;
                break;
            }
            default:
                mfem::mfem_error("Paraters::GetEpsilon(): Epsilon not specified for the given type.");
                break;
        }
        return epsilon;
    }

    double GetNu()
    {
        double nu;
        switch(type)
        {
            case 11:
                nu = 0.;
                break;
            case 12:
                nu = 0.;
                break;
            case 14:
                nu = 1.;
                break;
            case 15:
                nu = par;
                break;
            case 17:
                nu = 1.;
                break;
            case 18:
                nu = .005;
                break;
            case 19:
                nu = par;
                break;
            case 22:
                nu = par;
                break;
            case 23:
                nu = par;
                break;
            case 24:
                nu = par;
                break;
            case 25:
                nu = par;
                break;
            case 26:
                nu = par;
                break;
            default:
                mfem::mfem_error("Paraters::GetNu(): Nu not specified for the given type.");
                break;

        }
        return nu;
    }

    double GetKappa()
    {
        double kappa;
        switch(type)
        {
            case 11:
                kappa = 1.;
                break;
            case 12:
                kappa = 1.;
                break;
            case 14:
                kappa = 1.;
                break;
            case 15:
                kappa = 1.;
                break;
            case 17:
                kappa = .1;
                break;
            case 18:
                kappa = 1./20.;
                break;
            case 19:
                kappa = 1.;
                break;
            case 22:
                kappa = .1;
                break;
            case 23:
                kappa = 1.;
                break;
            case 24:
                kappa = 1.;
                break;
            case 25:
                kappa = 1.;
                break;
            case 26:
                kappa = .1;
                break;
            default:
                mfem::mfem_error("Paraters::GetNu(): Nu not specified for the given type.");
                break;

        }
        return kappa;
    }

    bool MHDSystem()
    {
        bool mhd_system;
        switch(type)
        {
            case 0:
                mhd_system = false;
                break;
            case 2:
                mhd_system = false;
                break;
            case 3:
                mhd_system = false;
                break;
            case 5:
                mhd_system = false;
                break;
            case 6:
                mhd_system = false;
                break;
            case 7:
                mhd_system = false;
                break;
            case 8:
                mhd_system = false;
                break;
            case 9:
                mhd_system = false;
                break;
            case 11:
                mhd_system = true;
                break;
            case 12:
                mhd_system = true;
                break;
            case 13:
                mhd_system = false;
                break;
            case 14:
                mhd_system = true;
                break;
            case 15:
                mhd_system = true;
                break;
            case 16:
                mhd_system = false;
                break;
            case 17:
                mhd_system = true;
                break;
            case 18:
                mhd_system = true;
                break;
            case 19:
                mhd_system = true;
                break;
            case 20:
                mhd_system = false;
                break;
            case 21:
                mhd_system = false;
                break;
            case 22:
                mhd_system = true;
                break;
            case 23:
                mhd_system = true;
                break;
            case 24:
                mhd_system = true;
                break;
            case 25:
                mhd_system = true;
                break;
            case 26:
                mhd_system = true;
                break;
            case 27:
                mhd_system = false;
                break;
            case 28:
                mhd_system = false;
                break;
            case 29:
                mhd_system = false;
                break;
            default:
                mfem::mfem_error("Paraters::MHDSystem(): Not specified for the given type.");
                break;
        }
        return mhd_system;
    }


    double exactSolution(const mfem::Vector x, double t){
        double res;
        mfem::Vector xx(x);
        switch(type)
        {
            default:
                mfem::mfem_error("Paraters::ExactSolution(): ExactSolution not specified for the given type.");
                break;
        }
        return res;
    };


    mfem::Vector AExactSolution(const mfem::Vector& v, double t)
    {
        mfem::Vector out(GetVectorDim());
        switch(type)
        {
            case 11:
            {
                out.Elem(0) = v.Elem(2);
                out.Elem(1) = 0.;
                out.Elem(2) = v.Elem(1)*cos(t);
                break;
            }
            case 12:
            {
                out.Elem(0) = 0.;
                out.Elem(1) = sin(t+v.Elem(0));
                out.Elem(2) = 0.;
                break;
            }
            case 14:
            {
                double x = v.Elem(0);
                double y = v.Elem(1);
                double z = v.Elem(2);
                out.Elem(0) = 0.;
                out.Elem(1) = sin(t+x);
                out.Elem(2) = 0.;
                break;
            }
            case 15:
            {
                double x = v.Elem(0);
                double y = v.Elem(1);
                double z = v.Elem(2);
                double pi = M_PI;
                out.Elem(0) = cos(pi*x)*sin(pi*y)*sin(pi*z)*exp(-t);
                out.Elem(1) =  2*sin(pi*x)*cos(pi*y)*sin(pi*z)*exp(-t);
                out.Elem(2) = -3*sin(pi*x)*sin(pi*y)*cos(pi*z)*exp(-t);
                break;
            }
            case 17:
            {
                double x = v.Elem(0);
                double y = v.Elem(1);
                double z = v.Elem(2);
                double pi = M_PI;
                out.Elem(0) = cos(pi*x)*sin(pi*y)*sin(pi*z);
                out.Elem(1) = 2.*sin(pi*x)*cos(pi*y)*sin(pi*z);
                out.Elem(2) = -3.*sin(pi*x)*sin(pi*y)*cos(pi*z);
                break;
            }
            case 18:
            {
                double x = v.Elem(0);
                double y = v.Elem(1);
                double z = v.Elem(2);
                double pi = M_PI;
                out.Elem(0) = 0.;
                out.Elem(1) = 0.;
                out.Elem(2) = y;
                break;
            }
            case 19:
            {
                double x = v.Elem(0);
                double y = v.Elem(1);
                double z = v.Elem(2);
                double pi = M_PI;
                out.Elem(0) = 0.;
                out.Elem(1) = t*(GetNu()-1.) + .5*(x-2)*(x-2);
                out.Elem(2) = 0.;
                break;
            }
            case 22:
            {
                double x = v.Elem(0);
                double y = v.Elem(1);
                double pi = M_PI;
                out.Elem(0) =  sin(pi*y)*cos(pi*x)*exp(-t);
                out.Elem(1) = -cos(pi*y)*sin(pi*x)*exp(-t);
                break;
            }
            case 23:
            {
                double x = v.Elem(0);
                double y = v.Elem(1);
                double pi = M_PI;
                out.Elem(0) =  sin(pi*y)*cos(pi*x)*exp(-t);
                out.Elem(1) = -cos(pi*y)*sin(pi*x)*exp(-t);
                out.Elem(2) = 0.;
                break;
            }
            case 24:
            {
                out.Elem(0) = 0.;
                out.Elem(1) = sin(t+v.Elem(0));
                out.Elem(2) = 0.;
                break;
            }
            case 25:
            {
                out.Elem(0) = v.Elem(2);
                out.Elem(1) = 0.;
                out.Elem(2) = v.Elem(1)*cos(t);
                break;
            }
            case 26:
            {
                double x = v.Elem(0);
                double y = v.Elem(1);
                double pi = M_PI;
                out.Elem(0) =  sin(pi*y)*cos(pi*x);
                out.Elem(1) = -cos(pi*y)*sin(pi*x);
                break;
            }
        }
        return out;
    }

    mfem::Vector VectorExactSolution(const mfem::Vector& v, double t)
    {
        mfem::Vector xx(v);
        mfem::Vector out(GetVectorDim());
        switch(type)
        {
            case 0:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double pi = M_PI;
                double sigma = 1;
                out.Elem(0)=sin(pi*x+pi)*cos(pi*y+pi);
                out.Elem(1)=-cos(pi*x+pi)*sin(pi*y+pi);
                break;
            }
            case 1:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double theta = atan2(y,x);
                double R = .15;
                double r = xx.Norml2();
                out.Elem(0) = cos(theta)*(1-R*R/(r*r))*cos(theta) + (1+R*R/(r*r))*sin(theta)*sin(theta);
                out.Elem(1) = sin(theta)*(1-R*R/(r*r))*cos(theta) - (1+R*R/(r*r))*cos(theta)*sin(theta);
                break;
            }
            case 2:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double pi = M_PI;
                double nu = GetEpsilon()*M_PI*M_PI;
                out.Elem(0)=cos(pi*x)*sin(pi*y)*exp(-2*nu*t);
                out.Elem(1)=-sin(pi*x)*cos(pi*y)*exp(-2*nu*t);
                break;
            }
            case 3:
            {
                if(t<GetTfinal()*3./4.) out = VectorInitialData(xx);
                else if(std::abs(xx.Elem(0))<.5-1e-11 & std::abs(xx.Elem(1))<.5-1e-11) {
                  out = VectorExactSolution(xx);
                }
                else{
                  out = 0.;
                }
                break;
            }
            case 4:
            {
                out.Elem(0) = -xx.Elem(1);
                out.Elem(1) = xx.Elem(0);
                break;
            }
            case 5:
            {
                out = VectorInitialData(xx);
                break;
            }
            case 6:
            {
                double pi = M_PI;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                out.Elem(0) = cos(pi*x)*sin(pi*y)*sin(pi*z);
                out.Elem(1) = -(cos(pi*y)*sin(pi*x)*sin(pi*z))/2.;
                out.Elem(2) = -(cos(pi*z)*sin(pi*x)*sin(pi*y))/2.;
                break;
            }
            case 7:
            {
                out = 0.;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                double pi = M_PI;
                double sigma = 1;
                double fac = 1.;
                out.Elem(0) = cos(fac*pi*x)*sin(fac*pi*y)*sin(fac*pi*z);
                out.Elem(1) = -(cos(fac*pi*y)*sin(fac*pi*x)*sin(fac*pi*z))/2.;
                out.Elem(2) = -(cos(fac*pi*z)*sin(fac*pi*x)*sin(fac*pi*y))/2.;
                break;
            }
            case 8:
            {
                out = 0.;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                double pi = M_PI;
                double sigma = 1;
                double fac = 1.;
                out.Elem(0) = -y*cos(t/4. + y*z);
                out.Elem(1) = -z*cos(t/4. + x*z);
                out.Elem(2) = -x*cos(t/4. + x*y);
                break;
            }
            case 9:
            {
                out = 0.;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                double pi = M_PI;
                double sigma = 1;
                double fac = 1.;
                out.Elem(0) =  -y*pi*cos(t/4 + pi*y*z)*cos(pi*y);
                out.Elem(1) = -z*pi*cos(t/4 + pi*x*z)*cos(pi*z);
                out.Elem(2) = -x*pi*cos(t/4 + pi*x*y)*cos(pi*x);
                break;
            }
            case 10:
            {
                out = 0.;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                double pi = M_PI;
                double sigma = 1;
                double fac = 1.;
                out.Elem(0) = cos(fac*pi*x)*sin(fac*pi*y)*sin(fac*pi*z);
                out.Elem(1) = -(cos(fac*pi*y)*sin(fac*pi*x)*sin(fac*pi*z))/2.;
                out.Elem(2) = -(cos(fac*pi*z)*sin(fac*pi*x)*sin(fac*pi*y))/2.;
                break;
            }
            case 11:
            {
                out.Elem(0) = v.Elem(1)*exp(-t);
                out.Elem(1) = v.Elem(2)*cos(t);
                out.Elem(2) = v.Elem(0);
                break;
            }
            case 12:
            {
                out.Elem(0) = sin(t)*sin(v.Elem(1));
                out.Elem(1) = 0.;
                out.Elem(2) = 0.;
                break;
            }
            case 13:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double pi = M_PI;
                double nu = GetEpsilon()*M_PI*M_PI;
                out.Elem(0)=cos(pi*x)*sin(pi*y)*exp(-2*nu*t);
                out.Elem(1)=-sin(pi*x)*cos(pi*y)*exp(-2*nu*t);
                out.Elem(2) = 0.;
                break;
            }
            case 14:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                out.Elem(0) = sin(t)*sin(y);
                out.Elem(1) = 0.;
                out.Elem(2) = 0.;
                break;
            }
            case 15:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                double pi = M_PI;
                out.Elem(0) = cos(pi*x)*sin(pi*y)*sin(pi*z)*exp(-t);
                out.Elem(1) = sin(pi*x)*cos(pi*y)*sin(pi*z)*exp(-t);
                out.Elem(2) = -2*sin(pi*x)*sin(pi*y)*cos(pi*z)*exp(-t);
                break;
            }
            case 16:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double pi = M_PI;
                double sigma = 1;
                out.Elem(0)=1.;
                out.Elem(1)=1.;
                break;
            }
            case 17:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                double pi = M_PI;
                out.Elem(0) =  cos(pi*x)*sin(pi*y)*sin(pi*z);
                out.Elem(1) = sin(pi*x)*cos(pi*y)*sin(pi*z);
                out.Elem(2) = -2*sin(pi*x)*sin(pi*y)*cos(pi*z);
                break;
            }
            case 18:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                double pi = M_PI;
                if( 1.-100*(.5-z)*(.5-z)>0. & 1.-6*x*x>0.)
                    out.Elem(0) = exp(2.)*exp(-1./(1.-6*x*x))*exp(-1./(1.-100*(.5-z)*(.5-z)));
                else out.Elem(0) = 0.;
                out.Elem(1) = 0;
                out.Elem(2) = 0;
                break;
            }
            case 19:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                double pi = M_PI;
                out.Elem(0) = 1./(x-2.);
                out.Elem(1) = 0;
                out.Elem(2) = 0;
                break;
            }
            case 20:
            {
                mfem::Vector Rx(GetVectorDim()), u0(GetVectorDim());
                mfem::DenseMatrix R(2,2);
                R.Elem(0,0) = cos(t);
                R.Elem(1,0) = sin(t);
                R.Elem(0,1) = -sin(t);
                R.Elem(1,1) = cos(t);
                R.Mult(xx,Rx);

                double x = Rx.Elem(0);
                double y = Rx.Elem(1);
                double pi = M_PI;

                double dist = std::sqrt(x*x+(y-.25)*(y-.25));
                if(dist <=.5){
                    u0.Elem(0) = -(4*pi*x*cos(pi*std::sqrt((y - 1./4.)*(y - 1./4.) + x*x))*cos(pi*std::sqrt((y - 1./4.)*(y - 1./4.) + x*x))*cos(pi*std::sqrt((y - 1./4.)*(y - 1./4.) + x*x))*sin(pi*std::sqrt((y - 1./4.)*(y - 1./4.) + x*x)))/std::sqrt((y - 1./4.)*(y - 1./4.) + x*x);
                    u0.Elem(1) =  -(2*pi*cos(pi*std::sqrt((y - 1./4.)*(y - 1./4.) + x*x))*cos(pi*std::sqrt((y - 1./4.)*(y - 1./4.) + x*x))*cos(pi*std::sqrt((y - 1./4.)*(y - 1./4.) + x*x))*sin(pi*std::sqrt((y - 1./4.)*(y - 1./4.) + x*x))*(2*y - 1/2))/std::sqrt((y - 1./4.)*(y - 1./4.) + x*x);
                }
                else {
                    u0 = 0.;
                }

                R.MultTranspose(u0,out);
                break;
            }
            case 21:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double pi = M_PI;
                out = 0.;
                break;
            }
            case 22:
            {
                double pi = M_PI;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                out.Elem(0) =  sin(pi*y)*cos(pi*x)*exp(-t);
                out.Elem(1) =  -cos(pi*y)*sin(pi*x)*exp(-t);
                break;
            }
            case 23:
            {
                double pi = M_PI;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                out.Elem(0) =  sin(pi*y)*cos(pi*x)*exp(-t);
                out.Elem(1) =  -cos(pi*y)*sin(pi*x)*exp(-t);
                out.Elem(2) = 0.;
                break;
            }
            case 24:
            {
                out.Elem(0) = sin(t)*sin(v.Elem(1));
                out.Elem(1) = 0.;
                out.Elem(2) = 0.;
                break;
            }
            case 25:
            {
                out.Elem(0) = v.Elem(1)*exp(-t);
                out.Elem(1) = v.Elem(2)*cos(t);
                out.Elem(2) = v.Elem(0);
                break;
            }
            case 26:
            {
                double pi = M_PI;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                out.Elem(0) =  sin(pi*y)*cos(pi*x);
                out.Elem(1) =  -cos(pi*y)*sin(pi*x);
                break;
            }
            case 27:
            {
                double pi = M_PI;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                if (xx.Elem(0) < -0.95 || xx.Elem(0) > 0.95 || xx.Elem(1) < -0.95 || xx.Elem(1) > 0.95) {
                    out.Elem(0) = sin(t);
                    out.Elem(1) = -cos(t);
                } else out = 0.;
                break;
            }
            case 28:
            {
                double pi = M_PI;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                out = std::numeric_limits<double>::quiet_NaN();
                break;
            }
            case 29:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double pi = M_PI;
                double nu = GetEpsilon()*M_PI*M_PI;
                out.Elem(0)=cos(pi*x)*sin(pi*y)*exp(-2*nu*t);
                out.Elem(1)=-sin(pi*x)*cos(pi*y)*exp(-2*nu*t);
                break;
            }
            default:
                mfem::mfem_error("Paraters::VectorExactSolution(): ExactSolution not specified for the given type.");
                break;
        }
        return out;
    }

    double InitialData(mfem::Vector x)
    {
        double res;
        double phi, eps;
        mfem::Vector xx(x);
        switch(type)
        {
            default:
                mfem::mfem_error("Paraters::InitialData(): InitialData not specified for the given type.");
                break;
        }
        return res;
    };

    mfem::Vector VectorInitialData(mfem::Vector x)
    {
        mfem::Vector xx(x);
        mfem::Vector out(x);
        switch(type)
        {
            case 0:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 1:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 2:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 3:
            {
                double pi = M_PI;
                out.Elem(0) = -pi*exp(x.Elem(0))*cos(pi*x.Elem(0))*sin(pi*x.Elem(1));
                out.Elem(1) =  pi*exp(x.Elem(0))*cos(pi*x.Elem(1))*sin(pi*x.Elem(0)) - exp(x.Elem(0))*cos(pi*x.Elem(0))*cos(pi*x.Elem(1));
                break;
            }
            case 4:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 5:
            {
                mfem::Vector out(3);
                double pi = M_PI;
                out.Elem(0) = -pi*exp(x.Elem(0))*cos(pi*x.Elem(0))*sin(pi*x.Elem(1));
                out.Elem(1) =  pi*exp(x.Elem(0))*cos(pi*x.Elem(1))*sin(pi*x.Elem(0)) - exp(x.Elem(0))*cos(pi*x.Elem(0))*cos(pi*x.Elem(1));
                out.Elem(2) = 0.;
                out = out;
                break;
            }
            case 6:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 7:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 8:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 9:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 10:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 11:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 12:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 13:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 14:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 15:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 16:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 17:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 18:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 19:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 20:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 21:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 22:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 23:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 24:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 25:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 26:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            case 27:
            {
                if(InitGF==NULL) {
                    // 1. Parse command line options.
                    const char *mesh_file = "../data/original-mesh/complexGeometry2.msh";
                    int order = 3;

                    // 2. Read the mesh from the given mesh file, and refine once uniformly.
                    mfem::Mesh mesh(mesh_file);
                    mesh.UniformRefinement();

                    // 3. Define a finite element space on the mesh. Here we use H1 continuous
                    //    high-order Lagrange finite elements of the given order.
                    mfem::H1_FECollection fec(order, mesh.Dimension());
                    mfem::FiniteElementSpace fespace(&mesh, &fec);

                    // 4. Extract the list of all the boundary DOFs. These will be marked as
                    //    Dirichlet in order to enforce zero boundary conditions.
                    mfem::Array<int> boundary_dofs;
                    //fespace.GetBoundaryTrueDofs(boundary_dofs);

                    // 5. Define the solution x as a finite element grid function in fespace. Set
                    //    the initial guess to zero, which also sets the boundary conditions.
                    mfem::GridFunction x(&fespace);
                    x = 0.0;

                    // 6. Set up the linear form b(.) corresponding to the right-hand side.
                    std::function<void(const mfem::Vector &, mfem::Vector &)> bc_lambda(
                            [this](const mfem::Vector &x, mfem::Vector &out) -> void {
                                out = this->VectorExactSolution(x,0.);
                            });
                    mfem::ConstantCoefficient zero(0.), one(1.);
                    mfem::VectorFunctionCoefficient bc_coeff(2, bc_lambda);
                    mfem::LinearForm b(&fespace);
                    b.AddBoundaryIntegrator(new mfem::BoundaryNormalLFIntegrator(bc_coeff));
                    b.Assemble();

                    mfem::LinearForm average(&fespace);
                    average.AddDomainIntegrator(new mfem::DomainLFIntegrator(one));
                    average.Assemble();

                    // 7. Set up the bilinear form a(.,.) corresponding to the -Delta operator.
                    mfem::BilinearForm a(&fespace);
                    a.AddDomainIntegrator(new mfem::DiffusionIntegrator);
                    a.Assemble();

                    // 8. Form the linear system A X = B. This includes eliminating boundary
                    //    conditions, applying AMR constraints, and other transformations.
                    int Ndofsr = average.Size();
                    int Ndofs = average.Size() + 1;
                    mfem::SparseMatrix Atemp(Ndofsr, Ndofsr), A(Ndofs, Ndofs);
                    mfem::Vector B, X;
                    a.FormLinearSystem(boundary_dofs, x, b, Atemp, X, B);
                    for (int i = 0; i < a.NumRows(); ++i) {
                        int *row_cols = Atemp.GetRowColumns(i);
                        double *row_vals = Atemp.GetRowEntries(i);
                        for (int j = 0; j < Atemp.RowSize(i); ++j) {
                            A.Add(i, row_cols[j], row_vals[j]);
                        }
                    }
                    for (int i = 0; i < average.Size(); ++i) {
                        A.Add(Ndofsr, i, average.Elem(i));
                        A.Add(i, Ndofsr, average.Elem(i));
                    }
                    A.Finalize();

                    mfem::Vector sol(Ndofs), RHS(Ndofs);
                    sol = 0.;
                    RHS = 0.;
                    for (int i = 0; i < b.Size(); ++i) {
                        RHS.Elem(i) = b.Elem(i);
                    }

                    // 9. Solve the system using PCG with symmetric Gauss-Seidel preconditioner.
                    mfem::MINRES(A, RHS, sol, 0, 20000, 1e-22, 0.0);

                    // 10. Recover the solution x as a grid function and save to file. The output
                    //     can be viewed using GLVis as follows: "glvis -m mesh.mesh -g sol.gf"

                    for (int i = 0; i < x.Size(); ++i) {
                        x.Elem(i) = sol.Elem(i);
                    }
                    x.Save("sol.gf");
                    mesh.Save("mesh.mesh");

                    // Save meshes and grid functions in VTK format
                    //std::fstream vtkFs( "sol.vtk", std::ios::out);
                    //std::fstream vtkFs_vec( "sol_vec.vtk", std::ios::out);

                    //const int ref = 0;
                    //mesh.PrintVTK( vtkFs, ref);
                    //x.SaveVTK( vtkFs, "scalar_gf", ref);

                    mfem::ND_FECollection fec_vec(order, mesh.Dimension());
                    mfem::FiniteElementSpace fespace_vec(&mesh, &fec_vec);
                    mfem::GradientGridFunctionCoefficient grad_x(&x);
                    mfem::GridFunction x_vec(&fespace_vec);
                    x_vec.ProjectCoefficient(grad_x);
                    //x_vec.SaveVTK( vtkFs, "scalar_gf", ref);




                    // Save gridfunction
                    InitMesh = new mfem::Mesh(mesh_file);
                    InitMesh->UniformRefinement();
                    InitFec = new mfem::ND_FECollection(order, mesh.Dimension());
                    InitFes = new mfem::FiniteElementSpace(InitMesh, InitFec);
                    InitGF = new mfem::GridFunction(InitFes);
                    InitGF->ProjectCoefficient(grad_x);

                                        // Save mfem meshes using VisitDataCollection
                    mfem::VisItDataCollection dc("mesh.mesh", &mesh);
                    dc.SetPrefixPath("");
                    dc.RegisterField("scalar_gf", &x);
                    dc.RegisterField("vector_gf", &x_vec);
                    dc.RegisterField("vector_gf2", InitGF);
                    dc.Save();

                    // Save meshes and grid functions in VTK format
                    std::fstream vtkFs("/home/wtonnon/Documents/sol.vtk", std::ios::out);

                    const int ref = 0;
                    mesh.PrintVTK(vtkFs, ref);
                    x.SaveVTK(vtkFs, "scalar_gf", ref);
                    x_vec.SaveVTK(vtkFs, "vector_gf", ref);
                    InitGF->SaveVTK(vtkFs, "vector_gf2", ref);
                    //vectorGF.SaveVTK( vtkFs, "vector_gf", ref);
                }
                mfem::DenseMatrix point_mat(2,1);
                point_mat.SetCol(0,x);
                mfem::Array<int> elem_ids;
                mfem::Array<mfem::IntegrationPoint> ips;
                int num_points_found = InitMesh->FindPoints(point_mat,elem_ids,ips);
                if(!num_points_found){
                    mfem::mfem_error("Parameters::VectorInitialData(): Point not found.");
                }
                InitGF->GetVectorValue(elem_ids[0],ips[0],out);

                break;
            }
            case 28:
            {
                //mfem::DenseMatrix point_mat(2,1);
                //point_mat.SetCol(0,x);
                //mfem::Array<int> elem_ids;
                mfem::Array<mfem::IntegrationPoint> ips;
                //int num_points_found = InitMesh->FindPoints(point_mat,elem_ids,ips);

                // For each point in 'point_mat', find the element whose center is closest.
                double min_dist = 1e10;
                min_dist = std::numeric_limits<double>::max();
                int e_idx = -1;

                mfem::Vector pt(InitMesh->Dimension());
                for (int i = 0; i < InitMesh->GetNE(); i++)
                {
                    InitMesh->GetElementTransformation(i)->Transform(
                    mfem::Geometries.GetCenter(InitMesh->GetElementBaseGeometry(i)), pt);
                        double dist = pt.DistanceTo(x);
                        if (dist < min_dist) {
                            min_dist = dist;
                            e_idx = i;
                        }
                }
                TraceBackMesh tbmesh(*InitMesh,false);
                int final_id=0;
                tbmesh.FindPoints(x,e_idx,final_id);
                mfem::IntegrationPoint ip;
                mfem::InverseElementTransformation* inv_tr = new mfem::InverseElementTransformation();
                if (final_id!=-1) {
                    inv_tr->SetTransformation(*InitMesh->GetElementTransformation(final_id));
                    inv_tr->SetReferenceTol(1e-5);
                    int res = inv_tr->Transform(x, ip);
                    inv_tr->SetReferenceTol(1e-12);
                    InitGF->GetVectorValue(final_id,ip,out);
                }
                else{
                    out = 0.;

                    mfem::mfem_warning(
                            "VectorTraceBackGridFunction::TraceBackAndProjectGridFunctionPerElement(): Point should be in given element, but is not.");
                }

                break;
            }
            case 29:
            {
                out = VectorExactSolution(x,0.);
                break;
            }
            default:
                mfem::mfem_error("Paraters::VectorInitialData(): InitialData not specified for the given type.");
                break;
        }
        return out;
    }

    double exactSolution(mfem::Vector x)
    {
        double res, phi;
        mfem::Vector xx(x);
        switch(type)
        {
            default:
                mfem::mfem_error("Paraters::exactSolution(x): ExactSolution not specified for the given type.");
                break;

        }
        return res;
    }

    mfem::Vector VectorExactSolution(mfem::Vector x)
    {
        mfem::Vector xx(x), out(x);
        switch(type)
        {
            case 0:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 1:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 2:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 3:
            {
                if(velocity_x.size()==0 | velocity_y.size()==0) {
                    {
                        std::ifstream asc_file(
                                "./../data/validation/p_3/U.asc");
                        std::string text;

                        asc_file >> text;
                        asc_file >> ncols;
                        asc_file >> text;
                        asc_file >> nrows;
                        asc_file >> text;
                        asc_file >> xllcorner;
                        asc_file >> text;
                        asc_file >> yllcorner;
                        asc_file >> text;
                        asc_file >> cellsize;
                        asc_file >> text;
                        asc_file >> nodata_value;

                        while (asc_file >> number) velocity_x.push_back(number);
                        asc_file.close();

                    }
                    {
                        std::ifstream asc_file(
                                "./../data/validation/p_3/V.asc");
                        std::string text;

                        asc_file >> text;
                        asc_file >> ncols;
                        asc_file >> text;
                        asc_file >> nrows;
                        asc_file >> text;
                        asc_file >> xllcorner;
                        asc_file >> text;
                        asc_file >> yllcorner;
                        asc_file >> text;
                        asc_file >> cellsize;
                        asc_file >> text;
                        asc_file >> nodata_value;

                        while (asc_file >> number) velocity_y.push_back(number);
                        asc_file.close();
                    }
                }

                int col = std::min(std::max(int((x.Elem(0)-xllcorner)/cellsize),0),ncols-1);
                int row = std::min(std::max(int((x.Elem(1)-yllcorner)/cellsize),0),nrows-1);

                out.Elem(0) = velocity_x.at(ncols*(nrows-row-1)+col);
                out.Elem(1) = velocity_y.at(ncols*(nrows-row-1)+col);

                break;
            }
            case 4:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 5:
            {
                if(velocity_x.size()==0 | velocity_y.size()==0) {
                    {
                        std::ifstream asc_file(
                                "./../data/validation/p_3/U.asc");
                        std::string text;

                        asc_file >> text;
                        asc_file >> ncols;
                        asc_file >> text;
                        asc_file >> nrows;
                        asc_file >> text;
                        asc_file >> xllcorner;
                        asc_file >> text;
                        asc_file >> yllcorner;
                        asc_file >> text;
                        asc_file >> cellsize;
                        asc_file >> text;
                        asc_file >> nodata_value;

                        while (asc_file >> number) velocity_x.push_back(number);
                        asc_file.close();

                    }
                    {
                        std::ifstream asc_file(
                                "./../data/validation/p_3/V.asc");
                        std::string text;

                        asc_file >> text;
                        asc_file >> ncols;
                        asc_file >> text;
                        asc_file >> nrows;
                        asc_file >> text;
                        asc_file >> xllcorner;
                        asc_file >> text;
                        asc_file >> yllcorner;
                        asc_file >> text;
                        asc_file >> cellsize;
                        asc_file >> text;
                        asc_file >> nodata_value;

                        while (asc_file >> number) velocity_y.push_back(number);
                        asc_file.close();
                    }
                }

                int col = std::min(std::max(int((x.Elem(0)-xllcorner)/cellsize),0),ncols-1);
                int row = std::min(std::max(int((x.Elem(1)-yllcorner)/cellsize),0),nrows-1);

                out.Elem(0) = velocity_x.at(ncols*(nrows-row-1)+col);
                out.Elem(1) = velocity_y.at(ncols*(nrows-row-1)+col);
                out.Elem(2) = 0.;

                break;

            }
            case 6:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 7:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 8:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 9:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 10:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 11:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 12:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 13:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 14:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 15:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 16:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 17:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 18:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 19:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 20:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 21:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 22:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 23:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 24:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 25:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 26:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 27:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 28:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            case 29:
            {
                out = VectorExactSolution(x,GetTfinal());
                break;
            }
            default:
                mfem::mfem_error("Paraters::VectorExactSolution(x): ExactSolution not specified for the given type.");
                break;
        }
        return out;
    }

    mfem::Vector velocity(mfem::Vector x, double t){
        mfem::Vector res(x.Size());
        switch(type)
        {
            case 0:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 1:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 2:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 3:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 4:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 5:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 6:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 7:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 8:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 9:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 10:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 11:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 12:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 13:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 14:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 15:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 16:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 17:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 18:
            {

                if(std::abs(x.Elem(0))<.5-1e-6 & std::abs(x.Elem(1))<.5-1e-6 & std::abs(x.Elem(2))<.5-1e-6) {
                  x.Print(std::cout);
                  std::cout << "used velocity\n";
                }
                res = VectorExactSolution(x,t);
                break;
            }
            case 19:
            {

                if(std::abs(x.Elem(0))<.5-1e-6 & std::abs(x.Elem(1))<.5-1e-6 & std::abs(x.Elem(2))<.5-1e-6) {
                  x.Print(std::cout);
                  std::cout << "used velocity\n";
                }
                res = VectorExactSolution(x,t);
                break;
            }
            case 20:
            {
                mfem::Vector beta(2);
                beta.Elem(0) = x.Elem(1);
                beta.Elem(1) = -x.Elem(0);
                res = beta;
                break;
            }
            case 21:
            {

                if(std::abs(x.Elem(0))<.5-1e-6 & std::abs(x.Elem(1))<.5-1e-6) {
                  x.Print(std::cout);
                  std::cout << "used velocity\n";
                }
                res = VectorExactSolution(x,t);
                break;
            }
            case 22:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 23:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 24:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 25:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 26:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 27:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 28:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            case 29:
            {
                res = VectorExactSolution(x,t);
                break;
            }
            default:
                mfem::mfem_error("Paraters::velocity(x,t): velocity not specified for the given type.");
                break;
        }
        return res;
    }

    double SourceData(mfem::Vector x, double t)
    {
        double res=0.;
        switch(type)
        {
            default:
                mfem::mfem_error("Paraters::SourceData(x,t): Source data not specified for the given type.");
                break;
        }
        return res;
    }

    mfem::Vector ASourceData(mfem::Vector xx, double t)
    {
        mfem::Vector res(GetVectorDim());
        switch(type)
        {
            case 11:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                res.Elem(0) =  x + y*cos(t);
                res.Elem(1) =  z*exp(-t);
                res.Elem(2) =  z*cos(t)*cos(t) - y*sin(t);
                break;
            }
            case 12:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                double eps = GetEpsilon();
                double nu = GetNu();
                double kappa = GetKappa();

                res.Elem(0) =  0.;
                res.Elem(1) =   cos(t + x) + nu*sin(t + x) + cos(t + x)*sin(t)*sin(y);
                res.Elem(2) =  0.;
                break;
            }
            case 14:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                res.Elem(0) = 0.;
                res.Elem(1) = cos(t + x) + sin(t + x) + cos(t + x)*sin(t)*sin(y);
                res.Elem(2) = 0.;
                break;
            }
            case 15:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                double eps = GetEpsilon();
                double nu = GetNu();
                double kappa = GetKappa();
                double pi = M_PI;
                res.Elem(0) =    3*pi*exp(-2*t)*cos(pi*x)*cos(pi*y)*cos(pi*y)*sin(pi*x)*sin(pi*z)*sin(pi*z) - exp(-t)*cos(pi*x)*sin(pi*y)*sin(pi*z) + 4*pi*exp(-2*t)*cos(pi*x)*cos(pi*z)*cos(pi*z)*sin(pi*x)*sin(pi*y)*sin(pi*y) - 2*pi*exp(-2*t)*cos(pi*x)*sin(pi*x)*sin(pi*y)*sin(pi*y)*sin(pi*z)*sin(pi*z) + 3*nu*pi*pi*exp(-t)*cos(pi*x)*sin(pi*y)*sin(pi*z);
                res.Elem(1) =  2*pi*exp(-2*t)*cos(pi*y)*cos(pi*z)*cos(pi*z)*sin(pi*x)*sin(pi*x)*sin(pi*y) - 2*exp(-t)*cos(pi*y)*sin(pi*x)*sin(pi*z) + 3*pi*exp(-2*t)*cos(pi*x)*cos(pi*x)*cos(pi*y)*sin(pi*y)*sin(pi*z)*sin(pi*z) - 4*pi*exp(-2*t)*cos(pi*y)*sin(pi*x)*sin(pi*x)*sin(pi*y)*sin(pi*z)*sin(pi*z) + 6*nu*pi*pi*exp(-t)*cos(pi*y)*sin(pi*x)*sin(pi*z);
                res.Elem(2) =  3*exp(-t)*cos(pi*z)*sin(pi*x)*sin(pi*y) - 2*pi*exp(-2*t)*cos(pi*x)*cos(pi*x)*cos(pi*z)*sin(pi*y)*sin(pi*y)*sin(pi*z) - pi*exp(-2*t)*cos(pi*y)*cos(pi*y)*cos(pi*z)*sin(pi*x)*sin(pi*x)*sin(pi*z) - 12*pi*exp(-2*t)*cos(pi*z)*sin(pi*x)*sin(pi*x)*sin(pi*y)*sin(pi*y)*sin(pi*z) - 9*nu*pi*pi*exp(-t)*cos(pi*z)*sin(pi*x)*sin(pi*y);

                break;
            }
            case 17:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                double eps = GetEpsilon();
                double nu = GetNu();
                double kappa = GetKappa();
                double pi = M_PI;
                res.Elem(0) = 3.*nu*pi*pi*cos(pi*x)*sin(pi*y)*sin(pi*z) + 3.*pi*cos(pi*x)*cos(pi*y)*cos(pi*y)*sin(pi*x)*sin(pi*z)*sin(pi*z) + 4.*pi*cos(pi*x)*cos(pi*z)*cos(pi*z)*sin(pi*x)*sin(pi*y)*sin(pi*y) - 2.*pi*cos(pi*x)*sin(pi*x)*sin(pi*y)*sin(pi*y)*sin(pi*z)*sin(pi*z);
                res.Elem(1) = 6.*nu*pi*pi*cos(pi*y)*sin(pi*x)*sin(pi*z) + 2.*pi*cos(pi*y)*cos(pi*z)*cos(pi*z)*sin(pi*x)*sin(pi*x)*sin(pi*y) + 3.*pi*cos(pi*x)*cos(pi*x)*cos(pi*y)*sin(pi*y)*sin(pi*z)*sin(pi*z) - 4.*pi*cos(pi*y)*sin(pi*x)*sin(pi*x)*sin(pi*y)*sin(pi*z)*sin(pi*z);
                res.Elem(2) = -9.*nu*pi*pi*cos(pi*z)*sin(pi*x)*sin(pi*y) - 2.*pi*cos(pi*x)*cos(pi*x)*cos(pi*z)*sin(pi*y)*sin(pi*y)*sin(pi*z) - pi*cos(pi*y)*cos(pi*y)*cos(pi*z)*sin(pi*x)*sin(pi*x)*sin(pi*z) - 12.*pi*cos(pi*z)*sin(pi*x)*sin(pi*x)*sin(pi*y)*sin(pi*y)*sin(pi*z);

                break;
            }
            case 18:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                double eps = GetEpsilon();
                double nu = GetNu();
                double kappa = GetKappa();
                double pi = M_PI;
                res.Elem(0) = 0.;
                res.Elem(1) = 0.;
                res.Elem(2) = 0.;

                break;
            }
            case 19:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                double eps = GetEpsilon();
                double nu = GetNu();
                double kappa = GetKappa();
                double pi = M_PI;
                res.Elem(0) = 0.;
                res.Elem(1) = 0.;
                res.Elem(2) = 0.;

                break;
            }
            case 22:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double eps = GetEpsilon();
                double nu = GetNu();
                double kappa = GetKappa();
                double pi = M_PI;
                res.Elem(0) =  2*nu*pi*pi*exp(-t)*cos(pi*x)*sin(pi*y) - 2*pi*exp(-2*t)*cos(pi*x)*sin(pi*x)*sin(pi*y)*sin(pi*y) - exp(-t)*cos(pi*x)*sin(pi*y);
                res.Elem(1) =  exp(-t)*cos(pi*y)*sin(pi*x) - 2*pi*exp(-2*t)*cos(pi*y)*sin(pi*x)*sin(pi*x)*sin(pi*y) - 2*nu*pi*pi*exp(-t)*cos(pi*y)*sin(pi*x);
                break;
            }
            case 23:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double eps = GetEpsilon();
                double nu = GetNu();
                double kappa = GetKappa();
                double pi = M_PI;
                res.Elem(0) =  2*nu*pi*pi*exp(-t)*cos(pi*x)*sin(pi*y) - 2*pi*exp(-2*t)*cos(pi*x)*sin(pi*x)*sin(pi*y)*sin(pi*y) - exp(-t)*cos(pi*x)*sin(pi*y);
                res.Elem(1) =  exp(-t)*cos(pi*y)*sin(pi*x) - 2*pi*exp(-2*t)*cos(pi*y)*sin(pi*x)*sin(pi*x)*sin(pi*y) - 2*nu*pi*pi*exp(-t)*cos(pi*y)*sin(pi*x);
                res.Elem(2) = 0.;
                break;
            }
            case 24:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                double eps = GetEpsilon();
                double nu = GetNu();
                double kappa = GetKappa();

                res.Elem(0) =  0.;
                res.Elem(1) =   cos(t + x) + nu*sin(t + x) + cos(t + x)*sin(t)*sin(y);
                res.Elem(2) =  0.;
                break;
            }
            case 25:
            {

                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                res.Elem(0) =  x + y*cos(t);
                res.Elem(1) =  z*exp(-t);
                res.Elem(2) =  z*cos(t)*cos(t) - y*sin(t);
                break;
            }
            case 26:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double eps = GetEpsilon();
                double nu = GetNu();
                double kappa = GetKappa();
                double pi = M_PI;
                res.Elem(0) =  0.;
                res.Elem(1) =  0.;
                break;
            }
        }
        return res;

    }

    mfem::Vector VectorSourceData(mfem::Vector xx, double t)
    {
        mfem::Vector res(GetVectorDim());
        switch(type)
        {
           case 0: {
                res = 0.;
                double nu = 0.;
                double pi = M_PI;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double eps = GetEpsilon();
                res.Elem(0) =  2.*eps*pi*pi*cos(pi*y)*sin(pi*x);
                res.Elem(1) = -2.*eps*pi*pi*cos(pi*x)*sin(pi*y);
                break;
            }
            case 1: {
                res = 0.;
                break;
            }
            case 2: {
                res = 0.;
                break;
            }
            case 3: {
                res=0.;
                break;
            }
            case 4: {
                res=0.;
                break;
            }
            case 5: {
                res=0.;
                break;
            }
            case 6: {
                double pi = M_PI;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                res.Elem(0) = -(pi*cos(pi*x)*sin(pi*x)*(6.*cos(pi*y)*cos(pi*y)*cos(pi*z)*cos(pi*z) - 7.*cos(pi*y)*cos(pi*y) - 7.*cos(pi*z)*cos(pi*z) + 8.))/4.;
                res.Elem(1) = -(pi*cos(pi*y)*sin(pi*y)*(3.*cos(pi*x)*cos(pi*x)*cos(pi*z)*cos(pi*z) - 2.*cos(pi*x)*cos(pi*x) - 2.*cos(pi*z)*cos(pi*z) + 1.))/2.;
                res.Elem(2) = -(pi*cos(pi*z)*sin(pi*z)*(3.*cos(pi*x)*cos(pi*x)*cos(pi*y)*cos(pi*y) - 2.*cos(pi*x)*cos(pi*x) - 2.*cos(pi*y)*cos(pi*y) + 1.))/2.;

                break;
            }
            case 7: {
                double pi = M_PI;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                res.Elem(0) = -(pi*cos(pi*x)*sin(pi*x)*(6.*cos(pi*y)*cos(pi*y)*cos(pi*z)*cos(pi*z) - 7.*cos(pi*y)*cos(pi*y) - 7.*cos(pi*z)*cos(pi*z) + 8.))/4.;
                res.Elem(1) = -(pi*cos(pi*y)*sin(pi*y)*(3.*cos(pi*x)*cos(pi*x)*cos(pi*z)*cos(pi*z) - 2.*cos(pi*x)*cos(pi*x) - 2.*cos(pi*z)*cos(pi*z) + 1.))/2.;
                res.Elem(2) = -(pi*cos(pi*z)*sin(pi*z)*(3.*cos(pi*x)*cos(pi*x)*cos(pi*y)*cos(pi*y) - 2.*cos(pi*x)*cos(pi*x) - 2.*cos(pi*y)*cos(pi*y) + 1.))/2.;

                break;
            }
            case 8: {
                double pi = M_PI;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                res.Elem(0) = x*cos(t*.25 + x*y)*cos(t*.25 + x*y) - (z*z*z*sin(t*.5 + 2*x*z))*.5 + (y*sin(t*.25 + y*z))*.25 - (x*x*y*sin(t*.5 + 2*x*y))*.5 + z*cos(t*.25 + x*z)*cos(t*.25 + y*z) - x*y*y*cos(t*.25 + x*y)*sin(t*.25 + y*z) - y*z*z*cos(t*.25 + x*z)*sin(t*.25 + y*z);
                res.Elem(1) = y*cos(t*.25 + y*z)*cos(t*.25 + y*z) - (x*x*x*sin(t*.5 + 2*x*y))*.5 + (z*sin(t*.25 + x*z))*.25 + x*cos(t*.25 + x*y)*cos(t*.25 + x*z) - (y*y*z*sin(t*.5 + 2*y*z))*.5 - x*x*z*cos(t*.25 + x*y)*sin(t*.25 + x*z) - y*z*z*cos(t*.25 + y*z)*sin(t*.25 + x*z);
                res.Elem(2) = z*cos(t*.25 + x*z)*cos(t*.25 + x*z) - (y*y*y*sin(t*.5 + 2*y*z))*.5 + (x*sin(t*.25 + x*y))*.25 + y*cos(t*.25 + x*y)*cos(t*.25 + y*z) - (x*z*z*sin(t*.5 + 2*x*z))*.5 - x*y*y*cos(t*.25 + y*z)*sin(t*.25 + x*y) - x*x*z*cos(t*.25 + x*z)*sin(t*.25 + x*y);



                break;
            }
            case 9: {
                double pi = M_PI;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                res.Elem(0) = x*pi*pi*cos(t/4 + pi*x*y)*cos(t/4 + pi*x*y)*cos(pi*x)*cos(pi*x) + (y*pi*sin(t/4 + pi*y*z)*cos(pi*y))/4 - z*z*z*pi*pi*pi*cos(t/4 + pi*x*z)*sin(t/4 + pi*x*z)*cos(pi*z)*cos(pi*z) - x*x*pi*pi*pi*cos(t/4 + pi*x*y)*cos(t/4 + pi*x*y)*cos(pi*x)*sin(pi*x) + z*pi*pi*cos(t/4 + pi*x*z)*cos(t/4 + pi*y*z)*cos(pi*y)*cos(pi*z) - x*x*y*pi*pi*pi*cos(t/4 + pi*x*y)*sin(t/4 + pi*x*y)*cos(pi*x)*cos(pi*x) - y*z*pi*pi*pi*cos(t/4 + pi*x*z)*cos(t/4 + pi*y*z)*cos(pi*z)*sin(pi*y) - x*y*y*pi*pi*pi*cos(t/4 + pi*x*y)*sin(t/4 + pi*y*z)*cos(pi*x)*cos(pi*y) - y*z*z*pi*pi*pi*cos(t/4 + pi*x*z)*sin(t/4 + pi*y*z)*cos(pi*y)*cos(pi*z);
                res.Elem(1) = y*pi*pi*cos(t/4 + pi*y*z)*cos(t/4 + pi*y*z)*cos(pi*y)*cos(pi*y) + (z*pi*sin(t/4 + pi*x*z)*cos(pi*z))/4 - x*x*x*pi*pi*pi*cos(t/4 + pi*x*y)*sin(t/4 + pi*x*y)*cos(pi*x)*cos(pi*x) - y*y*pi*pi*pi*cos(t/4 + pi*y*z)*cos(t/4 + pi*y*z)*cos(pi*y)*sin(pi*y) + x*pi*pi*cos(t/4 + pi*x*y)*cos(t/4 + pi*x*z)*cos(pi*x)*cos(pi*z) - y*y*z*pi*pi*pi*cos(t/4 + pi*y*z)*sin(t/4 + pi*y*z)*cos(pi*y)*cos(pi*y) - x*z*pi*pi*pi*cos(t/4 + pi*x*y)*cos(t/4 + pi*x*z)*cos(pi*x)*sin(pi*z) - x*x*z*pi*pi*pi*cos(t/4 + pi*x*y)*sin(t/4 + pi*x*z)*cos(pi*x)*cos(pi*z) - y*z*z*pi*pi*pi*cos(t/4 + pi*y*z)*sin(t/4 + pi*x*z)*cos(pi*y)*cos(pi*z);
                res.Elem(2) = z*pi*pi*cos(t/4 + pi*x*z)*cos(t/4 + pi*x*z)*cos(pi*z)*cos(pi*z) + (x*pi*sin(t/4 + pi*x*y)*cos(pi*x))/4 - y*y*y*pi*pi*pi*cos(t/4 + pi*y*z)*sin(t/4 + pi*y*z)*cos(pi*y)*cos(pi*y) - z*z*pi*pi*pi*cos(t/4 + pi*x*z)*cos(t/4 + pi*x*z)*cos(pi*z)*sin(pi*z) + y*pi*pi*cos(t/4 + pi*x*y)*cos(t/4 + pi*y*z)*cos(pi*x)*cos(pi*y) - x*z*z*pi*pi*pi*cos(t/4 + pi*x*z)*sin(t/4 + pi*x*z)*cos(pi*z)*cos(pi*z) - x*y*pi*pi*pi*cos(t/4 + pi*x*y)*cos(t/4 + pi*y*z)*cos(pi*y)*sin(pi*x) - x*y*y*pi*pi*pi*cos(t/4 + pi*y*z)*sin(t/4 + pi*x*y)*cos(pi*x)*cos(pi*y) - x*x*z*pi*pi*pi*cos(t/4 + pi*x*z)*sin(t/4 + pi*x*y)*cos(pi*x)*cos(pi*z);

                break;
            }
            case 10: {
                double pi = M_PI;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                res = 0.;
                break;
            }
            case 11: {
                double pi = M_PI;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                res.Elem(0) =x - y*exp(-t) + z*exp(-t)*cos(t);
                res.Elem(1) =  y*exp(-2*t) + x*cos(t) - z*sin(t);
                res.Elem(2) = z*cos(t)*cos(t) + y*exp(-t);
                break;
            }
            case 12: {
                double pi = M_PI;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                double eps = GetEpsilon();
                double nu = GetNu();
                double kappa = GetKappa();

                res.Elem(0) = cos(t)*sin(y) - kappa*cos(t + x)*sin(t + x) + eps*sin(t)*sin(y);
                res.Elem(1) =      cos(y)*sin(t)*sin(t)*sin(y);
                res.Elem(2) =    0.;
                break;
            }
            case 13: {
                res=0.;
                break;
            }
            case 14:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                res.Elem(0) = cos(t)*sin(y) + sin(t)*sin(y);
                res.Elem(1) = cos(y)*sin(t)*sin(t)*sin(y);
                res.Elem(2) = 0.;
                break;
            }
            case 15:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                double kappa = GetKappa();
                double eps = GetEpsilon();
                double nu = GetNu();
                double pi = M_PI;
                res.Elem(0) =  2*pi*exp(-2*t)*cos(pi*x)*cos(pi*y)*cos(pi*y)*sin(pi*x)*sin(pi*z)*sin(pi*z) - exp(-t)*cos(pi*x)*sin(pi*y)*sin(pi*z) - kappa*(6*pi*pi*pi*exp(-2*t)*cos(pi*x)*cos(pi*y)*cos(pi*y)*sin(pi*x)*sin(pi*z)*sin(pi*z) + 36*pi*pi*pi*exp(-2*t)*cos(pi*x)*cos(pi*z)*cos(pi*z)*sin(pi*x)*sin(pi*y)*sin(pi*y)) + 2*pi*exp(-2*t)*cos(pi*x)*cos(pi*z)*cos(pi*z)*sin(pi*x)*sin(pi*y)*sin(pi*y) - 2*pi*exp(-2*t)*cos(pi*x)*sin(pi*x)*sin(pi*y)*sin(pi*y)*sin(pi*z)*sin(pi*z) + 3*eps*pi*pi*exp(-t)*cos(pi*x)*sin(pi*y)*sin(pi*z);
                res.Elem(1) =  2*pi*exp(-2*t)*cos(pi*y)*cos(pi*z)*cos(pi*z)*sin(pi*x)*sin(pi*x)*sin(pi*y) - exp(-t)*cos(pi*y)*sin(pi*x)*sin(pi*z) - kappa*(45*pi*pi*pi*exp(-2*t)*cos(pi*y)*cos(pi*z)*cos(pi*z)*sin(pi*x)*sin(pi*x)*sin(pi*y) - 3*pi*pi*pi*exp(-2*t)*cos(pi*x)*cos(pi*x)*cos(pi*y)*sin(pi*y)*sin(pi*z)*sin(pi*z)) + 2*pi*exp(-2*t)*cos(pi*x)*cos(pi*x)*cos(pi*y)*sin(pi*y)*sin(pi*z)*sin(pi*z) - 2*pi*exp(-2*t)*cos(pi*y)*sin(pi*x)*sin(pi*x)*sin(pi*y)*sin(pi*z)*sin(pi*z) + 3*eps*pi*pi*exp(-t)*cos(pi*y)*sin(pi*x)*sin(pi*z);
                res.Elem(2) =  2*exp(-t)*cos(pi*z)*sin(pi*x)*sin(pi*y) - kappa*(12*pi*pi*pi*exp(-2*t)*cos(pi*x)*cos(pi*x)*cos(pi*z)*sin(pi*y)*sin(pi*y)*sin(pi*z) + 30*pi*pi*pi*exp(-2*t)*cos(pi*y)*cos(pi*y)*cos(pi*z)*sin(pi*x)*sin(pi*x)*sin(pi*z)) - pi*exp(-2*t)*cos(pi*x)*cos(pi*x)*cos(pi*z)*sin(pi*y)*sin(pi*y)*sin(pi*z) - pi*exp(-2*t)*cos(pi*y)*cos(pi*y)*cos(pi*z)*sin(pi*x)*sin(pi*x)*sin(pi*z) - 8*pi*exp(-2*t)*cos(pi*z)*sin(pi*x)*sin(pi*x)*sin(pi*y)*sin(pi*y)*sin(pi*z) - 6*eps*pi*pi*exp(-t)*cos(pi*z)*sin(pi*x)*sin(pi*y);

                break;
            }
            case 16: {
                res = 0.;
                break;
            }
            case 17:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                double kappa = GetKappa();
                double eps = GetEpsilon();
                double nu = GetNu();
                double pi = M_PI;
                res.Elem(0) = 3*eps*pi*pi*cos(pi*x)*sin(pi*y)*sin(pi*z) - kappa*(6*nu*pi*pi*pi*cos(pi*x)*cos(pi*y)*cos(pi*y)*sin(pi*x)*sin(pi*z)*sin(pi*z) + 36*nu*pi*pi*pi*cos(pi*x)*cos(pi*z)*cos(pi*z)*sin(pi*x)*sin(pi*y)*sin(pi*y)) + 2*pi*cos(pi*x)*cos(pi*y)*cos(pi*y)*sin(pi*x)*sin(pi*z)*sin(pi*z) + 2*pi*cos(pi*x)*cos(pi*z)*cos(pi*z)*sin(pi*x)*sin(pi*y)*sin(pi*y) - 2*pi*cos(pi*x)*sin(pi*x)*sin(pi*y)*sin(pi*y)*sin(pi*z)*sin(pi*z);
                res.Elem(1) = 3*eps*pi*pi*cos(pi*y)*sin(pi*x)*sin(pi*z) - kappa*(45*nu*pi*pi*pi*cos(pi*y)*cos(pi*z)*cos(pi*z)*sin(pi*x)*sin(pi*x)*sin(pi*y) - 3*nu*pi*pi*pi*cos(pi*x)*cos(pi*x)*cos(pi*y)*sin(pi*y)*sin(pi*z)*sin(pi*z)) + 2*pi*cos(pi*y)*cos(pi*z)*cos(pi*z)*sin(pi*x)*sin(pi*x)*sin(pi*y) + 2*pi*cos(pi*x)*cos(pi*x)*cos(pi*y)*sin(pi*y)*sin(pi*z)*sin(pi*z) - 2*pi*cos(pi*y)*sin(pi*x)*sin(pi*x)*sin(pi*y)*sin(pi*z)*sin(pi*z);
                res.Elem(2) = -kappa*(12*nu*pi*pi*pi*cos(pi*x)*cos(pi*x)*cos(pi*z)*sin(pi*y)*sin(pi*y)*sin(pi*z) + 30*nu*pi*pi*pi*cos(pi*y)*cos(pi*y)*cos(pi*z)*sin(pi*x)*sin(pi*x)*sin(pi*z)) - 6*eps*pi*pi*cos(pi*z)*sin(pi*x)*sin(pi*y) - pi*cos(pi*x)*cos(pi*x)*cos(pi*z)*sin(pi*y)*sin(pi*y)*sin(pi*z) - pi*cos(pi*y)*cos(pi*y)*cos(pi*z)*sin(pi*x)*sin(pi*x)*sin(pi*z) - 8*pi*cos(pi*z)*sin(pi*x)*sin(pi*x)*sin(pi*y)*sin(pi*y)*sin(pi*z);

                break;
            }
            case 18:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                double kappa = GetKappa();
                double eps = GetEpsilon();
                double nu = GetNu();
                double pi = M_PI;
                res = VectorExactSolution(xx,t);
                break;
            }
            case 19:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                double kappa = GetKappa();
                double eps = GetEpsilon();
                double nu = GetNu();
                double pi = M_PI;
                res = VectorExactSolution(xx,t);
                break;
            }
            case 20:
            {
                res = 0.;
                break;
            }
            case 21:
            {
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                res = 0.;
                if( 1.-100*(.5-y)*(.5-y)>0.)
                    res.Elem(0) = exp(1.)*exp(-1./(1.-100*(.5-y)*(.5-y)));
                else res.Elem(0) = 0.;

                break;
            }
            case 22: {
                double pi = M_PI;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double eps = GetEpsilon();
                double nu = GetNu();
                double kappa = GetKappa();
                res.Elem(0) = 2*eps*pi*pi*exp(-t)*cos(pi*x)*sin(pi*y) - 2*pi*exp(-2*t)*cos(pi*x)*sin(pi*x)*sin(pi*y)*sin(pi*y) - exp(-t)*cos(pi*x)*sin(pi*y) - 4*kappa*pi*pi*pi*exp(-2*t)*cos(pi*x)*cos(pi*y)*cos(pi*y)*sin(pi*x);
                res.Elem(1) =  exp(-t)*cos(pi*y)*sin(pi*x) - 2*pi*exp(-2*t)*cos(pi*y)*sin(pi*x)*sin(pi*x)*sin(pi*y) - 2*eps*pi*pi*exp(-t)*cos(pi*y)*sin(pi*x) - 4*kappa*pi*pi*pi*exp(-2*t)*cos(pi*x)*cos(pi*x)*cos(pi*y)*sin(pi*y);
                break;
            }
            case 23: {
                double pi = M_PI;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double eps = GetEpsilon();
                double nu = GetNu();
                double kappa = GetKappa();
                res.Elem(0) = 2*eps*pi*pi*exp(-t)*cos(pi*x)*sin(pi*y) - 2*pi*exp(-2*t)*cos(pi*x)*sin(pi*x)*sin(pi*y)*sin(pi*y) - exp(-t)*cos(pi*x)*sin(pi*y) - 4*kappa*pi*pi*pi*exp(-2*t)*cos(pi*x)*cos(pi*y)*cos(pi*y)*sin(pi*x);
                res.Elem(1) =  exp(-t)*cos(pi*y)*sin(pi*x) - 2*pi*exp(-2*t)*cos(pi*y)*sin(pi*x)*sin(pi*x)*sin(pi*y) - 2*eps*pi*pi*exp(-t)*cos(pi*y)*sin(pi*x) - 4*kappa*pi*pi*pi*exp(-2*t)*cos(pi*x)*cos(pi*x)*cos(pi*y)*sin(pi*y);
                res.Elem(2) = 0.;
                break;
            }
            case 24: {
                double pi = M_PI;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                double eps = GetEpsilon();
                double nu = GetNu();
                double kappa = GetKappa();

                res.Elem(0) = cos(t)*sin(y) - kappa*cos(t + x)*sin(t + x) + eps*sin(t)*sin(y);
                res.Elem(1) =      cos(y)*sin(t)*sin(t)*sin(y);
                res.Elem(2) =    0.;
                break;
            }
            case 25: {
                double pi = M_PI;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double z = xx.Elem(2);
                res.Elem(0) =x - y*exp(-t) + z*exp(-t)*cos(t);
                res.Elem(1) =  y*exp(-2*t) + x*cos(t) - z*sin(t);
                res.Elem(2) = z*cos(t)*cos(t) + y*exp(-t);
                break;
            }
            case 26: {
                double pi = M_PI;
                double x = xx.Elem(0);
                double y = xx.Elem(1);
                double eps = GetEpsilon();
                double nu = GetNu();
                double kappa = GetKappa();
                res.Elem(0) = 0.;
                res.Elem(1) = 0.;
                break;
            }
            case 27: {
                res.Elem(0) = 0.;
                res.Elem(1) = 0.;
                break;
            }
            case 28: {
                res.Elem(0) = 0.;
                res.Elem(1) = 0.;
                break;
            }
            case 29: {
                res = 0.;
                break;
            }
            default:
                mfem::mfem_error("Paraters::VectorSourceData(x,t): Source data not specified for the given type.");
                break;
        }
        return res;

    }

    mfem::Vector enforceBCs(const mfem::Vector& x, const mfem::Vector& val, double tol=1e-12)
    {
        mfem::Vector out(x.Size());
        switch(type)
        {
            case 1:
            {
                out = val;
                int dim = x.Size();
                for(int q=0;q<dim;++q)
                {
                    if(std::abs(x.Elem(q))>1.-tol)
                    {
                        out.Elem(q) = velocity(x,0.).Elem(q);
                    }
                }
                if(x.Norml2()<.15+tol)
                {
                    mfem::Vector outward_unit(2);
                    outward_unit.Elem(0) = x.Elem(0);
                    outward_unit.Elem(1) = x.Elem(1);
                    outward_unit *= 1./outward_unit.Norml2();

                    double to_be_subtracted = outward_unit*val;
                    out.Add(-to_be_subtracted,outward_unit);
                }
                break;
            }
            case -1:
            {
                out = val;
                int dim = x.Size();
                for(int q=0;q<dim;++q)
                {
                    if(std::abs(x.Elem(q))>1.-tol)
                    {
                        out.Elem(q) = 0.;
                    }
                }
                break;
            }
            case 4:
            {
                out = val;
                int dim = x.Size();
                for(int q=0;q<dim;++q)
                {
                    if(std::abs(x.Elem(q))>1.-tol)
                    {
                        out.Elem(q) = VectorExactSolution(x,0.).Elem(q);
                    }
                }
                break;
            }
            case 3:
            case 5:
            {
                out = val;
                int dim = x.Size();
                for(int q=0;q<dim;++q)
                {
                    if(std::abs(x.Elem(q))>.5-tol)
                    {
                        out.Elem(q) = 0.;
                    }
                }
                break;
            }
            case 18:
            case 21:
            {
                out = val;
                int dim = x.Size();
                for(int q=0;q<dim;++q)
                {
                    if(std::abs(x.Elem(q))>.5-tol)
                    {
                        out.Elem(q) = 0.;
                    }
                }
                break;
            }
            case 8:
            case 9:
            case 10:
            case 13:
            case 14:
            {
                out = val;
                int dim = x.Size();
                for(int q=0;q<dim;++q)
                {
                    if(std::abs(x.Elem(q))>.5-tol)
                    {
                        out.Elem(q) = VectorExactSolution(x, 0.).Elem(q);
                    }
                }
                break;
            }
            case 6:
            case 7:
            case 11:
            case 12:
            case 24:
            case 22:
            case 23:
            case 25:
            case 26:
            case 15:
            case 16:
            case 17:
            case 19:
            case 20:
            case 0:
            case 2:
            case 29:
                out = val;
                break;
            default:
                out = val;
                break;

        }
        return out;
    }

    mfem::Vector VectorCurl(const mfem::Vector& x, double t)
    {
        mfem::Vector out(GetVectorDim());
        switch(type)
        {

            case 0:{
                out.SetSize(1);
                double pi = M_PI;
                out.Elem(0) = 2*pi*sin(pi*x.Elem(0))*sin(pi*x.Elem(1));
                break;
            }
            case 2:{
                out.SetSize(1);
                double pi = M_PI;
                double nu = 1.;
                out.Elem(0) = -2*pi*exp(-2*nu*t)*cos(pi*x.Elem(0))*cos(pi*x.Elem(1));
                break;
            }
            case 11:{
                out.Elem(0)=-cos(t);
                out.Elem(1)=-1;
                out.Elem(2)=-exp(-t);
                break;
            }
            case 12:{
                out.Elem(0) = 0.;
                out.Elem(1) = 0.;
                out.Elem(2) =   -cos(x.Elem(1))*sin(t);
                break;
            }
            case 14:{
                out.Elem(0) = 0.;
                out.Elem(1) = 0.;
                out.Elem(2) = -cos(x.Elem(1))*sin(t);
                break;
            }
            case 15:{
                double xx = x.Elem(0);
                double y = x.Elem(1);
                double z = x.Elem(2);
                double pi = M_PI;
                out.Elem(0) = -3*pi*exp(-t)*cos(pi*y)*cos(pi*z)*sin(pi*xx);
                out.Elem(1) =   3*pi*exp(-t)*cos(pi*xx)*cos(pi*z)*sin(pi*y);
                out.Elem(2) = 0.;
                break;
            }
            case 16:{
                out.SetSize(1);
                double pi = M_PI;
                out.Elem(0) = 0.;
                break;
            }
            case 17:{
                double xx = x.Elem(0);
                double y = x.Elem(1);
                double z = x.Elem(2);
                double pi = M_PI;
                out.Elem(0) = -3*pi*cos(pi*y)*cos(pi*z)*sin(pi*xx);
                out.Elem(1) = 3*pi*cos(pi*xx)*cos(pi*z)*sin(pi*y);
                out.Elem(2) = 0.;
                break;
            }
            case 18:{
                double xx = x.Elem(0);
                double y = x.Elem(1);
                double z = x.Elem(2);
                double pi = M_PI;
                out = 0.;
                break;
            }
            case 19:{
                double xx = x.Elem(0);
                double y = x.Elem(1);
                double z = x.Elem(2);
                double pi = M_PI;
                out = 0.;
                break;
            }
            case 22:{
                out.SetSize(1);
                double pi = M_PI;
                double xx = x.Elem(0);
                double y = x.Elem(1);
                out.Elem(0) =  -2*pi*exp(-t)*cos(pi*xx)*cos(pi*y);
                break;
            }
            case 23:{
                out.SetSize(3);
                double xx = x.Elem(0);
                double y = x.Elem(1);
                double pi = M_PI;
                out.Elem(0) = 0.;
                out.Elem(1) = 0.;
                out.Elem(2) = -2*pi*exp(-t)*cos(pi*xx)*cos(pi*y);
                break;
            }
            case 24:{
                out.Elem(0) = 0.;
                out.Elem(1) = 0.;
                out.Elem(2) =   -cos(x.Elem(1))*sin(t);
                break;
            }
            case 25:{
                out.Elem(0)=-cos(t);
                out.Elem(1)=-1;
                out.Elem(2)=-exp(-t);
                break;
            }
            case 26:{
                out.SetSize(1);
                double pi = M_PI;
                double xx = x.Elem(0);
                double y = x.Elem(1);
                out.Elem(0) =  -2*pi*cos(pi*xx)*cos(pi*y);
                break;
            }
            case 27:{
                out.SetSize(1);
                double pi = M_PI;
                double xx = x.Elem(0);
                double y = x.Elem(1);
                out =  0.;
                break;
            }
            case 28:{
                out.SetSize(1);
                double pi = M_PI;
                double xx = x.Elem(0);
                double y = x.Elem(1);
                out =  0.;
                break;
            }
            case 29:{
                out.SetSize(1);
                double pi = M_PI;
                double nu = 1.;
                out.Elem(0) = -2*pi*exp(-2*nu*t)*cos(pi*x.Elem(0))*cos(pi*x.Elem(1));
                break;
            }
            default:
                mfem::mfem_error("Parameters::VectorCurl: Curl data not specified for the given type.");
                break;

        }
        return out;
    }

    mfem::Vector AVectorCurl(const mfem::Vector& x, double t)
    {
        mfem::Vector out(GetVectorDim());
        switch(type)
        {
            case 11:{

                out.Elem(0) = cos(t);
                out.Elem(1) = 1.;
                out.Elem(2) = 0.;
                break;

            }
            case 12:{

                out.Elem(0) = 0.;
                out.Elem(1) = 0.;
                out.Elem(2) = cos(t+x.Elem(0));
                break;
            }
            case 14:{
                out.Elem(0) = 0.;
                out.Elem(1) = 0.;
                out.Elem(2) = cos(t+x.Elem(0));
                break;
            }
            case 15:{
                double xx = x.Elem(0);
                double y = x.Elem(1);
                double z = x.Elem(2);
                double pi = M_PI;
                out.Elem(0) = -5*pi*exp(-t)*cos(pi*y)*cos(pi*z)*sin(pi*xx);
                out.Elem(1) =  4*pi*exp(-t)*cos(pi*xx)*cos(pi*z)*sin(pi*y);
                out.Elem(2) =    pi*exp(-t)*cos(pi*xx)*cos(pi*y)*sin(pi*z);
                break;
            }
            case 17:{
                double xx = x.Elem(0);
                double y = x.Elem(1);
                double z = x.Elem(2);
                double pi = M_PI;
                out.Elem(0) = -5*pi*cos(pi*y)*cos(pi*z)*sin(pi*xx);
                out.Elem(1) = 4*pi*cos(pi*xx)*cos(pi*z)*sin(pi*y);
                out.Elem(2) = pi*cos(pi*xx)*cos(pi*y)*sin(pi*z);
                break;
            }
            case 18:{
                double xx = x.Elem(0);
                double y = x.Elem(1);
                double z = x.Elem(2);
                double pi = M_PI;
                out = 0.;
                break;
            }
            case 19:{
                double xx = x.Elem(0);
                double y = x.Elem(1);
                double z = x.Elem(2);
                double pi = M_PI;
                out.Elem(0) = 0.;
                out.Elem(1) = 0.;
                out.Elem(2) = xx-2.;
                break;
            }
            case 22:{
                out.SetSize(1);
                double xx = x.Elem(0);
                double y = x.Elem(1);
                double pi = M_PI;
                out.Elem(0) = -2*pi*exp(-t)*cos(pi*xx)*cos(pi*y);
                break;
            }
            case 23:{
                out.SetSize(3);
                double xx = x.Elem(0);
                double y = x.Elem(1);
                double pi = M_PI;
                out.Elem(0) = 0.;
                out.Elem(1) = 1.;
                out.Elem(2) = -2*pi*exp(-t)*cos(pi*xx)*cos(pi*y);
                break;
            }
            case 24:{

                out.Elem(0) = 0.;
                out.Elem(1) = 0.;
                out.Elem(2) = cos(t+x.Elem(0));
                break;
            }
            case 25:{

                out.Elem(0) = cos(t);
                out.Elem(1) = 1.;
                out.Elem(2) = 0.;
                break;

            }
            case 26:{
                out.SetSize(1);
                double xx = x.Elem(0);
                double y = x.Elem(1);
                double pi = M_PI;
                out.Elem(0) = -2*pi*cos(pi*xx)*cos(pi*y);
                break;
            }
            default:
                mfem::mfem_error("Parameters::AVectorCurl: Curl data not specified for the given type.");
                break;

        }
        return out;
    }


    mfem::Vector JExact(const mfem::Vector& x, double t)
    {
        mfem::Vector out(GetVectorDim());
        switch(type)
        {
            case 11:{
                out.Elem(0) = 0.;
                out.Elem(1) = 0.;
                out.Elem(2) = 0.;
                break;

            }
            case 12:{

                out.Elem(0) = 0.;
                out.Elem(1) = sin(t+x.Elem(0));
                out.Elem(2) = 0.;
                break;
            }
            case 15:{
                double pi = M_PI;
                double xx = x.Elem(0);
                double y = x.Elem(1);
                double z = x.Elem(2);
                out.Elem(0) =  3*pi*pi*exp(-t)*cos(pi*xx)*sin(pi*y)*sin(pi*z);
                out.Elem(1) = 6*pi*pi*exp(-t)*cos(pi*y)*sin(pi*xx)*sin(pi*z);
                out.Elem(2) =  -9*pi*pi*exp(-t)*cos(pi*z)*sin(pi*xx)*sin(pi*y);
                break;
            }
            case 22:{
                double pi=M_PI;
                double xx = x.Elem(0);
                double y = x.Elem(1);
                out.Elem(0) =  2*pi*pi*exp(-t)*cos(pi*xx)*sin(pi*y);
                out.Elem(1) =  -2*pi*pi*exp(-t)*cos(pi*y)*sin(pi*xx);
                break;
            }
            case 23:{
                double xx = x.Elem(0);
                double y = x.Elem(1);
                double pi=M_PI;
                out = 0.;
                out.Elem(0) =  2*pi*pi*exp(-t)*cos(pi*xx)*sin(pi*y);
                out.Elem(1) =  -2*pi*pi*exp(-t)*cos(pi*y)*sin(pi*xx);
                break;
            }
            case 24:{

                out.Elem(0) = 0.;
                out.Elem(1) = sin(t+x.Elem(0));
                out.Elem(2) = 0.;
                break;
            }
            case 25:{

                out.Elem(0) = 0.;
                out.Elem(1) = 0.;
                out.Elem(2) = 0.;
                break;

            }
            case 26:{
                double pi=M_PI;
                double xx = x.Elem(0);
                double y = x.Elem(1);
                out.Elem(0) =  2*pi*pi*cos(pi*xx)*sin(pi*y);
                out.Elem(1) =  -2*pi*pi*cos(pi*y)*sin(pi*xx);
                break;
            }

            default:
                mfem::mfem_error("Parameters::JExact: Curl data not specified for the given type.");
                break;

        }

        return out;
    }

    bool exactInitialConditionAvailable()
    {
      switch(type)
      {
        case 28:
          return false;
        default:
          return true;
      }
    }

    void computeInitialCondition(mfem::GridFunction& WE_GF)
    {
      switch(type)
      {
        case 28:
        {
            // 1. Parse command line options.
            const char *mesh_file = "../data/original-mesh/complexGeometry2.msh";
            int order = 3;

            // 2. Read the mesh from the given mesh file, and refine once uniformly.
            mfem::Mesh mesh(mesh_file);
            //mesh.UniformRefinement();

            // Define arbitrary function u
            std::function<void(const mfem::Vector &, mfem::Vector &)> init_fun(
                    [this](const mfem::Vector &x, mfem::Vector &out) -> void {
                        out.Elem(0) = sin(2*cos(x.Norml2())-atan2(x.Elem(1),x.Elem(0)));
                        out.Elem(1) = sin(cos(x.Norml2())-2*atan2(x.Elem(1),x.Elem(0)));;
                    });
            mfem::VectorFunctionCoefficient init_fun_coeff(2, init_fun);
            mfem::RT_FECollection fec_RT(order,2);
            mfem::FiniteElementSpace fespace_RT(&mesh, &fec_RT);

            mfem::GridFunction init_func_gf(&fespace_RT);
            init_func_gf.ProjectCoefficient(init_fun_coeff);

            // Compute the divergence
            init_func_gf *= -1.;
            mfem::DivergenceGridFunctionCoefficient div_init_func_coeff(&init_func_gf);


            // 3. Define a finite element space on the mesh. Here we use H1 continuous
            //    high-order Lagrange finite elements of the given order.
            mfem::H1_FECollection fec(order, mesh.Dimension());
            mfem::FiniteElementSpace fespace(&mesh, &fec);

            // 4. Extract the list of all the boundary DOFs. These will be marked as
            //    Dirichlet in order to enforce zero boundary conditions.
            mfem::Array<int> boundary_dofs;
            //fespace.GetBoundaryTrueDofs(boundary_dofs);

            // 5. Define the solution x as a finite element grid function in fespace. Set
            //    the initial guess to zero, which also sets the boundary conditions.
            mfem::GridFunction x(&fespace);
            x = 0.0;

            // 6. Set up the linear form b(.) corresponding to the right-hand side.
            mfem::ConstantCoefficient zero(0.), one(1.);
            mfem::LinearForm b(&fespace);
            b.AddBoundaryIntegrator(new mfem::BoundaryNormalLFIntegrator(init_fun_coeff));
            b.AddDomainIntegrator(new mfem::DomainLFIntegrator(div_init_func_coeff));
            b.Assemble();

            mfem::LinearForm average(&fespace);
            average.AddDomainIntegrator(new mfem::DomainLFIntegrator(one));
            average.Assemble();

            // 7. Set up the bilinear form a(.,.) corresponding to the -Delta operator.
            mfem::BilinearForm a(&fespace);
            a.AddDomainIntegrator(new mfem::DiffusionIntegrator);
            a.Assemble();

            // 8. Form the linear system A X = B. This includes eliminating boundary
            //    conditions, applying AMR constraints, and other transformations.
            int Ndofsr = average.Size();
            int Ndofs = average.Size() + 1;
            mfem::SparseMatrix Atemp(Ndofsr, Ndofsr), A(Ndofs, Ndofs);
            mfem::Vector B, X;
            a.FormLinearSystem(boundary_dofs, x, b, Atemp, X, B);
            for (int i = 0; i < a.NumRows(); ++i) {
                int *row_cols = Atemp.GetRowColumns(i);
                double *row_vals = Atemp.GetRowEntries(i);
                for (int j = 0; j < Atemp.RowSize(i); ++j) {
                    A.Add(i, row_cols[j], row_vals[j]);
                }
            }
            for (int i = 0; i < average.Size(); ++i) {
                A.Add(Ndofsr, i, average.Elem(i));
                A.Add(i, Ndofsr, average.Elem(i));
            }
            A.Finalize();

            mfem::Vector sol(Ndofs), RHS(Ndofs);
            sol = 0.;
            RHS = 0.;
            for (int i = 0; i < b.Size(); ++i) {
                RHS.Elem(i) = b.Elem(i);
            }

            // 9. Solve the system using PCG with symmetric Gauss-Seidel preconditioner.
            mfem::MINRES(A, RHS, sol, 0, 20000, 1e-22, 0.0);

            // 10. Recover the solution x as a grid function and save to file. The output
            //     can be viewed using GLVis as follows: "glvis -m mesh.mesh -g sol.gf"

            for (int i = 0; i < x.Size(); ++i) {
                x.Elem(i) = sol.Elem(i);
            }
            x.Save("sol.gf");
            mesh.Save("mesh.mesh");

            // Save meshes and grid functions in VTK format
            //std::fstream vtkFs( "sol.vtk", std::ios::out);
            //std::fstream vtkFs_vec( "sol_vec.vtk", std::ios::out);

            //const int ref = 0;
            //mesh.PrintVTK( vtkFs, ref);
            //x.SaveVTK( vtkFs, "scalar_gf", ref);

            mfem::ND_FECollection fec_vec(order, mesh.Dimension());
            mfem::FiniteElementSpace fespace_vec(&mesh, &fec_vec);
            mfem::GradientGridFunctionCoefficient grad_x(&x);
            mfem::GridFunction x_vec(&fespace_vec);
            x_vec.ProjectCoefficient(grad_x);
            //x_vec.SaveVTK( vtkFs, "scalar_gf", ref);




            // Save gridfunction
            InitMesh = new mfem::Mesh(mesh_file);
            //InitMesh->UniformRefinement();
            InitFec = new mfem::ND_FECollection(order, mesh.Dimension());
            InitFes = new mfem::FiniteElementSpace(InitMesh, InitFec);
            InitGF = new mfem::GridFunction(InitFes);
            InitGF->ProjectCoefficient(init_fun_coeff);
            InitGF->Add(-1.,x_vec);

                                // Save mfem meshes using VisitDataCollection
            mfem::VisItDataCollection dc("mesh.mesh", &mesh);
            dc.SetPrefixPath("");
            dc.RegisterField("scalar_gf", &x);
            dc.RegisterField("vector_gf", &x_vec);
            dc.RegisterField("vector_gf2", InitGF);
            dc.Save();

            // Save meshes and grid functions in VTK format
            std::fstream vtkFs("/home/wtonnon/Documents/sol.vtk", std::ios::out);

            const int ref = 0;
            mesh.PrintVTK(vtkFs, ref);
            x.SaveVTK(vtkFs, "scalar_gf", ref);
            x_vec.SaveVTK(vtkFs, "vector_gf", ref);
            InitGF->SaveVTK(vtkFs, "vector_gf2", ref);
            //vectorGF.SaveVTK( vtkFs, "vector_gf", ref);

            //mfem::FiniteElementCollection* WE_fec = new WE_FECollection(order, InitMesh->Dimension());
            //mfem::FiniteElementSpace *fes = new mfem::FiniteElementSpace(&mesh, WE_fec);
            //mfem::GridFunction WE_GF(fes);
            mfem::VectorGridFunctionCoefficient WE_GF_coeff(InitGF);
            WE_GF.ProjectCoefficient(WE_GF_coeff);
            //WE_GF.Print(std::cout);


            return;
        }
        default:
          mfem::mfem_error("Parameters::computeInitialCondition(): Initial condition cannot be computed.");
          return;
      }
    }

};






#define SEMILAGRANGE0FORMS_PARAMETERS_H

#endif //SEMILAGRANGE0FORMS_PARAMETERS_H
