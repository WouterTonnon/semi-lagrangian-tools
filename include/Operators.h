/*
Copyright 2021 W.R. Tonnon

This file is part of Semi-Lagrangian Tools
Operators to define advection of discrete differential 1-forms.

Copyright (C) <2021>  <W.R. Tonnon>

Semi-Lagrangian Tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Semi-Lagrangian Tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MFEM_TEST_OPERATORS_H
#define MFEM_TEST_OPERATORS_H

#include "mfem.hpp"
#include <math.h>
#include "VertexValuedGridFunction.h"
#include "SmallEdgeFiniteElement.h"



/// A class that extends the capabilities of mfem::GridFunction.
class VectorTraceBackGridFunction : public mfem::GridFunction
{
protected:
    const std::function<void(const int, const mfem::Vector&, mfem::Vector&)> traceback_mapping;
    const std::function<void(const mfem::Vector&, double, mfem::Vector&)> DirichletBoundaryCondition;
    TraceBackMesh mesh;
    double h_max;
    mfem::SparseMatrix GlobalDofsToEdge;

public:
    VectorTraceBackGridFunction() = delete;

    VectorTraceBackGridFunction(
            const std::function<void(const int, const mfem::Vector &, mfem::Vector &)>& traceback_mapping,
            const std::function<void(const mfem::Vector&, double, mfem::Vector&)>& DirichletBoundaryCondition
    )
            : GridFunction(), mesh(*fes->GetMesh(),false), DirichletBoundaryCondition(DirichletBoundaryCondition), traceback_mapping(traceback_mapping)
    {
        double h_min;
        double kappa_min;
        double kappa_max;
        mesh.GetCharacteristics(h_min,h_max,kappa_min,kappa_max);
    }

    VectorTraceBackGridFunction(const GridFunction &orig,
                                const std::function<void(const int, const mfem::Vector &, mfem::Vector &)>& traceback_mapping,
                                const std::function<void(const mfem::Vector&, double, mfem::Vector&)>& DirichletBoundaryCondition
    )
            : GridFunction(orig), mesh(*fes->GetMesh(),false), DirichletBoundaryCondition(DirichletBoundaryCondition), traceback_mapping(traceback_mapping)
    {
        double h_min;
        double kappa_min;
        double kappa_max;
        mesh.GetCharacteristics(h_min,h_max,kappa_min,kappa_max);}

    VectorTraceBackGridFunction (mfem::FiniteElementSpace *f,
                                 const std::function<void(const int, const mfem::Vector &, mfem::Vector &)>& traceback_mapping,
                                 const std::function<void(const mfem::Vector&, double, mfem::Vector&)>& DirichletBoundaryCondition
    )
            : GridFunction(f), mesh(*fes->GetMesh(),false), DirichletBoundaryCondition(DirichletBoundaryCondition), traceback_mapping(traceback_mapping)
    {
        double h_min;
        double kappa_min;
        double kappa_max;
        mesh.GetCharacteristics(h_min,h_max,kappa_min,kappa_max);
    }

    VectorTraceBackGridFunction (mfem::FiniteElementSpace *f
    )
    : GridFunction(f), mesh(),
    DirichletBoundaryCondition([](const mfem::Vector& in, double t, mfem::Vector& out)->void{return;}),
    traceback_mapping(
            [](const int elem, const mfem::Vector &x, mfem::Vector &out) -> void {
                return;
            })
    {}

    VectorTraceBackGridFunction (mfem::FiniteElementSpace *f,
                                 double *data,
                                 const std::function<void(const int, const mfem::Vector &, mfem::Vector &)>& traceback_mapping,
                                 const std::function<void(const mfem::Vector&, double, mfem::Vector&)>& DirichletBoundaryCondition
    )
            : GridFunction(f,data), mesh(*fes->GetMesh(),false), DirichletBoundaryCondition(DirichletBoundaryCondition), traceback_mapping(traceback_mapping)
    {
        double h_min;
        double kappa_min;
        double kappa_max;
        mesh.GetCharacteristics(h_min,h_max,kappa_min,kappa_max);}

    VectorTraceBackGridFunction (mfem::Mesh *m,
                           std::istream &input,
                                 const std::function<void(const int, const mfem::Vector &, mfem::Vector &)>& traceback_mapping,
                                 const std::function<void(const mfem::Vector&, double, mfem::Vector&)>& DirichletBoundaryCondition
    )
            : GridFunction(m,input), mesh(*fes->GetMesh(),false), DirichletBoundaryCondition(DirichletBoundaryCondition), traceback_mapping(traceback_mapping)
    {
        double h_min;
        double kappa_min;
        double kappa_max;
        mesh.GetCharacteristics(h_min,h_max,kappa_min,kappa_max);}

    VectorTraceBackGridFunction (mfem::Mesh *m, GridFunction *gf_array[],
                                 int num_pieces,
                                 const std::function<void(const int, const mfem::Vector &, mfem::Vector &)>& traceback_mapping,
                                 const std::function<void(const mfem::Vector&, double, mfem::Vector&)>& DirichletBoundaryCondition
    )
            : GridFunction(m, gf_array, num_pieces), mesh(*fes->GetMesh(),false), DirichletBoundaryCondition(DirichletBoundaryCondition), traceback_mapping(traceback_mapping)
    {
        double h_min;
        double kappa_min;
        double kappa_max;
        mesh.GetCharacteristics(h_min,h_max,kappa_min,kappa_max);}

    VectorTraceBackGridFunction (VectorTraceBackGridFunction& other)
    : GridFunction(other), mesh(other.mesh,true), DirichletBoundaryCondition(other.DirichletBoundaryCondition), traceback_mapping(other.traceback_mapping)
    {}

    /// Copy operator
    void operator=(VectorTraceBackGridFunction& other)
    {
        Set(1.,other);
    }

    /// Copy operator
    void operator=(double other)
    {
        mfem::Vector temp(Size());
        temp = 1.;
        Set(other,temp);
    }

    void TraceBackAndProjectGridFunctionPerElement(mfem::GridFunction& coeff, const double current_t, const double dt);




    /// (Pre)compute the matrix that maps local shape functions to the tangential averages along small edges.
    template <mfem::Geometry::Type geom>
    void PrepareGlobalDofsToEdge()
    {
        if(GlobalDofsToEdge.NumRows()!=0) return;
        else{
            GlobalDofsToEdge = mfem::SparseMatrix(fes->GetNDofs());
            for (int i = 0; i < fes->GetMesh()->GetNE(); ++i) {
                mfem::Array<int> local_to_global_dof_indices;
                fes->GetElementVDofs(i, local_to_global_dof_indices);

                Whitney_TriangleElement<geom> *whitney_element = (Whitney_TriangleElement<geom> *) fes->GetFE(i);
                mfem::DenseMatrix *DofsToEdge = whitney_element->GetDofsToEdge();

                mfem::DenseMatrix temp(DofsToEdge->Size());
                GlobalDofsToEdge.GetSubMatrix(local_to_global_dof_indices, local_to_global_dof_indices, temp);

                GlobalDofsToEdge.SetSubMatrix(local_to_global_dof_indices, local_to_global_dof_indices, *DofsToEdge, 2);
            }
            GlobalDofsToEdge.Finalize();
        }
    }

    /// Project a given vector-valued function onto the finite-element space. Note that the local shape functions of
    /// small edges are linear dependent, and thus the best fit that can be achieved is a least-squares fit.
    /// \param F Function with two arguments of type mfem::Vector, the first being the input space coordinate, the second
    /// being the output.
    template <mfem::Geometry::Type geom>
    void ProjectLeastSquaresCoefficient(std::function< void(const Vector &, Vector &)> F)
    {
        // Precompute the matrix that connects DoFs to tangential averages over small edges
        PrepareGlobalDofsToEdge<geom>();

        // We need a quadrature rule to integrate over the DoFs.
        mfem::IntegrationRules IntRules(0, mfem::Quadrature1D::GaussLegendre);
        mfem::IntegrationRule ir = IntRules.Get(mfem::Geometry::SEGMENT, 8);
        mfem::Array<double> weights = ir.GetWeights();
        mfem::Vector global_vals_before_MINRES(fes->GetNDofs());

        // Loop over all elements
        for (int i = 0; i < fes->GetMesh()->GetNE(); ++i) {
            // Get the global indices that correspond to the DoFs of this element
            mfem::Array<int> local_to_global_dof_indices;
            fes->GetElementVDofs(i, local_to_global_dof_indices);

            // Obtain the type of element.
            Whitney_TriangleElement<geom> *whitney_element = (Whitney_TriangleElement<geom> *) fes->GetFE(i);

            // Get the DoFs (small edges), each represented as a set of two points in space.
            std::vector<std::vector<mfem::Vector>> local_dofs;
            local_dofs = whitney_element->GetDofs();

            // Take the isogeometric transformation that defines the element in the mesh from the reference element.
            mfem::IsoparametricTransformation elem_tr;
            fes->GetMesh()->GetElementTransformation(i, &elem_tr);

            // We need to transform the DoFs in the reference element to lines in physical space, before we can integrate over them.
            mfem::Vector local_averages(local_dofs.size()), local_vals(local_dofs.size());
            // Loop over all DoFs of the element
            for (int l = 0; l < local_dofs.size(); ++l) {
                std::vector<mfem::Vector> global_dofs;
                // Loop over the vertices of the DoF (small edge).
                for (int k = 0; k < 2; ++k) {
                    // Transform from mfem::Vector type to mfem::IntegrationPoint type.
                    mfem::IntegrationPoint ip;
                    ip.x = local_dofs.at(l).at(k).Elem(0);
                    ip.y = local_dofs.at(l).at(k).Elem(1);
                    if (fes->GetMesh()->Dimension() == 3) ip.z = local_dofs.at(l).at(k).Elem(2);

                    // Transform the vertex to a vertex in physical space.
                    mfem::Vector global_dof(fes->GetMesh()->Dimension());
                    elem_tr.SetIntPoint(&ip);
                    elem_tr.Transform(ip, global_dof);

                    // Store the global location of the vertex
                    global_dofs.push_back(global_dof);
                }

                // Now we need to integrate over the small edges in physical space.
                double val = 0;
                // Loop over all quadrature points
                for (int q = 0; q < ir.GetNPoints(); ++q) {
                    // Determine the point in physical space that corresponds to the quadrature point
                    double frac = ir.IntPoint(q).x;
                    mfem::Vector p = global_dofs.at(1);
                    p -= global_dofs.at(0);
                    p *= frac;
                    p += global_dofs.at(0);

                    // Evaluate the function at the quadrature point
                    mfem::Vector vec;
                    F(p, vec);

                    // We set tangent := x1-x0;
                    mfem::Vector tangent(fes->GetMesh()->Dimension());
                    tangent = global_dofs.at(1);
                    tangent -= global_dofs.at(0);

                    // We need the tangential component, so we multiply the output of the function with the tangent.
                    double prod;
                    if (fes->GetFE(i)->GetMapType() == mfem::FiniteElement::MapType::H_CURL) {
                        prod = tangent * vec;
                    } else
                        mfem::mfem_error(
                                "VectorTraceBackGridFunction::TraceBackAndProjectGridFunctionPerElementSmallEdges(): Only implemented for Whitney elements.");

                    // Multiply with the appropriate weight and add to the integral
                    val += weights[q] * prod;
                }
                // Store the value in a vector with local values
                local_averages.Elem(l) = val;
            }

            mfem::Vector sol(local_averages.Size());
            sol = 0.;
            whitney_element->TransformAverageToBasis(local_averages,sol);

            // It remains to solve the Least-Squares problem. We do that by solving a system of the form A^T A x = A^T b,
            // instead of A x = b. This yields the least-squares solution.
            /*
            mfem::DenseMatrix* LocalDofsToEdge = whitney_element->GetDofsToEdge();
            mfem::TransposeOperator TransposeLocalDofsToEdge(LocalDofsToEdge);
            mfem::ProductOperator LHSMatrix(&TransposeLocalDofsToEdge,LocalDofsToEdge, false, false);
            mfem::Vector LocalDofsToEdge_RHS(local_averages);
            TransposeLocalDofsToEdge.Mult(local_averages,LocalDofsToEdge_RHS);
            mfem::Vector sol(local_averages.Size());
            sol = 0.;
            double tol = 1e-10;
            mfem::MINRES(LHSMatrix,LocalDofsToEdge_RHS,sol,0,99999,tol*tol,tol*tol);
            */

            // Store the values on the right location in the vector
            SetSubVector(local_to_global_dof_indices, sol);
        }
    }

    /// Project a given GridFunction onto the finite element space by first translating the input coordinates.
    /// Note that the local shape functions of small edges are linear dependent, and thus the best fit that can be achieved
    /// is a least-squares fit.
    /// \param coeff The mfem::GridFunction to be projected on the finite-element space.
    /// \param current_t time at which the evaluation is performed. This is needed to enforce the right boundary conditions.
    /// \param dt timestep size.
    template <mfem::Geometry::Type geom>
    void TraceBackAndProjectGridFunctionPerElementSmallEdges(mfem::GridFunction& coeff, const double current_t, const double dt)
    {
        // Tolerance
        double tol =1e-12;

        // We will need to invert an ElementTransformation.
        mfem::InverseElementTransformation* inv_tr = new mfem::InverseElementTransformation();
        inv_tr->SetReferenceTol(tol);
        inv_tr->SetPhysicalRelTol(tol);

        // We will need this later
        mfem::Vector non_LS_vals(Size());
        non_LS_vals = 0.;
        std::vector<std::vector<mfem::Vector>> local_dofs;

        // Save the current DoF averages
        mfem::Vector saved_avgs(coeff.Size());
        PrepareGlobalDofsToEdge<geom>();
        GlobalDofsToEdge.Mult(coeff,saved_avgs);

        // Loop over all elements in the mesh
        for(int i=0; i<mesh.GetNE(); ++i)
        {
            // Get the global indices that correspond to the DoFs of this element
            mfem::Array<int> local_to_global_dof_indices;
            fes->GetElementVDofs(i, local_to_global_dof_indices);

            // Obtain the type of element.
            Whitney_TriangleElement<geom> *whitney_element = (Whitney_TriangleElement<geom>*)fes->GetFE(i);

            // Obtain the type of element.
            local_dofs = whitney_element->GetDofs();

            // Take the isogeometric transformation that defines the element in the mesh from the reference element.
            mfem::IsoparametricTransformation elem_tr;
            mesh.GetElementTransformation(i, &elem_tr);

            // We need to transform the DoFs in the reference element to lines in physical space, before we can integrate over them.
            mfem::Vector local_averages(local_dofs.size()), local_vals(local_dofs.size());

            // Loop over all DoFs of the element
            for(int l=0; l<local_dofs.size(); ++l){
                std::vector<mfem::Vector> transformed_global_dofs, global_dofs;
                // Loop over the vertices of the DoF (small edge).
                for(int k=0; k<2; ++k){
                    // Loop over the vertices of the DoF (small edge).
                    mfem::IntegrationPoint ip;
                    ip.x = local_dofs.at(l).at(k).Elem(0);
                    ip.y = local_dofs.at(l).at(k).Elem(1);
                    if(VectorDim()==3) ip.z = local_dofs.at(l).at(k).Elem(2);

                    // Transform the vertex to a vertex in physical space.
                    mfem::Vector global_dof(VectorDim()), transformed_global_dof(VectorDim());
                    elem_tr.SetIntPoint(&ip);
                    elem_tr.Transform(ip,global_dof);

                    // Translate the coordinates using the given transformation
                    traceback_mapping(i,global_dof,transformed_global_dof);

                    // Store the translated global location of the vertex
                    transformed_global_dofs.push_back(transformed_global_dof);
                    global_dofs.push_back(global_dof);
                }

                // We now have the translated endpoints of the small edge in physical space. However, due to translation,
                // it could be that the translated endpoints moved to different elements. Therefor, we need to find
                // in which element of the mesh they reside.
                int elem_id0, elem_id1;
                mesh.FindPoints(transformed_global_dofs.at(0), i, elem_id0, tol*0.001);
                mesh.FindPoints(transformed_global_dofs.at(1), i, elem_id1, tol*0.001);

                // It could be that the translated endpoints are in different elements, but these elements are also not
                // necessarily neighbours. Since we need to integrate over the small edge, we need to split the small
                // edge in pieces that are contained in unique elements.
                mfem::Array<int> elem_ids;
                mfem::Array<double> fractions;
                mesh.SplitLineInElements3(transformed_global_dofs.at(0),transformed_global_dofs.at(1),elem_id0,elem_id1,elem_ids,fractions,tol);

                // Sanity check
                if(elem_ids.Size()!=fractions.Size())
                    mfem::mfem_error(
                            "VectorTraceBackGridFunction::TraceBackAndProjectGridFunctionPerElement(): The size of elem_ids and fractions must be the same.");


                // Intermezzo: At this point in the algorithm, we traced back to vertices of the edges to their place in the mesh,
                // then we traced the line connecting x0 to x1 and split it in multiple parts. elem_ids contains, for every part
                // of the line, the index of the element to which it belongs. fractions contains the fraction of the line that
                // is associated with the elemenet number as given in elem_ids. It remains to integrate over the edge to obtain
                // the correct value for the DoF.

                // We need a quadrature rule of sufficient order
                mfem::IntegrationRules IntRules(0,mfem::Quadrature1D::GaussLegendre);
                mfem::IntegrationRule ir = IntRules.Get(mfem::Geometry::SEGMENT, whitney_element->GetOrder());
                mfem::Array<double> weights = ir.GetWeights();

                // Since 'coeff' is in a finite-element space, we know that within elements 'coeff' has a polynomial
                // representation. Therefor, we integrate over all fractions of the small edge individually.
                double val=0;
                double zero_fraction = 0.;

                // We set non-transformed tangent
                mfem::Vector non_transformed_tangent(VectorDim());
                non_transformed_tangent = global_dofs.at(1);
                non_transformed_tangent -= global_dofs.at(0);

                // We set transformed tangent
                mfem::Vector transformed_tangent(VectorDim());
                transformed_tangent = transformed_global_dofs.at(1);
                transformed_tangent -= transformed_global_dofs.at(0);

                // Loop over the separate parts of the line integral.
                for(int j=0; j<elem_ids.Size(); ++j)
                {
                    // Loop over the quadrature points
                    for(int q=0; q<ir.GetNPoints(); ++q) {
                        // Determine the physical point corresponding to the quadrature point.
                        double frac = 0;
                        for (int k = 0; k < j; ++k) frac += fractions[k];
                        double frac_base = frac;
                        frac += ir.IntPoint(q).x * fractions[j];
                        mfem::Vector p = transformed_global_dofs.at(1);
                        p -= transformed_global_dofs.at(0);
                        p *= frac;
                        p += transformed_global_dofs.at(0);


                        mfem::Vector vec;
                        if (elem_ids[j] == -1)
                            // The point lies outside the mesh. Apply boundary conditions.
                        {
                            DirichletBoundaryCondition(p, current_t - dt, vec);
                            if(isnan(vec.Elem(0))){
                                int index = local_to_global_dof_indices[l];
                                double avg;
                                if(index<0){
                                    avg = -saved_avgs.Elem(-index-1);
                                }
                                else avg = saved_avgs.Elem(index);

                                avg /= transformed_tangent.Norml2();
                                vec.Set(avg,transformed_tangent);
                            }
                            
                        } else
                            // The point lies inside a mesh element. Determine the vector value from the given gridfunction.
                        {
                            mfem::IntegrationPoint ip;
                            inv_tr->SetTransformation(*mesh.GetElementTransformation(elem_ids[j]));

                            inv_tr->SetReferenceTol(1e-5);
                            int res = inv_tr->Transform(p, ip);
                            inv_tr->SetReferenceTol(tol);
                            if (res != mfem::InverseElementTransformation::Inside) {
                                mfem::mfem_error(
                                        "VectorTraceBackGridFunction::TraceBackAndProjectGridFunctionPerElement(): Point should be in given element, but is not.");
                            }
                            coeff.GetVectorValue(elem_ids[j], ip, vec);
                        }

                        // We set tangent := x1-x0;
                        mfem::Vector tangent(VectorDim());
                        tangent = transformed_global_dofs.at(1);
                        tangent -= transformed_global_dofs.at(0);

                        // We need the tangential component, so we multiply the output of the function with the tangent.
                        double prod;
                        if (fes->GetFE(i)->GetMapType() == mfem::FiniteElement::MapType::H_CURL) {
                            prod = tangent * vec;
                        } else
                            mfem::mfem_error(
                                    "VectorTraceBackGridFunction::TraceBackAndProjectGridFunctionPerElementSmallEdges(): Only implemented for Whitney elements.");

                        // Multiply with the appropriate weight and add to the integral
                        val += weights[q]*fractions[j] * prod;
                    }
                }

                local_averages.Elem(l) = val;
            }

            // It remains to solve the Least-Squares problem. We do that by solving a system of the form A^T A x = A^T b,
            // instead of A x = b. This yields the least-squares solution.
            mfem::Vector sol(local_averages.Size());
            sol = 0.;
            whitney_element->TransformAverageToBasis(local_averages,sol);

            // Store the values on the right location in the vector
            SetSubVector(local_to_global_dof_indices, sol);
        }

        delete inv_tr;
    }
};



#endif //MFEM_TEST_OPERATORS_H
