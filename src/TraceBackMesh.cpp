/*
Copyright 2021 W.R. Tonnon

This file is part of Semi-Lagrangian Tools
Mesh class that allows for splitting lines through the domain into parts that are
contained in single mesh elements.

Copyright (C) <2021>  <W.R. Tonnon>

Semi-Lagrangian Tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Semi-Lagrangian Tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TraceBackMesh.h"

void TraceBackMesh::BuildEdgeToElementTable() {
    // Obtain elem_to_edge table and transpose it.

    // If EdgeToElement is non-empty, we already built the EdgeToElementTable.
    if(EdgeToElement.size()!=0) return;

    mfem::Table elem_to_edge;
    mfem::Array<int> A;
    EdgeToElement.resize(GetNEdges());

    int out=GetElementToEdgeTable(elem_to_edge, A);
    for(int i=0; i<GetNE(); ++i) {
        elem_to_edge.GetRow(i,A);
        for (int j = 0; j < A.Size(); ++j) {
            EdgeToElement.at(A[j]).push_back(i);
        }
    }

    return;
}

TraceBackMesh::TraceBackMesh (const mfem::Mesh& mesh,
                              bool copy_nodes)
        : Mesh(mesh,copy_nodes)
{
    double kappa_min;
    double kappa_max;
    this->GetCharacteristics(h_min,mesh_width,kappa_min,kappa_max);

}

double TraceBackMesh::SplitLineInElements3(const mfem::Vector& x0i,
                                           const mfem::Vector& x1i,
                                           int id0,
                                           mfem::Array<int>& elem_ids,
                                           mfem::Array<double>& fractions,
                                           double tol)
{
    // Idea of the algorithm: We have a point x that starts at x0i. At every iteration, we move this point in the direction
    // of the vector x1i-x0i, i.e. in the direction of the point x1i, as far as we can until it reaches the boundary of an
    // element. If it reaches the boundary, we store how far we moved and the id of the element in which we moved. THen
    // we switch to the element on the other side of the boundary and start moving again. We keep doing this until we reach x1i.


    // Empty the output variables.
    elem_ids.DeleteAll();
    fractions.DeleteAll();

    // The start point needs to lie inside the mesh.
    if(id0<=-1)
        mfem::mfem_error("TraceBackMesh::SplitLineInElements3(): id0 needs to be bigger or equal to zero. x1i may lie outside the mesh.");

    // If x0i and x1i are the same point, we are done.
    if(x0i.DistanceTo(x1i)<tol) {
        elem_ids.Append(id0);
        fractions.Append(1.);
        return 0.;
    }

    // We define the tangent vector
    mfem::Vector tangent(x1i);
    tangent.Add(-1.,x0i);

    int dim = Dimension();
    mfem::DenseMatrix Jinv(dim,dim);
    auto inv_tr = new mfem::InverseElementTransformation;
    mfem::IntegrationPoint ip;

    // Set the right tolerances for later
    inv_tr->SetReferenceTol(tol);
    inv_tr->SetPhysicalRelTol(tol);

    // Double check if x0i lies in element id0.
    inv_tr->SetTransformation(*GetElementTransformation(id0));
    int test = inv_tr->Transform(x0i,ip);
    if(test!=mfem::InverseElementTransformation::Inside)
        mfem::mfem_error("TraceBackMesh::SplitLineInElements3(): x0i needs to be contained in id0.");

    // Initialize x and the element that we start moving in (id).
    int id = id0, it = 0;
    mfem::Vector x(x0i);

    // Now we start the iterations to move x towards x1i.
    bool finished = false;
    while(!finished)
    {
        it++;

        if(it>500){
            std::cout << "it = " << it << std::endl;
        }
        // First, we check if x1 is in the current element. If x is outside the mesh, we consider the curent element
        // all space outside the mesh.
        int found;
        if(id!=-1){
            inv_tr->SetTransformation(*GetElementTransformation(id));
            found = inv_tr->Transform(x1i, ip);
        }
        else found = mfem::InverseElementTransformation::Inside;

        if(found == mfem::InverseElementTransformation::Inside)
        {
            // If x1i is in the current element, we are done.

            bool added = false;
            // We check for every dimension, if x is allowed to move in that direction, and add the final fraction accordingly.
            for(int i=0; i<dim & !added; ++i)
                if(std::abs(tangent.Elem(i))>tol) {
                    fractions.Append(1.-fractions.Sum());
                    added = true;
                }
            elem_ids.Append(id);
            finished = true;
        }
        else
        {
            // We need to move x to the boundary of the element.

            // Start by transforming x to the reference element
            inv_tr->SetTransformation(*GetElementTransformation(id));
            inv_tr->SetReferenceTol(1000000*tol);
            int found_in_elem = inv_tr->Transform(x, ip);
            inv_tr->SetReferenceTol(tol);

            // x is expected to be in element 'id'. If this is not the case, we try again with a lower tolerance and
            // otherwise we return an error. This is for robustness only.
            if(found_in_elem!=mfem::InverseElementTransformation::Inside)
            {

                int final_id;
                mfem::Vector centt(dim);
                GetElementCenter(id,centt);
                //centt.Print(std::cout);
                FindPoints(x,id,final_id,tol);
                std::cout << "dist = " << x.DistanceTo(x1i) << std::endl;
                std::cout << "fractions: \n";
                fractions.Print(std::cout);
                std::cout << "Elem ids: \n";
                elem_ids.Print(std::cout);

                std::cout << id << ", " << final_id << std::endl;
                x.Print(std::cout);
                inv_tr->SetReferenceTol(1e-3);
                int found_in_elem = inv_tr->Transform(x, ip);
                inv_tr->SetReferenceTol(tol);

                if(final_id==-1){
                    id = -1;
                    continue;
                }

                if(found_in_elem!=mfem::InverseElementTransformation::Inside)
                    mfem::mfem_warning("TraceBackMesh::SplitLineInElements3(): x is not in element with number id. Set to zero.");
                else
                    mfem::mfem_warning("TraceBackMesh::SplitLineInElements3(): Had to use large tolerance to find point in element, but found.");
            }

            // Transform mfem::IntegrationPoint to mfem::Vector
            mfem::Vector x_ref(dim);
            x_ref.Elem(0) = ip.x;
            x_ref.Elem(1) = ip.y;
            if(dim==3) x_ref.Elem(2) = ip.z;

            // Find the inverse Jacobian
            Jinv=GetElementTransformation(id)->Jacobian();
            Jinv.Invert();

            // Compute the direction vector tangent=x1i-x0i to the reference frame.
            mfem::Vector tangent_ref(dim);
            Jinv.Mult(tangent,tangent_ref);

            // Compute the distance to the final point in the reference frame.
            mfem::Vector diff(x1i);
            diff -= x;
            double t = diff.Norml2()/tangent.Norml2();

            // Check how far we can move and which face/edge is the limit. Store the relative distance in 't', the face
            // or edge in 'face'. This depends on the geometry of the element.
            double temp;
            int face = -1;
            if(GetElementBaseGeometry(id)==mfem::Geometry::SQUARE) {
                for (int i = 0; i < dim; ++i) {
                    if (tangent_ref.Elem(i) > tol) {
                        temp = (1. - x_ref.Elem(i)) / tangent_ref.Elem(i);
                        if (  (t >= 0. & temp < t)) {
                            t = temp;
                            face = i + 1;
                        }
                    } else if (tangent_ref.Elem(i) < -tol) {
                        temp = -x_ref.Elem(i) / tangent_ref.Elem(i);
                        if (  (t >= 0. & temp < t)) {
                            t = temp;
                            if (i == 0) face = 3;
                            if (i == 1) face = 0;
                        }
                    }
                }
            }
            else if(GetElementBaseGeometry(id)==mfem::Geometry::TRIANGLE)
            {
                for (int i = 0; i < dim; ++i) {
                    if (tangent_ref.Elem(i) < -tol) {
                        temp = -x_ref.Elem(i) / tangent_ref.Elem(i);
                        if ((t >= 0. & temp < t)) {
                            t = temp;
                            if (i == 0) face = 2;
                            if (i == 1) face = 0;
                        }
                    }
                }
                if(tangent_ref.Elem(0)+tangent_ref.Elem(1)>tol){
                    temp = (1.-x_ref.Elem(0)-x_ref.Elem(1))/(tangent_ref.Elem(0)+tangent_ref.Elem(1));
                    if ((t >= 0. & temp < t)) {
                        t = temp;
                        face = 1;
                    }
                }
            }
            else if(GetElementBaseGeometry(id)==mfem::Geometry::CUBE)
            {
                for (int i = 0; i < dim; ++i) {
                    if (tangent_ref.Elem(i) > tol) {
                        temp = (1. - x_ref.Elem(i)) / tangent_ref.Elem(i);
                        if (  (t >= 0. & temp < t)) {
                            t = temp;
                            if (i==0) face = 2;
                            if (i==1) face = 3;
                            if (i==2) face = 5;
                        }
                    } else if (tangent_ref.Elem(i) < -tol) {
                        temp = -x_ref.Elem(i) / tangent_ref.Elem(i);
                        if (  (t >= 0. & temp < t)) {
                            t = temp;
                            if (i == 0) face = 4;
                            if (i == 1) face = 1;
                            if (i == 2) face = 0;
                        }
                    }
                }
            }
            else if(GetElementBaseGeometry(id)==mfem::Geometry::TETRAHEDRON)
            {
                for (int i = 0; i < dim; ++i) {
                    if (tangent_ref.Elem(i) < -tol) {
                        temp = -x_ref.Elem(i) / tangent_ref.Elem(i);
                        if ( (t >= 0. & temp < t)) {
                            t = temp;
                            if (i==0) face = 1;
                            if (i==1) face = 2;
                            if (i==2) face = 3;
                        }
                    }
                }
                if(tangent_ref.Elem(0)+tangent_ref.Elem(1)+tangent_ref.Elem(2)>tol){
                    temp = (1.-x_ref.Elem(0)-x_ref.Elem(1)-x_ref.Elem(2))/(tangent_ref.Elem(0)+tangent_ref.Elem(1)+tangent_ref.Elem(2));
                    if ((t >= 0. & temp < t)) {
                        t = temp;
                        face = 0;
                    }
                }
            }

            // If we moved far enough, we are close enough to x1i and can finish.
            if(t>diff.Norml2()/tangent.Norml2()-tol)
            {
                finished = true;
            }

            // Check if the fractions still add up to at most 1.
            if (t > 1.+tol) {
                mfem::mfem_error("SplitLineinElements3(): fraction cannot be bigger than 1.");
            }

            // Add the data
            fractions.Append(std::max(t,0.));
            elem_ids.Append(id);


            if(!finished) {
                // If we are not finished yet, it means 'x' is currently at a boundary of element 'id'. The next step is
                // to find the appropriate element on the other side of the boundary. For robustness reasons, we consider
                // different cases: x lies on the interior of a face, on an edge (but not on a vertex) or on a vertex.
                // First we check if the point lies on a vertex, then an edge, then the face.

                // First compute the point of interest
                x_ref.Add(t, tangent_ref);
                x.Add(t, tangent);

                // Find the Vertices
                const mfem::Geometry::Type geom_type(GetElementBaseGeometry(id));
                mfem::Geometry geom;
                const mfem::IntegrationRule *vertexRule = geom.GetVertices(GetElementBaseGeometry(id));

                mfem::Geometry::Constants<mfem::Geometry::SQUARE> element_properties_square;
                mfem::Geometry::Constants<mfem::Geometry::TRIANGLE> element_properties_triangle;
                mfem::Geometry::Constants<mfem::Geometry::CUBE> element_properties_cube;
                mfem::Geometry::Constants<mfem::Geometry::TETRAHEDRON> element_properties_tetrahedron;

                bool found_on_vertex = false;
                int NumVertices = -1;
                if (dim == 2) NumVertices = 2;
                else if (GetElementBaseGeometry(id) == mfem::Geometry::CUBE) NumVertices = 4;
                else if (GetElementBaseGeometry(id) == mfem::Geometry::TETRAHEDRON) NumVertices = 3;

                // Loop over all the vertices of the element
                /*
                for (int j = 0; j < NumVertices; ++j) {
                    int vertexNumber = -1;
                    if (geom_type == mfem::Geometry::SQUARE) vertexNumber = element_properties_square.Edges[face][j];
                    if (geom_type == mfem::Geometry::TRIANGLE)
                        vertexNumber = element_properties_triangle.Edges[face][j];
                    if (geom_type == mfem::Geometry::CUBE) vertexNumber = element_properties_cube.FaceVert[face][j];
                    if (geom_type == mfem::Geometry::TETRAHEDRON)
                        vertexNumber = element_properties_tetrahedron.FaceVert[face][j];

                    // Compute the distance from the vertex to 'x'
                    const mfem::IntegrationPoint ip = vertexRule->IntPoint(vertexNumber);
                    double distSquared = 1.;
                    if (dim == 2)
                        distSquared = (ip.x - x_ref.Elem(0)) * (ip.x - x_ref.Elem(0)) +
                                      (ip.y - x_ref.Elem(1)) * (ip.y - x_ref.Elem(1));
                    else if (dim == 3)
                        distSquared = (ip.x - x_ref.Elem(0)) * (ip.x - x_ref.Elem(0)) +
                                      (ip.y - x_ref.Elem(1)) * (ip.y - x_ref.Elem(1)) +
                                      (ip.z - x_ref.Elem(2)) * (ip.z - x_ref.Elem(2));

                    // If the distance falls within the tolerance, we consider 'x' to lie on the vertex.
                    if (distSquared < tol * tol) {
                        found_on_vertex = true;

                        // The point lies on a vertex, we extend the point a bit further beyond the vertex, then we check
                        // all elements around the vertex if they contain the extended point.
                        mfem::Vector temp(x);
                        temp.Add(h_min * .5 / tangent.Norml2(), tangent);

                        mfem::Array<int> elems;
                        if (!vtoel) vtoel = GetVertexToElementTable();
                        mfem::Array<int> vertices;
                        GetElementVertices(id, vertices);
                        vtoel->GetRow(vertices[vertexNumber], elems);

                        bool found_around_vertex = false;
                        for (int k = 0; k < elems.Size() & !found_around_vertex; ++k) {
                            int elem_id = elems[k];
                            if (elem_id != id) {
                                inv_tr->SetTransformation(*GetElementTransformation(elem_id));
                                mfem::IntegrationPoint ip_test;
                                int found_in_element = inv_tr->Transform(temp, ip_test);
                                if (found_in_element == mfem::InverseElementTransformation::Inside) {
                                    id = elem_id;
                                    found_around_vertex = true;
                                }
                            }
                        }

                        // If none of the elements contain the point, it must mean that the point lies outside the mesh.
                        if (!found_around_vertex) {
                            id = -1;
                        }
                    }
                }
                */

                // If 'x' was not found on a vertex, we check if point lies on an edge
                bool found_on_edge = false;
                /*
                if (!found_on_vertex) {
                    int local_edge_ind = -1;

                    // We check for every edge manually.
                    if (GetElementBaseGeometry(id) == mfem::Geometry::CUBE) {
                        if (pow(x_ref.Elem(0), 2) + pow(x_ref.Elem(1), 2) < tol * tol) local_edge_ind = 8;
                        if (pow(x_ref.Elem(0), 2) + pow(x_ref.Elem(1) - 1, 2) < tol * tol) local_edge_ind = 11;
                        if (pow(x_ref.Elem(0) - 1, 2) + pow(x_ref.Elem(1), 2) < tol * tol) local_edge_ind = 9;
                        if (pow(x_ref.Elem(0) - 1, 2) + pow(x_ref.Elem(1) - 1, 2) < tol * tol) local_edge_ind = 10;
                        if (pow(x_ref.Elem(0), 2) + pow(x_ref.Elem(2), 2) < tol * tol) local_edge_ind = 3;
                        if (pow(x_ref.Elem(0) - 1, 2) + pow(x_ref.Elem(2), 2) < tol * tol) local_edge_ind = 1;
                        if (pow(x_ref.Elem(1), 2) + pow(x_ref.Elem(2), 2) < tol * tol) local_edge_ind = 0;
                        if (pow(x_ref.Elem(1) - 1, 2) + pow(x_ref.Elem(2), 2) < tol * tol) local_edge_ind = 2;
                        if (pow(x_ref.Elem(0), 2) + pow(x_ref.Elem(2) - 1, 2) < tol * tol) local_edge_ind = 7;
                        if (pow(x_ref.Elem(0) - 1, 2) + pow(x_ref.Elem(2) - 1, 2) < tol * tol) local_edge_ind = 5;
                        if (pow(x_ref.Elem(1), 2) + pow(x_ref.Elem(2) - 1, 2) < tol * tol) local_edge_ind = 4;
                        if (pow(x_ref.Elem(1) - 1, 2) + pow(x_ref.Elem(2) - 1, 2) < tol * tol) local_edge_ind = 6;
                    } else if (GetElementBaseGeometry(id) == mfem::Geometry::TETRAHEDRON) {
                        if (pow(x_ref.Elem(0), 2) + pow(x_ref.Elem(1), 2) < tol * tol) local_edge_ind = 2;
                        if (pow(x_ref.Elem(1), 2) + pow(x_ref.Elem(2), 2) < tol * tol) local_edge_ind = 0;
                        if (pow(x_ref.Elem(0), 2) + pow(x_ref.Elem(2), 2) < tol * tol) local_edge_ind = 1;
                        if (pow(x_ref.Elem(0) + x_ref.Elem(1) - 1, 2) + pow(x_ref.Elem(2), 2) < tol * tol)
                            local_edge_ind = 3;
                        if (pow(x_ref.Elem(0) + x_ref.Elem(2) - 1, 2) + pow(x_ref.Elem(1), 2) < tol * tol)
                            local_edge_ind = 4;
                        if (pow(x_ref.Elem(1) + x_ref.Elem(2) - 1, 2) + pow(x_ref.Elem(0), 2) < tol * tol)
                            local_edge_ind = 5;
                    }

                    if (local_edge_ind != -1) {
                        auto curl = [](const mfem::Vector &x, const mfem::Vector &y) -> mfem::Vector {
                            mfem::Vector out(x.Size());
                            out.Elem(0) = x.Elem(1) * y.Elem(2) - x.Elem(2) * y.Elem(1);
                            out.Elem(1) = x.Elem(2) * y.Elem(0) - x.Elem(0) * y.Elem(2);
                            out.Elem(2) = x.Elem(0) * y.Elem(1) - x.Elem(1) * y.Elem(0);
                            return out;
                        };

                        // Prepare edge
                        mfem::Array<int> edges, cor, vertices;
                        GetElementEdges(id, edges, cor);

                        GetEdgeVertices(edges[local_edge_ind], vertices);
                        mfem::Vector edge(dim);
                        for (int q = 0; q < dim; ++q) {
                            edge.Elem(q) = GetVertex(vertices[1])[q] - GetVertex(vertices[0])[q];
                        }

                        mfem::Vector unit_tangent(tangent);
                        unit_tangent *= 1./unit_tangent.Norml2();

                        // Prepare Edge to Element Table
                        BuildEdgeToElementTable();
                        auto etoel = EdgeToElement;

                        // For all elements around the edges, we check if the unit tangent lies between the two triangles
                        // that are faces of the tetrahedron, but also contain the considered edge.
                        std::vector<int> elems_around_edge = etoel.at(edges[local_edge_ind]);
                        double unit_tangent_times_cent = -100000;
                        for (int q = 0; q < elems_around_edge.size() ; ++q) {
                            int elem_id = elems_around_edge.at(q);
                            mfem::Array<int> vertices_elem_q, non_edge_vertices;
                            GetElementVertices(elem_id, vertices_elem_q);
                            for(int p=0; p<4; ++p){
                                if(vertices_elem_q[p]!=vertices[0] & vertices_elem_q[p]!=vertices[1])
                                    non_edge_vertices.Append(vertices_elem_q[p]);
                            }

                            mfem::Vector a(3), b(3), temp(3);
                            for (int q = 0; q < dim; ++q) {
                                temp.Elem(q) = GetVertex(non_edge_vertices[0])[q] - GetVertex(vertices[0])[q];
                            }
                            a = curl(edge,temp);
                            a *= 1./a.Norml2();
                            for (int q = 0; q < dim; ++q) {
                                temp.Elem(q) = GetVertex(non_edge_vertices[1])[q] - GetVertex(vertices[0])[q];
                            }
                            b = curl(edge,temp);
                            b *= 1./b.Norml2();

                            mfem::Vector cent(3);
                            GetElementCenter(elem_id, cent);
                            cent -= x;
                            cent *= 1./cent.Norml2();

                            if(((a*unit_tangent>=-tol & b*unit_tangent<=tol) | (a*unit_tangent<=tol & b*unit_tangent>=-tol))  )
                            {
                                bool old_element = false;
                                for(int c=0; c<elem_ids.Size(); ++c)
                                    if(elem_id==elem_ids[c]) old_element = true;
                                if(unit_tangent*cent>=unit_tangent_times_cent & !old_element)
                                {
                                    unit_tangent_times_cent = unit_tangent*cent;
                                    id = elem_id;
                                    found_on_edge = true;
                                }
                            }

                        }

                        // if no element on the other side was found, x1i must lie outside the mesh.
                        if (!found_on_edge) {
                            id = -1;
                            found_on_edge = true;
                        }

                    }

                }
                */

                if (!found_on_vertex & !found_on_edge) {
                    // We can assume 'x' lies on the interior of a face.
                    mfem::Array<int> fcs, ors, elems;

                    if (dim == 2) GetElementEdges(id, fcs, ors);
                    if (dim == 3) GetElementFaces(id, fcs, ors);
                    if (!ftoel) ftoel = GetFaceToElementTable();
                    ftoel->GetRow(fcs[face], elems);
                    if (elems.Size() == 2) {
                        if (elems[0] == id) id = elems[1];
                        else if (elems[1] == id) id = elems[0];
                        else {
                            mfem::mfem_error(
                                    "TraceBackMesh::SplitLineInElements3(): The current element is not on either side of the face.");
                        }
                    } else if (elems.Size() == 1 & elems[0] == id) id = -1;
                    else{
                        fractions.Print(std::cout);
                        std::cout << "face, fcs[face] = " << face << ", " << fcs[face] << std::endl;
                        mfem::mfem_error(
                                "TraceBackMesh::SplitLineInElements3(): The number of elements that border this face is not 1 or 2. This is unexpected.");

                    }
                }
            }

        }
    }

    if(fractions.Sum()>1.+1e-5) {
        mfem::mfem_error("SplitLineInElements3(): Sum of fractions cannot be bigger than one.");
    }

    // Clean up
    delete inv_tr;

    return 0.;

}

double TraceBackMesh::SplitLineInElements3(const mfem::Vector& x0i,
                                           const mfem::Vector& x1i,
                                           int id0i,
                                           int id1i,
                                           mfem::Array<int>& elem_ids,
                                           mfem::Array<double>& fractions,
                                           double tol)
{
    // GOAL
    // We take a line through a triangular mesh, and split the line in multiple parts, each part contained in a single
    // element.

    // INPUT
    // x0i: First vertex of the edge
    // x1i: Second vertex of the edge
    // id01: element id of element that contains x0i, -1 if outside the domain
    // id1i: element id of element that contains x1i, -1 if outside the domain
    // tol: required tolerance

    // OUTPUT
    // elem_ids: element id's of the elements in which the parts of the line are contained
    // fractions: We split the line in multiple elements, this array gives how much percentage of the line the part occupies
    //            example: line goes from 0 to 1, fractions = [.25,.75] means that the line is divided in [0,.25] and [.25,.75]

    // We assume that if both vertices lie outside the mesh, the entire line lies outside the mesh
    if(id0i==-1 && id1i == -1)
    {
        elem_ids.Append(-1);
        fractions.Append(1.-fractions.Sum());
        return 0.;
    }

    if(id0i==id1i)
    {
        elem_ids.Append(id0i);
        fractions.Append(1.);
        return 0.;
    }

    // We always start searching inside the mesh and go to outside the mesh.
    // This means that if vertex 1 lies outside the mesh, but vertex 2 lies inside the mesh, we need to start searching
    // at vertex 2.
    int direction;
    mfem::Vector x0(Dimension()), x1(Dimension());
    int id0, id1;
    if(id0i!=-1)
    {
        direction = 1;
        x0 = x0i;
        x1 = x1i;
        id0 = id0i;
        id1 = id1i;
    }
    else
    {
        direction = 0;
        x0 = x1i;
        x1 = x0i;
        id0 = id1i;
        id1 = id0i;
    }

    // Call the other SplitLineINElements3 with appropriate elements
    SplitLineInElements3(x0,x1,id0,elem_ids,fractions,tol);

    if(elem_ids.Last()==-1 & id1!=-1)
    {
        // We reverse the line
        x0 = x1i;
        x1 = x0i;
        id0 = id1i;
        id1 = id0i;

        // We do the same analysis on the reversed line
        mfem::Array<int> elem_ids2;
        mfem::Array<double> fractions2;
        SplitLineInElements3(x0,x1,id0,elem_ids2,fractions2,tol);

        if(elem_ids2.Last()==-1) {
            double remaining_outside_fraction = fractions.Last() - (1. - fractions2.Last());
            if (remaining_outside_fraction < 1e-5) {
                //std::cout << remaining_outside_fraction << ", " << fractions2.Last() << ", " << fractions.Last() << std::endl;
                //mfem::mfem_error(
                //        "SplitLineInElements3(): Line crosses outside of the mesh, but something went wrong in the processing.");
            }
            fractions.Last() = remaining_outside_fraction;
            for (int k = elem_ids2.Size() - 2; k >= 0; --k) {
                fractions.Append(fractions2[k]);
                elem_ids.Append(elem_ids2[k]);
            }
        }
        else
        {
            // There is no part in the middle of the line that is outside the mesh. Instead the forward line method evaluated
            // to outside the mesh, but the backward version did not. This is possible if the x2 is very close to the boundary.
            elem_ids.DeleteAll();
            fractions.DeleteAll();
            for(int k=elem_ids2.Size()-1; k>=0; --k) {
                fractions.Append(fractions2[k]);
                elem_ids.Append(elem_ids2[k]);
            }
        }
    }


    // If we reversed the direction at the start, reverse back now.
    if(direction==0)
    {
        mfem::Array<int> temp_int;
        mfem::Array<double> temp_double;
        for(int k=0;k<fractions.Size();++k)
        {
            temp_int.Append(elem_ids[elem_ids.Size()-k-1]);
            temp_double.Append(fractions[elem_ids.Size()-k-1]);
        }
        for(int k=0;k<fractions.Size();++k)
        {
            elem_ids[k] = temp_int[k];
            fractions[k] = temp_double[k];
        }
    }

    return 0.;

}

int TraceBackMesh::FindPoints(const mfem::Vector& x,
                              const int& start_id,
                              int& final_id,
                              double tol)
{
    // We find the point using the same algorithm as we use for SplitLineInElements.

    mfem::Vector cent(Dimension());
    GetElementCenter(start_id,cent);

    mfem::Array<int> elem_ids;
    mfem::Array<double> fractions;

    SplitLineInElements3(cent,x,start_id,elem_ids,fractions, tol);

    //elem_ids.Print(std::cout);
    //fractions.Print(std::cout);
    final_id = elem_ids.Last();
    return 1;
}
