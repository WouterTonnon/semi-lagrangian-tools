/*
Copyright 2021 W.R. Tonnon

This file is part of Semi-Lagrangian Tools
Definition of smoothening discrete differential forms to make them
pointwise-defined.

Copyright (C) <2021>  <W.R. Tonnon>

Semi-Lagrangian Tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Semi-Lagrangian Tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "VertexValuedGridFunction.h"
#include "Operators.h"

mfem::Vector PointWiseGridFunction::evalPointWise(mfem::Vector x, int ind, double tol, bool vertex)
{
    mfem::mfem_error("PointWiseGridFunction::evalPointWise(): Not Implemented for this class.");
    return x;
}


mfem::Vector SmoothenedPointWiseGridFunction::evalPointWise(mfem::Vector x, int elem_id, double tol, bool vertex) {
    // We will need this later
    mfem::Array<int> elem_ids;
    mfem::Array<mfem::IntegrationPoint> ips;
    mfem::Vector out(x.Size());
    out = 0.;

    // We need to invert an ElementTranformation later
    mfem::InverseElementTransformation* inv_tr = new mfem::InverseElementTransformation();
    inv_tr->SetElementTol(tol);
    inv_tr->SetReferenceTol(tol);
    inv_tr->SetPhysicalRelTol(tol);

    // We prepare a quadrature rule for lines
    mfem::IntegrationRules IntRules(0,mfem::Quadrature1D::GaussLegendre);
    mfem::IntegrationRule ir = IntRules.Get(mfem::Geometry::SEGMENT, order);
    mfem::Array<double> weights = ir.GetWeights();

    // Loop over all direction (x-, y-, and z-direction)
    for (int q = 0; q < x.Size(); ++q) {
        // Consider a line along dimension 'q' of length equal to the meshwidth
        mfem::Vector x0(x), x1(x);
        x0.Elem(q) += -.5 * h_max;
        x1.Elem(q) += .5 * h_max;

        // Find the elements that contain the endpoints of the line
        elem_ids.SetSize(2);
        mesh.FindPoints(x0, elem_id, elem_ids[0],tol*.01);
        mesh.FindPoints(x1, elem_id, elem_ids[1],tol*.01);

        // Split the line in parts that are all contained in a unique element.
        mfem::Array<int> elems_line;
        mfem::Array<double> fractions;
        mesh.SplitLineInElements3(x0, x1, elem_ids[0], elem_ids[1], elems_line, fractions, tol);

        // Loop over all the parts of the line that are contained in a unique element
        for (int j = 0; j < elems_line.Size(); ++j) {
            // perform quadrature over this part of the line
            for(int l=0; l<ir.GetNPoints(); ++l) {
                // Find the quadrature point in physical space
                double frac = 0;
                for (int k = 0; k < j; ++k) frac += fractions[k];
                frac += ir.IntPoint(l).x * fractions[j];
                mfem::Vector p = x0;
                p *= -1.;
                p += x1;
                p *= frac;
                p += x0;

                // Determine the value of the function that needs to be evaluated.
                mfem::Vector vec;
                if (elems_line[j] == -1)
                    // The point lies outside the mesh. Apply boundary conditions.
                {
                    DirichletBoundaryCondition(p, vec);
                    if(isnan(vec.Elem(0))) {
                        vec = 0.;
                    }
                } else
                    // The point lies inside a mesh element. Determine the vector value from the given gridfunction.
                {
                    mfem::IntegrationPoint ip;
                    inv_tr->SetTransformation(*mesh.GetElementTransformation(elems_line[j]));
                    inv_tr->SetReferenceTol(10000*tol);
                    int res = inv_tr->Transform(p, ip);
                    inv_tr->SetReferenceTol(tol);
                    if (res != mfem::InverseElementTransformation::Inside)
                        mfem::mfem_error(
                                "VectorTraceBackGridFunction::TraceBackAndProjectGridFunctionPerElement(): Unexpected behaviour.");
                    GetVectorValue(elems_line[j], ip, vec);
                }

                // Get the appropriate weight and add to the integral.
                out.Elem(q) += weights[l] * fractions[j] * vec.Elem(q);
            }
        }
    }

    // Can be used to enforce the known boundary conditions (such as normal BCs), but this is problem dependent.
    out = param.enforceBCs(x,out, tol);

    //Clean up
    delete inv_tr;

    return out;

}

mfem::Vector SmoothenedPointWiseGridFunction::evalAtVertex(int i, double tol)
{
    // Get vertex 'i' and store it in mfem::Vector format
    double* vertex = mesh.GetVertex(i);
    mfem::Vector x(mesh.Dimension());
    for(int j=0; j<mesh.Dimension(); ++j)
        x.Elem(j) = vertex[j];

    // Find elements that are close to the vertex
    if(!vtoel) vtoel = mesh.GetVertexToElementTable();
    mfem::Array<int> elements;
    vtoel->GetRow(i,elements);

    // Evaluate the function at the vertex
    return evalPointWise(x, elements[0], tol, true);

}

mfem::Vector RandomPointWiseGridFunction::evalPointWise(mfem::Vector x, int elem_id, double tol, bool vertex) {
    // We will need this later
    mfem::Array<int> elem_ids;
    mfem::Array<mfem::IntegrationPoint> ips;
    mfem::Vector out(x.Size());
    out = 0.;

    // We will need to invert an ElementTransformation later
    mfem::InverseElementTransformation* inv_tr = new mfem::InverseElementTransformation();
    inv_tr->SetElementTol(tol);
    inv_tr->SetReferenceTol(tol);
    inv_tr->SetPhysicalRelTol(tol);

    // Find an element that contains 'x' (this could be multiple, but we just take the one that we get by FindPoints)
    int final_id;
    mesh.FindPoints(x,elem_id,final_id);

    // Take the value of the function with respect to the found element and at point 'x'
    if (final_id == -1)
        // The point lies outside the mesh.. Apply boundary conditions.
    {
        DirichletBoundaryCondition(x, out);
    } else
        // The point lies inside a mesh element. Determine the vector value from the given gridfunction.
    {
        mfem::IntegrationPoint ip;
        inv_tr->SetTransformation(*mesh.GetElementTransformation(final_id));
        int res = inv_tr->Transform(x, ip);
        if (res != mfem::InverseElementTransformation::Inside)
            mfem::mfem_error(
                    "VectorTraceBackGridFunction::TraceBackAndProjectGridFunctionPerElement(): Unexpected behaviour.");
        GetVectorValue(final_id, ip, out);
    }

    // Can be used to enforce the known boundary conditions (such as normal BCs), but this is problem dependent.
    out = param.enforceBCs(x,out, tol);

    // Clean up
    delete inv_tr;

    return out;
}

mfem::Vector RandomPointWiseGridFunction::evalAtVertex(int i, double tol)
{
    // Get vertex 'i' and store it in mfem::Vector format
    double* vertex = mesh.GetVertex(i);
    mfem::Vector x(mesh.Dimension());
    for(int j=0; j<mesh.Dimension(); ++j)
        x.Elem(j) = vertex[j];

    // Find elements that are close to the vertex
    if(!vtoel) vtoel = mesh.GetVertexToElementTable();
    mfem::Array<int> elements;
    vtoel->GetRow(i,elements);

    // Evaluate the function at the vertex
    return evalPointWise(x, elements[0], tol, true);
}
