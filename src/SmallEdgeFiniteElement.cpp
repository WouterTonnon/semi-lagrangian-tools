/*
Copyright 2021 W.R. Tonnon

This file is part of Semi-Lagrangian Tools
An implementation of discrete differential forms based on work by Francesca
Rapetti and Alain Bossavit.

Copyright (C) <2021>  <W.R. Tonnon>

Semi-Lagrangian Tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Semi-Lagrangian Tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "SmallEdgeFiniteElement.h"
#include <numeric>

template<mfem::Geometry::Type geom>
void Whitney_TriangleElement<geom>::PrecomputeIndAndRot()
{
    /*
     * Precomputes the index and orientation of the degreees of freedom. Recall that each degree of freedom
     * is associated with an edge and a permutation of a k-map.
     */
    indices.resize(edges.size(), std::vector<int>(permutations.size()));
    rots.resize(edges.size(), std::vector<int>(permutations.size()));

    // Loop over the DoFs associated with edges
    for(int i=0; i<edges.size(); ++i)
    {
        // Loop over the DoFs associated with permuations of k-maps.
        for(int j=0; j<permutations.size(); ++j)
        {
            int rot;
            int index = GetIndAndRot(i,j,rot);
            indices.at(i).at(j) = index;
            rots.at(i).at(j) = rot;
        }
    }
    precomputedIndAndRot = true;
}


template<mfem::Geometry::Type geom>
int Whitney_TriangleElement<geom>::GetIndAndRot(int edge, int perm, int& rot) const
{
    /*
     * Computes the local numbering of the DoF associated to edge "edge" and k-map "permutation".
     * Also computes the orientation of the edge with respect to the associated face
     */

    // If already precomputed, we are done.
    if(precomputedIndAndRot)
    {
        rot = rots.at(edge).at(perm);
        return indices.at(edge).at(perm);
    }

    // Find vertex indices associated with edge
    int v0, v1, v2;
    v0 = mfem::Geometry::Constants<geom>::Edges[edge][0];
    v1 = mfem::Geometry::Constants<geom>::Edges[edge][1];

    // Default value of the orientation is 1
    rot = 1;

    // The DoF is associated to the edge
    if(permutations.at(perm).at(v0) + permutations.at(perm).at(v1)==k){
        return p*edge + permutations.at(perm).at(v1);
    }

    // We have to distinguish between 2D and 3D
    if(GetDim()==3) {
        // Loop over all faces
        for (int i = 0; i < mfem::Geometry::NumFaces[geom]; ++i) {
            std::vector<int> face_vert(0);
            std::vector<int> found(3, 0);

            // Check which edge vertices are also vertices of the considered face
            for(int j = 0; j < 3; ++j){
                face_vert.push_back(mfem::Geometry::Constants<mfem::Geometry::TETRAHEDRON>::FaceVert[i][j]);
                if(face_vert.at(j)==v0){
                    found.at(j) = 1;
                }
                else if(face_vert.at(j)==v1){
                    found.at(j) = 2;
                }
            }

            // Determine the orientation of the edge w.r.t. the associated face
            if(std::accumulate(found.begin(),found.end(),0)==3){
                bool found_or = false;
                int j = 0;
                while(!found_or){
                    if(found.at(j)) {
                        found_or = true;
                        if(found.at(j)==1 & found.at((j+1)%3)==2) rot = 1;
                        else rot = -1;
                    }
                    j++;
                }
            }


            // Check if both edge vertices are also vertices of the considered face => edge must be an edge
            // of the face
            if(std::accumulate(found.begin(),found.end(),0)==3){
                // Find the number of the edge wrt the associated face
                auto index = (std::distance(found.begin(),std::find(found.begin(),found.end(),0))+1)%3;

                // Sum all k-map integers corresponding to the vertices of the face
                int sum = 0;
                for(int j=0; j<3; ++j) sum += permutations.at(perm).at(face_vert.at(j));

                // if all the k-map integers corresponding to the vertices are zero, the DoF is on a face
                if(sum==k){
                    // Compute the appropriate index
                    int q = permutations.at(perm).at(v0)+permutations.at(perm).at(v1);
                    int g = 0;
                    for(int l=0; l<q; ++l){
                        g += l+1;
                    }
                    g += permutations.at(perm).at(v1);
                    int z = 0;
                    for(int l=0; l<k; ++l) z += l+1;
                    return p*6 + i*3*z + index*z + g;
                }
            }
        }

        // The DoF is associated with the interior of the tetrahedron.
        int z = 0;
        for(int i=0; i<k; ++i) z += i+1;
        return p*6 + 4*3*z + InnerDofs.at(edge).at(perm);
    }
    else if(dim==2){
        // The DoF is associated with the interior of the triangle.
        int q = permutations.at(perm).at(v0)+permutations.at(perm).at(v1);
        int g = 0;
        for(int i=0; i<q; ++i){
            g += i+1;
        }
        g += permutations.at(perm).at(v1);
        int z = 0;
        for(int i=0; i<k; ++i) z += i+1;
        return g+edge*z+p*3;
    }

    return -999999999;
}

template<mfem::Geometry::Type geom>
void Whitney_TriangleElement<geom>::Project(mfem::VectorCoefficient &vc,
                                      mfem::ElementTransformation &Trans,
                                      mfem::Vector &dofs_out) const
{

    mfem::IntegrationRules IntRules(0, mfem::Quadrature1D::GaussLegendre);
    mfem::IntegrationRule ir = IntRules.Get(mfem::Geometry::SEGMENT, 8);
    mfem::Array<double> weights = ir.GetWeights();

    // First we compute the average along the (small) Edges
    // We need to transform the DoFs in the reference element to lines in physical space, before we can integrate over them.
    mfem::Vector local_averages(dofs.size()), local_vals(dofs.size());
    // Loop over all DoFs of the element
    for (int l = 0; l < dofs.size(); ++l) {
        std::vector<mfem::Vector> global_dofs;
        // Now we need to integrate over the small edges in physical space.
        double val = 0;
        // Loop over all quadrature points
        for (int q = 0; q < ir.GetNPoints(); ++q) {
            std::vector<mfem::Vector> global_dofs;

            // Loop over the vertices of the DoF (small edge).
            for (int k = 0; k < 2; ++k) {
                // Transform from mfem::Vector type to mfem::IntegrationPoint type.
                mfem::IntegrationPoint ip;
                ip.x = dofs.at(l).at(k).Elem(0);
                ip.y = dofs.at(l).at(k).Elem(1);
                if (GetDim() == 3) ip.z = dofs.at(l).at(k).Elem(2);

                // Transform the vertex to a vertex in physical space.
                mfem::Vector global_dof(GetDim());
                Trans.SetIntPoint(&ip);
                Trans.Transform(ip, global_dof);

                // Store the global location of the vertex
                global_dofs.push_back(global_dof);
            }

            // Determine the point in physical space that corresponds to the quadrature point
            double frac = ir.IntPoint(q).x;
            mfem::Vector p = dofs.at(l).at(1);
            p -= dofs.at(l).at(0);
            p *= frac;
            p += dofs.at(l).at(0);

            // Evaluate the function at the quadrature point
            mfem::IntegrationPoint ip;
            ip.Set(p.GetData(),GetDim());
            mfem::Vector vec;
            vec.SetSize(GetDim());
            vc.Eval(vec,Trans,ip);

            // We set tangent := x1-x0;
            mfem::Vector tangent(GetDim());
            tangent = global_dofs.at(1);
            tangent -= global_dofs.at(0);

            // We need the tangential component, so we multiply the output of the function with the tangent.
            double prod;
            if (  GetMapType() == mfem::FiniteElement::MapType::H_CURL) {
                prod = tangent * vec;
            } else
                mfem::mfem_error(
                        "VectorTraceBackGridFunction::TraceBackAndProjectGridFunctionPerElementSmallEdges(): Only implemented for Whitney elements.");

            // Multiply with the appropriate weight and add to the integral
            val += weights[q] * prod;

        }
        // Store the value in a vector with local values
        local_averages.Elem(l) = val;
    }

    // Now we transform these averages to the DoFs
    dofs_out.SetSize(dofs.size());
    TransformAverageToBasis(local_averages,dofs_out);
}

void GetPermutations(int NumBalls, int NumBoxes, std::vector<std::vector<int>>& data)
{
    /*
     * Computes all the possibilities to put NumBalls balls in NumBoxes boxes.
     */

    if(data.size()==0) data.push_back(std::vector<int>(0));

    if(NumBoxes>1)
    {
        std::vector<int> backup(data.back());
        for(int i=0; i<=NumBalls; ++i)
        {
            data.back().push_back(NumBalls-i);
            GetPermutations(i,NumBoxes-1,data);
            if(i!=NumBalls) data.push_back(backup);
        }
    }
    else
    {
        data.back().push_back(NumBalls);
    }

}



int factorial(int n)
{
// Compute the factorial recursively
if(n<0) mfem::mfem_error("factorial(): n cannot be smaller than 0");
return (n==1 || n==0) ? 1: n * factorial(n - 1);
}

mfem::Vector cross(const mfem::Vector& A, const mfem::Vector& B)
{
    /*
     * Compute the cross product. Consider the following cases
     * A & B are 2-dimensional: cross(A,B) outputs a 1-dimensional vector
     * A & B are 3-dimensional: cross(A,B) outputs a 3-dimensional vector
     * Combinations are possible.
     */

    mfem::Vector Ap(3), Bp(3);
    Ap = 0.;
    Bp = 0.;

    if(A.Size()==1) Ap.Elem(2) = A.Elem(0);
    if(B.Size()==1) Bp.Elem(2) = B.Elem(0);
    if(A.Size()==2) {
        Ap.Elem(0) = A.Elem(0);
        Ap.Elem(1) = A.Elem(1);
    }
    if(B.Size()==2){
        Bp.Elem(0) = B.Elem(0);
        Bp.Elem(1) = B.Elem(1);
    }
    if(A.Size()==3) Ap = A;
    if(B.Size()==3) Bp = B;

    mfem::Vector out(3);
    out.Elem(0) = Ap.Elem(1)*Bp.Elem(2)-Ap.Elem(2)*Bp.Elem(1);
    out.Elem(1) = Ap.Elem(2)*Bp.Elem(0)-Ap.Elem(0)*Bp.Elem(2);
    out.Elem(2) = Ap.Elem(0)*Bp.Elem(1)-Ap.Elem(1)*Bp.Elem(0);
    return out;
}


template<mfem::Geometry::Type geom>
void Whitney_TriangleElement<geom>::CalcVShape(const mfem::IntegrationPoint &ip, mfem::DenseMatrix &shape) const {
    /*
     * Computes the value of the Shape Functions at point ip and stores them in shape.
     */


    // This element is constructed from lowest-order Whitney elements, so we evaluate those first
    mfem::DenseMatrix W1;
    if(dim==2) W1.SetSize(3,2);
    else W1.SetSize(6,3);
    mfem::Vector W0(GetDim()+1);
    if(dim==2) {
        w1_2d.CalcVShape(ip, W1);
        w0_2d.CalcShape(ip, W0);
    }
    else{
        w1_3d.CalcVShape(ip, W1);
        w0_3d.CalcShape(ip, W0);
    }

    // Set the appropriate size of shape
    shape.SetSize(GetDof(),GetDim());

    mfem::Vector row(W1.Width());
    // Loop over edges (every DoF is associated with an edge and k-map)
    for(int i=0; i<W1.NumRows(); ++i) {
        // Loop over k-maps (every DoF is associated with an edge and k-map)
        for (int j=0; j<permutations.size(); ++j) {
            int orientation;

            // Find the right local DoF index
            int row_ind = GetIndAndRot(i,j, orientation);

            // Initialize the rows with the 1st order whitney element and fix the orientation
            W1.GetRow(i,row);
            row *= orientation;
            shape.SetRow(row_ind, row);

            // Multiply with the appropriate 0-form Whitney elements according to the associated k-map
            for(int k=0; k<GetDim()+1; ++k) // Loop over vertices
            {
                if(permutations.at(j).at(k)!=0) {
                    for (int q = 0; q < GetDim(); ++q) // Loop over vector components
                        shape.Elem(row_ind, q) *= pow(W0.Elem(k), double(permutations.at(j).at(k)));
                }
            }
        }
    }
}

template<mfem::Geometry::Type geom>
void Whitney_TriangleElement<geom>::CalcCurlShape(const mfem::IntegrationPoint &ip,
                   mfem::DenseMatrix &curl_shape) const
{
    if(p==1)
    {
        // For first order, we can simply use MFEM's implemented discrete differential forms.
        if(dim==2) {
            curl_shape.SetSize(3,1);
            w1_2d.CalcCurlShape(ip, curl_shape);
        }
        else{
            curl_shape.SetSize(6,3);
            w1_3d.CalcCurlShape(ip, curl_shape);
        }
    }
    else if(p==2)
    {
        // For reasons of computational complexity, all required curls were computed using SymPy. We simply fill them in here.
        // Note that this only works for second order
        if(dim==2) {
            mfem::DenseMatrix curl_shape_temp;
            curl_shape_temp.SetSize(9, 1);
            curl_shape_temp.Elem(0, 0) = 3. - 3 * ip.y - 3 * ip.x;
            curl_shape_temp.Elem(1, 0) = 3 * ip.x;
            curl_shape_temp.Elem(2, 0) = 3 * ip.y - 1;
            curl_shape_temp.Elem(3, 0) = 2 - 3 * ip.y - 3 * ip.x;
            curl_shape_temp.Elem(4, 0) = 3 * ip.x;
            curl_shape_temp.Elem(5, 0) = 3 * ip.y;
            curl_shape_temp.Elem(6, 0) = 3 - 3 * ip.y - 3 * ip.x;
            curl_shape_temp.Elem(7, 0) = 3 * ip.x - 1;
            curl_shape_temp.Elem(8, 0) = 3 * ip.y;

            curl_shape.SetSize(9, 1);
            for (int i = 0; i < 3; ++i) {
                for (int j = 0; j < 3; ++j) {
                    int rot;
                    int ind = GetIndAndRot(i, j, rot);
                    curl_shape.Elem(ind, 0) = rot * curl_shape_temp.Elem(3 * i + j, 0);
                }
            }
        }
        else{
            mfem::DenseMatrix curl_shape_temp;
            curl_shape_temp.SetSize(24, 3);
            curl_shape_temp.Elem(0, 0) = 0;
            curl_shape_temp.Elem(1, 0) = 0;
            curl_shape_temp.Elem(2, 0) = ip.x;
            curl_shape_temp.Elem(3, 0) = -ip.x;
            curl_shape_temp.Elem(4, 0) = 3 - 3*ip.y - 3*ip.z - 3*ip.x;
            curl_shape_temp.Elem(5, 0) = 2*ip.x;
            curl_shape_temp.Elem(6, 0) = 3*ip.y;
            curl_shape_temp.Elem(7, 0) = ip.x + 3*ip.z - 1;
            curl_shape_temp.Elem(8, 0) = 3*ip.x + 3*ip.y + 3*ip.z - 3;
            curl_shape_temp.Elem(9, 0) = -2*ip.x;
            curl_shape_temp.Elem(10, 0) = 1 - 3*ip.y - ip.x;
            curl_shape_temp.Elem(11, 0) = -3*ip.z;
            curl_shape_temp.Elem(12, 0) = ip.x;
            curl_shape_temp.Elem(13, 0) = 0;
            curl_shape_temp.Elem(14, 0) = 0;
            curl_shape_temp.Elem(15, 0) = -ip.x;
            curl_shape_temp.Elem(16, 0) = -ip.x;
            curl_shape_temp.Elem(17, 0) = 0;
            curl_shape_temp.Elem(18, 0) = ip.x;
            curl_shape_temp.Elem(19, 0) = 0;
            curl_shape_temp.Elem(20, 0) = 2 - 3*ip.y - 3*ip.z - 2*ip.x;
            curl_shape_temp.Elem(21, 0) = 2*ip.x;
            curl_shape_temp.Elem(22, 0) = 3*ip.y;
            curl_shape_temp.Elem(23, 0) = 3*ip.z;

            curl_shape_temp.Elem(0, 1) = 3*ip.x + 3*ip.y + 3*ip.z - 3;
            curl_shape_temp.Elem(1, 1) = -3*ip.x;
            curl_shape_temp.Elem(2, 1) = -2*ip.y;
            curl_shape_temp.Elem(3, 1) = 1 - 3*ip.z - ip.y;
            curl_shape_temp.Elem(4, 1) = 0;
            curl_shape_temp.Elem(5, 1) = -ip.y;
            curl_shape_temp.Elem(6, 1) = 0;
            curl_shape_temp.Elem(7, 1) = ip.y;
            curl_shape_temp.Elem(8, 1) = 3 - 3*ip.y - 3*ip.z - 3*ip.x;
            curl_shape_temp.Elem(9, 1) = 3*ip.x + ip.y - 1;
            curl_shape_temp.Elem(10, 1) = 2*ip.y;
            curl_shape_temp.Elem(11, 1) = 3*ip.z;
            curl_shape_temp.Elem(12, 1) = ip.y;
            curl_shape_temp.Elem(13, 1) = 0;
            curl_shape_temp.Elem(14, 1) = 0;
            curl_shape_temp.Elem(15, 1) = -ip.y;
            curl_shape_temp.Elem(16, 1) = 3*ip.x + 2*ip.y + 3*ip.z - 2;
            curl_shape_temp.Elem(17, 1) = -3*ip.x;
            curl_shape_temp.Elem(18, 1) = -2*ip.y;
            curl_shape_temp.Elem(19, 1) = -3*ip.z;
            curl_shape_temp.Elem(20, 1) = ip.y;
            curl_shape_temp.Elem(21, 1) = -ip.y;
            curl_shape_temp.Elem(22, 1) = 0;
            curl_shape_temp.Elem(23, 1) = 0;

            curl_shape_temp.Elem(0, 2) = 3 - 3*ip.y - 3*ip.z - 3*ip.x;
            curl_shape_temp.Elem(1, 2) = 3*ip.x;
            curl_shape_temp.Elem(2, 2) = 3*ip.y + ip.z - 1;
            curl_shape_temp.Elem(3, 2) = 2*ip.z;
            curl_shape_temp.Elem(4, 2) = 3*ip.x + 3*ip.y + 3*ip.z - 3;
            curl_shape_temp.Elem(5, 2) = 1 - ip.z - 3*ip.x;
            curl_shape_temp.Elem(6, 2) = -3*ip.y;
            curl_shape_temp.Elem(7, 2) = -2*ip.z;
            curl_shape_temp.Elem(8, 2) = 0;
            curl_shape_temp.Elem(9, 2) = ip.z;
            curl_shape_temp.Elem(10, 2) = -ip.z;
            curl_shape_temp.Elem(11, 2) = 0;
            curl_shape_temp.Elem(12, 2) = 2 - 3*ip.y - 2*ip.z - 3*ip.x;
            curl_shape_temp.Elem(13, 2) = 3*ip.x;
            curl_shape_temp.Elem(14, 2) = 3*ip.y;
            curl_shape_temp.Elem(15, 2) = 2*ip.z;
            curl_shape_temp.Elem(16, 2) = -ip.z;
            curl_shape_temp.Elem(17, 2) = 0;
            curl_shape_temp.Elem(18, 2) = ip.z;
            curl_shape_temp.Elem(19, 2) = 0;
            curl_shape_temp.Elem(20, 2) = ip.z;
            curl_shape_temp.Elem(21, 2) = -ip.z;
            curl_shape_temp.Elem(22, 2) = 0;
            curl_shape_temp.Elem(23, 2) = 0;


            curl_shape.SetSize(24, 3);
            for (int i = 0; i < 6; ++i) {
                for (int j = 0; j < 4; ++j) {
                    int rot;
                    int ind = GetIndAndRot(i, j, rot);
                    curl_shape.Elem(ind, 0) = rot * curl_shape_temp.Elem(4 * i + j, 0);
                    curl_shape.Elem(ind, 1) = rot * curl_shape_temp.Elem(4 * i + j, 1);
                    curl_shape.Elem(ind, 2) = rot * curl_shape_temp.Elem(4 * i + j, 2);
                }
            }
        }
    }
    else mfem::mfem_error("Whitney_TriangleElement::CalcCurlShape(): Only first- and second-order currently implemented.");

}

template<mfem::Geometry::Type geom>
void Whitney_TriangleElement<geom>::CalcVShape (mfem::ElementTransformation &Trans, mfem::DenseMatrix &shape) const
{
   CalcVShape(Trans.GetIntPoint(), vshape);
   Mult(vshape, Trans.InverseJacobian(), shape);
}

template<mfem::Geometry::Type geom>
void Whitney_TriangleElement<geom>::FindDofs(std::vector<std::vector<mfem::Vector>> &dofs) {
    /*
     * Find all DoFs
     */

    // Choose the required size
    dofs.resize(edges.size()*permutations.size());

    // Loop over edges (every DoF is associated with an edge and k-map)
    for(int i=0; i<edges.size(); ++i){
        // Loop over k-maps (every DoF is associated with an edge and k-map)
        for(int j=0; j<permutations.size(); ++j){
            std::vector<mfem::Vector> dof;

            // Loop over the vertices of the edge
            for(int q=0; q<2; ++q){
                // Store vertex q in convenient format
                mfem::Vector dof_vertex(dim);
                dof_vertex.Elem(0) = edges.at(i).at(q).Elem(0);
                dof_vertex.Elem(1) = edges.at(i).at(q).Elem(1);
                if(dim==3) dof_vertex.Elem(2) = edges.at(i).at(q).Elem(2);

                // Apply the appropriate contraction mappings as described in Rapetti (2014)
                for(int k=0; k<vertices.size(); ++k){
                    double fac = 1.;
                    for(int g=0; g<k; ++g) fac += permutations.at(j).at(g);
                    fac = fac/(fac+permutations.at(j).at(k));
                    dof_vertex.Elem(0) = (dof_vertex.Elem(0)-vertices.at(k).Elem(0))*fac + vertices.at(k).Elem(0);
                    dof_vertex.Elem(1) = (dof_vertex.Elem(1)-vertices.at(k).Elem(1))*fac + vertices.at(k).Elem(1);
                    if(dim==3) dof_vertex.Elem(2) = (dof_vertex.Elem(2)-vertices.at(k).Elem(2))*fac + vertices.at(k).Elem(2);
                }

                // Store the vertex
                dof.push_back(dof_vertex);
            }

            // Obtain local DoF index and orientation
            int orientation;
            int row_ind = GetIndAndRot(i,j,orientation);

            // Reverse direction DoF if needed
            std::vector<mfem::Vector> dof_reverse;
            if(orientation==-1) {
                dof_reverse.push_back(dof.at(1));
                dof_reverse.push_back(dof.at(0));
            }

            // Store either the normal or reversed DoF.
            if(orientation==1) dofs.at(row_ind) = dof;
            else dofs.at(row_ind) = dof_reverse;

        }
    }
}

template<mfem::Geometry::Type geom>
void Whitney_TriangleElement<geom>::FindProjectionmatrix(mfem::DenseMatrix& EdgeToDofs) {
    /*
     * Find transpose of matrix A as described in Rapetti (2014)
     */

    // Find a quadrature rule with required order
    mfem::IntegrationRules IntRules(0,mfem::Quadrature1D::GaussLegendre);
    mfem::IntegrationRule ir = IntRules.Get(mfem::Geometry::SEGMENT, p);
    mfem::Array<double> weights = ir.GetWeights();

    // Initialize required matrices
    mfem::DenseMatrix shape(GetDof(),GetDim());
    EdgeToDofs.SetSize(GetDof(),GetDof());
    EdgeToDofs = 0.;

    // Loop over the DoFs (1-simplices)
    for(int i=0; i<dofs.size(); ++i){
        // Loop over quadrature points
        for(int q=0; q<ir.GetNPoints(); ++q)
        {
            // Reformat quadrature points to something MFEM understands
            mfem::IntegrationPoint IntPoint;
            if(dim==2) {
                IntPoint.Set2(
                        dofs.at(i).at(0).Elem(0) * (1 - ir.IntPoint(q).x) + dofs.at(i).at(1).Elem(0) * ir.IntPoint(q).x,
                        dofs.at(i).at(0).Elem(1) * (1 - ir.IntPoint(q).x) + dofs.at(i).at(1).Elem(1) * ir.IntPoint(q).x);
            }
            else{
                IntPoint.Set3(
                        dofs.at(i).at(0).Elem(0) * (1 - ir.IntPoint(q).x) + dofs.at(i).at(1).Elem(0) * ir.IntPoint(q).x,
                        dofs.at(i).at(0).Elem(1) * (1 - ir.IntPoint(q).x) + dofs.at(i).at(1).Elem(1) * ir.IntPoint(q).x,
                        dofs.at(i).at(0).Elem(2) * (1 - ir.IntPoint(q).x) + dofs.at(i).at(1).Elem(2) * ir.IntPoint(q).x);
            }

            // Obtain shape functions evaluated at the quadrature point
            CalcVShape(IntPoint,shape);

            // Loop over the local shape functions
            for(int j=0; j<dofs.size(); ++j){
                // Multiply the shape functions with the tangent of the DoF and the quadrature weight.
                EdgeToDofs.Elem(i,j) += (shape.Elem(j,0)*(dofs.at(i).at(1).Elem(0)-dofs.at(i).at(0).Elem(0))
                                      +  shape.Elem(j,1)*(dofs.at(i).at(1).Elem(1)-dofs.at(i).at(0).Elem(1)))*weights[q];
                if(dim==3)
                    EdgeToDofs.Elem(i,j) += shape.Elem(j,2)*(dofs.at(i).at(1).Elem(2)-dofs.at(i).at(0).Elem(2))*weights[q];
            }
        }
    }
}

template<mfem::Geometry::Type geom>
void Whitney_TriangleElement<geom>::TransformAverageToBasis(const mfem::Vector& in, mfem::Vector& out) const {
    if(p==1){
        out = in;
    }
    else {
        int num_faces, num_edges;
        if (geom == mfem::Geometry::TETRAHEDRON) {
            num_faces = 4;
            num_edges = 6;
        } else if (geom == mfem::Geometry::TRIANGLE) {
            num_faces = 1;
            num_edges = 3;
        }

        for (int i = 0; i < num_edges; ++i)
            EdgeToDofs_edge.Mult(in.GetData() + 2 * i, out.GetData() + 2 * i);

        mfem::Vector in_adjusted(in.GetData() + num_edges * 2, num_faces * 3);
        DofsEdgeToFace.AddMult_a(-1., mfem::Vector(out.GetData(), num_edges * 2), in_adjusted);

        for (int i = 0; i < num_faces; ++i)
            EdgeToDofs_face.Mult(in_adjusted.GetData() + i * 3, out.GetData() + num_edges * 2 + i * 3);
    }
}

template class Whitney_TriangleElement<mfem::Geometry::TRIANGLE>;
template class Whitney_TriangleElement<mfem::Geometry::TETRAHEDRON>;
