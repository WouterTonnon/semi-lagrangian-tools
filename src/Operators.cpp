/*
Copyright 2021 W.R. Tonnon

This file is part of Semi-Lagrangian Tools
Operators to define advection of discrete differential 1-forms.

Copyright (C) <2021>  <W.R. Tonnon>

Semi-Lagrangian Tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Semi-Lagrangian Tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Operators.h"
#include "mfem.hpp"
#include "VertexValuedGridFunction.h"
#include "TraceBackMesh.h"
#include <iostream>
#include <functional>
#include <set>




void VectorTraceBackGridFunction::TraceBackAndProjectGridFunctionPerElement(mfem::GridFunction& coeff, const double current_t, const double dt)
{
    if(fes->GetFE(0)->GetMapType()!= coeff.FESpace()->GetFE(0)->GetMapType()) mfem::mfem_error("VectorTraceBackGridFunction::TraceBackAndProjectGridFunctionPerElement(): FiniteElementSpaces are not the same!");
    double zero = 100. * std::numeric_limits<double>::epsilon();
    TraceBackMesh mesh(*fes->GetMesh(),false);
    mfem::Array<int> dof, vertices, edge_inds;
    mfem::Array<double> A_array;
    fes->BuildDofToArrays();
    int dim = mesh.Dimension();
    mfem::DenseMatrix A(dim, 1);
    mfem::Vector x(dim);
    int elem_id0, elem_id1;
    mfem::Array<mfem::IntegrationPoint> ips;
    double tol = 1e-9;



    const mfem::Table elem_to_edge = mesh.ElementToEdgeTable();

    mfem::InverseElementTransformation* inv_tr = new mfem::InverseElementTransformation();
    //inv_tr->SetElementTol(tol);
    inv_tr->SetReferenceTol(100*tol);
    inv_tr->SetPhysicalRelTol(100*tol);


    mesh.BuildEdgeToElementTable();
    const std::vector<std::vector<int>>* edge_to_element = mesh.GetEdgeToElementTable();

    double t = current_t;
    double var_dt = dt;

    // Initialize the traceback operator
    //LinearConvectionVelocity Oper(velocity, x, t);
    //ode_solver->Init(Oper);

    // We loop over the degrees of freedoms, that is, edges in this case
    for(int i=0; i<mesh.GetNEdges(); ++i) {
        //if(i%int(mesh.GetNEdges()/100)==0) {
        //    double prgrs = double(i) / double(mesh.GetNEdges());
        //    std::cout << "progress: " << prgrs << std::endl;
        //}

        fes->GetEdgeDofs(i, dof);
        if (dof.Size() != 1)
            mfem::mfem_error(
                    "VectorTraceBackGridFunction::TraceBackAndProjectGridFunctionPerElement(): Expected 1 dof, found a different amount.");

        // Find the element corresponding to the DoF-edge.
        int elem = fes->GetElementForDof(dof[0]);

        // Get the vertices of the edge
        mesh.GetEdgeVertices(i, vertices);
        mfem::Vector x0(dim), x1(dim);
        double* data_vert = mesh.GetVertex(vertices[0]);
        x0.Elem(0)=data_vert[0];
        x0.Elem(1)=data_vert[1];
        if(mesh.Dimension()==3) x0.Elem(2) = data_vert[2];
        data_vert = mesh.GetVertex(vertices[1]);
        x1.Elem(0)=data_vert[0];
        x1.Elem(1)=data_vert[1];
        if(mesh.Dimension()==3) x1.Elem(2) = data_vert[2];
        double len = x1.DistanceTo(x0);

        //std::cout << "x0 = ";
        //x0.Print(std::cout);
        //std::cout << "x1 = ";
        //x1.Print(std::cout);

        // Translate the coordinates using the given transformation
        mfem::Vector temp(x0), temp2(x1);
        /* Needs testing still!!!! */
        traceback_mapping(i,temp,x0);
        traceback_mapping(i,temp2,x1);

        // Save the vertices, because we will need them later
        mfem::Vector x0s(x0), x1s(x1);

        // Find the elements that contain the vertices
        //A.SetCol(0, x0);
        int num_pts_found0 = mesh.FindPoints(x0, elem, elem_id0, tol);
        //A.SetCol(0, x1);
        int num_pts_found1 = mesh.FindPoints(x1, elem, elem_id1, tol);

        // The traced back vertices can be in different elements. If so, we need to split the line integral from x0 to x1
        // into small parts that are contained in individual elements. We start with some boilerplate stuff.
        mfem::Array<int> elem_ids;
        mfem::Array<mfem::Vector*> abscissae;
        mfem::Array<double> fractions;
        mesh.SplitLineInElements3(x0,x1,elem_id0,elem_id1,elem_ids,fractions,tol);

        // elem_ids and fractions should have the same size.
        if(elem_ids.Size()!=fractions.Size())
            mfem::mfem_error(
                 "VectorTraceBackGridFunction::TraceBackAndProjectGridFunctionPerElement(): The size of elem_ids and fractions must be the same.");


        // Intermezzo: At this point in the algorithm, we traced back to vertices of the edges to their place in the mesh,
        // then we traced the line connecting x0 to x1 and split it in multiple parts. elem_ids contains for every part
        // of the line, the index of the element to which it belongs. fractions contains the fraction of the line that
        // is associated with the elemenet number as given in elem_ids.

        // It remains to integrate over the edge to obtain the correct value for the DoF.
        double val=0;
        for(int j=0; j<elem_ids.Size(); ++j)
        // We evaluate every line fraction separately, and use a midpoint rule to evaluate the integral. (exact for linear elements)
        {
            // Determine the right point on the line
            double frac=0;
            for(int k=0; k<j; ++k) frac+=fractions[k];
            frac+=.5*fractions[j];

            mfem::Vector p = x0;
            p*=-1.;
            p+=x1;
            p*=frac;
            p+=x0;


            mfem::Vector vec;
            if(elem_ids[j]==-1)
            // The point lies outside the mesh.. Apply boundary conditions.
            {
                DirichletBoundaryCondition(p, current_t -  dt,vec);
            }
            else
            // The point lies inside a mesh element. Determine the vector value from the given gridfunction.
            {
                mfem::IntegrationPoint ip;
                inv_tr->SetTransformation(*mesh.GetElementTransformation(elem_ids[j]));
                int res = inv_tr->Transform(p, ip);
                if(res != mfem::InverseElementTransformation::Inside) {
                    std::cout << "fractions.Sum()-1 = " << fractions.Sum()-1. << std::endl;
                    std::cout << "fractions:\n";
                    fractions.Print(std::cout);
                    std::cout << "elem_ids:\n";
                    elem_ids.Print(std::cout);
                    std::cout << "id = " << elem_ids[j] << ", p = ";
                    p.Print(std::cout);
                    std::cout << ", cell center = ";
                    mfem::Vector cent(mesh.Dimension());
                    mesh.GetElementCenter(elem_ids[j],cent);
                    cent.Print(std::cout);
                    mfem::mfem_error( "VectorTraceBackGridFunction::TraceBackAndProjectGridFunctionPerElement(): Unexpected behaviour.");
                }

                coeff.GetVectorValue(elem_ids[j], ip,vec);
            }

            // We set tangent := x1-x0;
            mfem::Vector tangent(dim);
            tangent = x1s;
            tangent -= x0s;
            double scaling = len/tangent.Norml2();
            //if(scaling<.5 | scaling>2.) std::cout << "scaling = " << scaling << std::endl;
            //tangent *= len/tangent.Norml2();

            // We compute innerprod := (vec,x1-x0);
            double prod;
            if(fes->GetFE(0)->GetMapType() == mfem::FiniteElement::MapType::H_CURL)
            {
                prod=tangent*vec;
            }
            else if(fes->GetFE(0)->GetMapType() == mfem::FiniteElement::MapType::H_DIV && dim==2)
            {
                mfem::mfem_warning("VectorTraceBackGridFunction::TraceBackAndProjectGridFunctionPerElement(): Using RT elements.");
                mfem::Vector normal(2);
                normal.Elem(0) = tangent.Elem(1);
                normal.Elem(1) = -tangent.Elem(0);
                prod = normal*vec;
            }
            else mfem::mfem_error("VectorTraceBackGridFunction::TraceBackAndProjectGridFunctionPerElement(): Not supported for RT elements in 3D.");

            // We add the value for this fraction to the integral.
            if(std::abs(scaling-1.)>.5)
            {
                //std::cout << "prod = " << prod << ", scaling = " << scaling << std::endl;
            }
            val+=fractions[j]*prod;
        }

        // We set the DoF to the value that we found.
        //std::cout << "coeff.Elem(i), val = " << coeff.Elem(i) << ", " << val << std::endl;
        Elem(i)=val;

        // Clean-up
        for(int j=0; j<abscissae.Size(); ++j) delete abscissae[j];
        elem_ids.DeleteAll();
        abscissae.DeleteAll();
        fractions.DeleteAll();
    }

    // clean up
    delete inv_tr;
}
