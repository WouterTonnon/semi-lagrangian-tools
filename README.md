# Building Semi-Lagrangian Tools

## Dependencies
Before installing, the following dependencies need to be installed.
### Fedora/RHEL
~~~
dnf install fontconfig-devel freetype-devel SDL2-devel glew-devel glm-devel libpng-devel libXi-devel boost-devel
~~~

### Ubuntu/Debian (apt-based)
~~~
apt-get install libfontconfig1-dev libfreetype-dev libsdl2-dev libglew-dev libglm-dev libpng-dev libxi-dev libboost-all-dev
~~~

### MacOS X (Homebrew)
~~~
brew install fontconfig freetype sdl2 glew glm libpng libxi boost
~~~

## Compiling the library and apps
Start by making a build directory. For example, from the source directory, use the following commands.
~~~
mdkir cmake-build-release
cd cmake-build-release
~~~
Initialize the build directory
~~~
cmake -DCMAKE_BUILD_TYPE=Release ..
~~~
Install the library and apps
~~~
make
~~~

# Running Semi-Lagrangian Tools
In this section we will assume that the Semi-Lagrangian Tools are installed on the system as described in the previous section. From root, navigate to the folder 'scripts'
~~~
cd ./scripts
~~~

## Advection of differential forms
To reproduce the plots as presented on the advection of differential forms, we first run the experiments from the folder 'scripts'
~~~
python3 run_all_advection.py
~~~
and then generate the plots
~~~
python3 plot_advection.py
~~~
The plots can be found png- and pgf-format in the folder
~~~
semi-lagrangian-tools/data/figures
~~~

## Incompressible Euler Equations
To reproduce the plots as presented on the incompressible Euler equations, we first run the experiments from the folder 'scripts'
~~~
python3 run_all_euler.py
~~~
and then generate the convergence plots
~~~
python3 plot_Euler.py
~~~
The plots can be found png- and pgf-format in the folder
~~~
semi-lagrangian-tools/data/figures
~~~
In order to generate the visual plots as displayed in fig. 2.5-2.10, run the following from project root.
~~~
./cmake-build-release/extern/glvis/glvis -run ./data/visualisation/p_3_par_1_0000_Euler_2ndOrder_Cons_V_0_i_2_r_<value_r>_dt_<value_dt>/p_3_par_1_0000_Euler_2ndOrder_Cons_V_0_i_2_r_<value_r<_dt_<value_dt>.glvs
~~~
where <value_r> is replaced by the number of uniform mesh-refinements are performed to obtain the mesh which is used to generate the solution to be visualized, <value_dt> is the timestep used to generate the solution to be visualised formatted as 0_0500000000 for dt=0.05. Note that the following {r,dt}-pairs are available using the standard generated test results.
| dt           | r |
|--------------|---|
| 0_0500000000 | 0 |
| 0_0250000000 | 1 |
| 0_0125000000 | 2 |
| 0_0062500000 | 3 |
| 0_0031250000 | 4 |
| 0_0015625000 | 5 |

Once the GUI opened, pressing spacebar gives the visualisation of the solution at the next timestep. Press 'h' to get an overview of the available commands in the command line. See also glvis.org for a more elaborate documentation.

Similarly, to reproduce fig. 2.16, we use the following command
~~~
./cmake-build-release/extern/glvis/glvis -run ./data/visualisation/p_21_par_0_0000_Euler_2ndOrder_NonCons_V_0_i_2_r_3_dt_0_0100000000/p_21_par_0_0000_Euler_2ndOrder_NonCons_V_0_i_2_r_3_dt_0_0100000000.glvs
~~~
Note that all relevant vtk files (for use with Paraview) for the Euler equations can be found in the subfolders of
~~~
semi-lagrangian-tools/data/visualisation
~~~
## Incompressible Magnetohydrodynamics Equations
To reproduce the plots as presented on the incompressible magnetohydrodynamics equations, we first run the experiments from the folder 'scripts'. We have three experiments that are all run using separate python scripts.
~~~
python3 run_exp_1_MHD.py
python3 run_exp_2_MHD.py
python3 run_exp_3_MHD.py
~~~
After running all these experiments, we generate the plots using the following single script.
~~~
python3 plot_MHD.py
~~~
In order to generate the visual plots for experiment 3 of the incompressible MHD equations, run the following from the project root.
~~~
./cmake-build-release/extern/glvis/glvis -run ./data/visualisation/p_22_par_0_0000_MHD_<scheme>_r_<value_r>_dt_<value_dt>/p_22_par_0_0000_MHD_<scheme>_r_<value_r>_dt_<value_dt>_<quantity>.glvs
~~~
where <value_r> is replaced by the number of uniform mesh-refinements are performed to obtain the mesh which is used to generate the solution to be visualized, <value_dt> is the timestep used to generate the solution to be visualised formatted as 0_0500000000 for dt=0.05, <quantity> denotes the value to be plotted with options 'A' (Magnetic field, curl A), 'L' (Lorentz force), 'u' (velocity field), and <scheme> denotes the scheme used with the following options
| scheme | <scheme>           |
|--------|--------------------|
| A      | scheme_A_E_V_0_i_2 |
| B      | scheme_B_V_0_i_2   |
| E      | scheme_A_E_V_1_i_2 |

Once the GUI opened, pressing spacebar gives the visualisation of the solution at the next timestep. Press 'h' to get an overview of the available commands in the command line. See also glvis.org for a more elaborate documentation.
