import os, re, pandas
import numpy as np
import matplotlib
matplotlib.use("pgf")
matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})
import matplotlib.pyplot as plt
import convergence_analysis as ca
import math

## Figure 2.2
# The options can be found in the .csv files in ../data/output
plots = [['mesh width $h$','L2 Error u']]

# Define dictionary with folders of convergence results and their desired names in the legend
dir_name_and_legend = {
    "p_0_par_0_0000_Euler_1stOrder_EnergyCons2_V_0_i_2": "first-order,\n $\epsilon=0$",
    "p_0_par_0_0100_Euler_1stOrder_EnergyCons2_V_0_i_2": "first-order,\n $\epsilon=0.01$",
    "p_0_par_1_0000_Euler_1stOrder_EnergyCons2_V_0_i_2": "first-order,\n $\epsilon=1$",
    "p_0_par_0_0000_Euler_2ndOrder_EnergyCons_V_0_i_2": "second-order,\n $\epsilon=0$",
    "p_0_par_0_0100_Euler_2ndOrder_EnergyCons_V_0_i_2": "second-order,\n $\epsilon=0.01$",
    "p_0_par_1_0000_Euler_2ndOrder_EnergyCons_V_0_i_2": "second-order,\n $\epsilon=1$",
}

# Loop over al directories (each containing results for a series of mesh/timestep refinements)
for item in dir_name_and_legend.items():
    print(item)
    name = item[0]
    ca.plot_problem_convergence(name, plots, dir_name_and_legend)

plt.close('all')

## Figure 2.2
# The options can be found in the .csv files in ../data/output
plots = [['mesh width $h$','L2 Error u']]

# Define dictionary with folders of convergence results and their desired names in the legend
dir_name_and_legend = {
    "p_2_par_0_0000_Euler_1stOrder_EnergyCons2_V_0_i_2": "first-order,\n $\epsilon=0$",
    #"p_2_par_0_0001_Euler_1stOrder_EnergyCons2_V_0_i_2": "first-order,\n $\epsilon=0.0001$",
    #"p_2_par_0_0010_Euler_1stOrder_EnergyCons2_V_0_i_2": "first-order,\n $\epsilon=0.001$",
    "p_2_par_0_0100_Euler_1stOrder_EnergyCons2_V_0_i_2": "first-order,\n $\epsilon=0.01\pi^{-2}$",
    "p_2_par_0_1000_Euler_1stOrder_EnergyCons2_V_0_i_2": "first-order,\n $\epsilon=0.1\pi^{-2}$",
    #"p_2_par_1_0000_Euler_1stOrder_EnergyCons2_V_0_i_2": "first-order,\n $\epsilon=1$",
    "p_2_par_0_0000_Euler_2ndOrder_EnergyCons_V_0_i_2": "second-order,\n $\epsilon=0$",
    #"p_2_par_0_0001_Euler_2ndOrder_EnergyCons_V_0_i_2": "second-order,\n $\epsilon=0.0001$",
    #"p_2_par_0_0010_Euler_2ndOrder_EnergyCons_V_0_i_2": "second-order,\n $\epsilon=0.001$",
    "p_2_par_0_0100_Euler_2ndOrder_EnergyCons_V_0_i_2": "second-order,\n $\epsilon=0.01\pi^{-2}$",
    "p_2_par_0_1000_Euler_2ndOrder_EnergyCons_V_0_i_2": "second-order,\n $\epsilon=0.1\pi^{-2}$",
    #"p_2_par_1_0000_Euler_2ndOrder_EnergyCons_V_0_i_2": "second-order,\n $\epsilon=1$",
    }

# Loop over al directories (each containing results for a series of mesh/timestep refinements)
for item in dir_name_and_legend.items():
    print(item)
    name = item[0]
    ca.plot_problem_convergence(name, plots, dir_name_and_legend)

plt.close('all')

## Figure 2.3
plots = [['time $t$','L2 Norm u']]

dir_name_and_legend = {
    "p_2_par_0_0000_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0000_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": "first-order,\n $\epsilon=0$",
    "p_2_par_0_0100_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0100_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": "first-order,\n $\epsilon=0.01\pi^{-2}$",
    "p_2_par_0_1000_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_1000_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": "first-order,\n $\epsilon=0.1\pi^{-2}$",
    #"p_2_par_0_0010_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0010_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": "first-order,\n $\epsilon=0.001$",
    #"p_2_par_0_0001_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0001_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": "first-order,\n $\epsilon=0.0001$",
    #"p_2_par_0_0000_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0000_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": "second-order,\n $\epsilon=0$",
    #"p_2_par_0_1000_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_1000_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": "second-order,\n $\epsilon=0.1$",
    #"p_2_par_0_0100_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0100_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": "second-order,\n $\epsilon=0.01$",
    #"p_2_par_0_0010_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0010_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": "second-order,\n $\epsilon=0.001$",
    #"p_2_par_0_0001_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0001_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": "second-order,\n $\epsilon=0.0001$",
}

dir_name_and_exact_solution = {
    "p_2_par_0_0000_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0000_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.*t),
    "p_2_par_0_0100_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0100_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.01*t),
    "p_2_par_0_1000_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_1000_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.1*t),
    #"p_2_par_0_0010_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0010_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.001*t),
    #"p_2_par_0_0001_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0001_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.0001*t),
    #"p_2_par_0_0000_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0000_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.*t),
    #"p_2_par_0_1000_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_1000_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.1*t),
    #"p_2_par_0_0100_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0100_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.01*t),
    #"p_2_par_0_0010_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0010_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.001*t),
    #"p_2_par_0_0001_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0001_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.0001*t),
}

dir_name_and_exact_solution_name = {
    "p_2_par_0_0000_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0000_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": "exact,\n $\epsilon=0$",
    "p_2_par_0_0100_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0100_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": "exact,\n $\epsilon=0.01\pi^{-2}$",
    "p_2_par_0_1000_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_1000_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": "exact,\n $\epsilon=0.1\pi^{-2}$",
    #"p_2_par_0_0010_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0010_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": "first-order,\n $\epsilon=0.001$",
    #"p_2_par_0_0001_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0001_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": "first-order,\n $\epsilon=0.0001$",
    #"p_2_par_0_0000_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0000_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": "second-order,\n $\epsilon=0$",
    #"p_2_par_0_1000_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_1000_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": "second-order,\n $\epsilon=0.1$",
    #"p_2_par_0_0100_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0100_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": "second-order,\n $\epsilon=0.01$",
    #"p_2_par_0_0010_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0010_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": "second-order,\n $\epsilon=0.001$",
    #"p_2_par_0_0001_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0001_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": "second-order,\n $\epsilon=0.0001$",
}

for item in dir_name_and_legend.items():
    print(item)
    name = item[0]
    ca.plot_invariants(name,plots,dir_name_and_legend,dir_name_and_exact_solution,dir_name_and_exact_solution_name)

os.rename('../data/figures/cons_p_2_Euler_EnergyCons2_V_0_i_2_L2_Norm_u.png', '../data/figures/cons_p_2_Euler_EnergyCons_1stOrder_V_0_i_2_L2_Norm_u.png')
os.rename('../data/figures/cons_p_2_Euler_EnergyCons2_V_0_i_2_L2_Norm_u.pgf', '../data/figures/cons_p_2_Euler_EnergyCons_1stOrder_V_0_i_2_L2_Norm_u.pgf')

plt.close('all')

## Figure 2.3
plots = [['time $t$','L2 Norm u']]

dir_name_and_legend = {
    #"p_2_par_0_0000_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0000_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": "first-order,\n $\epsilon=0$",
    #"p_2_par_0_1000_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_1000_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": "first-order,\n $\epsilon=0.1$",
    #"p_2_par_0_0100_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0100_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": "first-order,\n $\epsilon=0.01$",
    #"p_2_par_0_0010_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0010_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": "first-order,\n $\epsilon=0.001$",
    #"p_2_par_0_0001_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0001_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": "first-order,\n $\epsilon=0.0001$",
    "p_2_par_0_0000_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0000_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": "second-order,\n $\epsilon=0$",
    "p_2_par_0_0100_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0100_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": "second-order,\n $\epsilon=0.01\pi^{-2}$",
    "p_2_par_0_1000_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_1000_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": "second-order,\n $\epsilon=0.1\pi^{-2}$",
    #"p_2_par_0_0010_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0010_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": "second-order,\n $\epsilon=0.001$",
    #"p_2_par_0_0001_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0001_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": "second-order,\n $\epsilon=0.0001$",
}

dir_name_and_exact_solution = {
    #"p_2_par_0_0000_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0000_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.*t),
    #"p_2_par_0_1000_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_1000_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.1*t),
    #"p_2_par_0_0100_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0100_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.01*t),
    #"p_2_par_0_0010_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0010_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.001*t),
    #"p_2_par_0_0001_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0001_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.0001*t),
    "p_2_par_0_0000_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0000_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.*t),
    "p_2_par_0_0100_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0100_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.01*t),
    "p_2_par_0_1000_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_1000_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.1*t),
    #"p_2_par_0_0010_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0010_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.001*t),
    #"p_2_par_0_0001_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0001_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.0001*t),
}

dir_name_and_exact_solution_name = {
    #"p_2_par_0_0000_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0000_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.*t),
    #"p_2_par_0_1000_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_1000_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.1*t),
    #"p_2_par_0_0100_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0100_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.01*t),
    #"p_2_par_0_0010_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0010_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.001*t),
    #"p_2_par_0_0001_Euler_1stOrder_EnergyCons2_V_0_i_2/p_2_par_0_0001_Euler_1stOrder_EnergyCons2_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.0001*t),
    "p_2_par_0_0000_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0000_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": "exact,\n $\epsilon=0$",
    "p_2_par_0_0100_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0100_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": "exact,\n $\epsilon=0.01\pi^{-2}$",
    "p_2_par_0_1000_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_1000_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": "exact,\n $\epsilon=0.1\pi^{-2}$",
    #"p_2_par_0_0010_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0010_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.001*t),
    #"p_2_par_0_0001_Euler_2ndOrder_EnergyCons_V_0_i_2/p_2_par_0_0001_Euler_2ndOrder_EnergyCons_V_0_i_2_r_3_dt_0_0062500000.csv": lambda t: 1./math.sqrt(2.)*math.exp(-2.*0.0001*t),
}

for item in dir_name_and_legend.items():
    print(item)
    name = item[0]
    ca.plot_invariants(name,plots,dir_name_and_legend,dir_name_and_exact_solution,dir_name_and_exact_solution_name)

os.rename('../data/figures/cons_p_2_Euler_EnergyCons_V_0_i_2_L2_Norm_u.png', '../data/figures/cons_p_2_Euler_EnergyCons_2ndOrder_V_0_i_2_L2_Norm_u.png')
os.rename('../data/figures/cons_p_2_Euler_EnergyCons_V_0_i_2_L2_Norm_u.pgf', '../data/figures/cons_p_2_Euler_EnergyCons_2ndOrder_V_0_i_2_L2_Norm_u.pgf')

plt.close('all')
