import convergence_analysis as ca

ca.analyse_convergence("Advection_2ndOrder_NonCons",    \
                       1,                           \
                       3,                           \
                       20,                           \
                       0,                           \
                       0,                           \
                       [3,4,5,6],               \
                       [.2,.1,.05,.025,.0125,.0125/2,.0125/4,.0125/8]   \
                        )

ca.analyse_convergence("Advection_1stOrder_NonCons",    \
                       1,                           \
                       3,                           \
                       20,                           \
                       0,                           \
                       0,                           \
                       [3,4,5,6],               \
                       [.2,.1,.05,.025,.0125,.0125/2,.0125/4,.0125/8]   \
                        )

ca.analyse_convergence("Advection_2ndOrder_NonCons",    \
                       1,                           \
                       3,                           \
                       7,                           \
                       0,                           \
                       0,                           \
                       [0,1,2,3],               \
                       [.5,.25,.125,.0625]   \
                        )

ca.analyse_convergence("Advection_1stOrder_NonCons",    \
                       1,                           \
                       3,                           \
                       7,                           \
                       0,                           \
                       0,                           \
                       [0,1,2,3,4],               \
                       [1.,.5,.25,.125,.0625]   \
                        )

ca.analyse_convergence("Advection_2ndOrder_NonCons",    \
                       1,                           \
                       3,                           \
                       8,                           \
                       0,                           \
                       0,                           \
                       [0,1,2,3],               \
                       [.5,.25,.125,.0625]   \
                        )

ca.analyse_convergence("Advection_1stOrder_NonCons",    \
                       1,                           \
                       3,                           \
                       8,                           \
                       0,                           \
                       0,                           \
                       [0,1,2,3],               \
                       [.5,.25,.125,.0625]   \
                        )
