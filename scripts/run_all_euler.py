import convergence_analysis as ca
from multiprocessing import Process


def c1():
    """
    # Experiment 1
    # Fig 2.1
    ca.analyse_convergence("Euler_1stOrder_NonCons",    \
                           0,                           \
                           2,                           \
                           2,                           \
                           1,                           \
                           0,                           \
                           [0,1,2,3,4],               \
                           [.2,.1,.05,.025,.0125,.0125/2,.0125/4,.0125/8]   \
                            )

    ca.analyse_convergence("Euler_2ndOrder_NonCons",    \
                           0,                           \
                           2,                           \
                           2,                           \
                           1,                           \
                           0,                           \
                           [0,1,2,3],               \
                           [.2,.1,.05,.025,.0125,.0125/2,.0125/4,.0125/8]   \
                            )
    """
    # Experiment 2
    # Fig 2.2 + Fig 2.3
    for par in [0.,.01,.1]:

        ca.analyse_convergence("Euler_2ndOrder_EnergyCons", \
                               0, \
                               2, \
                               2, \
                               par, \
                               0, \
                               [0,1,2,3,4], \
                               [.05,.025,.0125,.0125/2,.0125/4,.0125/8] \
                               )

        ca.analyse_convergence("Euler_1stOrder_EnergyCons2",    \
                               0,                           \
                               2,                           \
                               2,                           \
                               par,                           \
                               0, \
                               [0,1,2,3,4], \
                               [.05,.025,.0125,.0125/2,.0125/4,.0125/8] \
                               )



    for par in [0.,.01,1.]:
        ca.analyse_convergence("Euler_2ndOrder_EnergyCons", \
                               0, \
                               2, \
                               0, \
                               par, \
                               0, \
                               [0,1,2,3,4], \
                               [.05,.025,.0125,.0125/2,.0125/4,.0125/8] \
                               )

        ca.analyse_convergence("Euler_1stOrder_EnergyCons2",    \
                               0,                           \
                               2,                           \
                               0,                           \
                               par,                           \
                               0, \
                               [0,1,2,3,4], \
                               [.05,.025,.0125,.0125/2,.0125/4,.0125/8] \
                               )







#    # Experiment 2
#    # Fig 2.2 + Fig 2.3
#    for par in [0,.01,1]:
#        ca.analyse_convergence("Euler_1stOrder_NonCons",    \
#                               0,                           \
#                               2,                           \
#                               0,                           \
#                               par,                           \
#                               0,                           \
#                               [0,1,2,3,4],               \
#                               [.2,.1,.05,.025,.0125,.0125/2,.0125/4,.0125/8]   \
#                                )
#
#        ca.analyse_convergence("Euler_2ndOrder_NonCons",    \
#                               0,                           \
#                               2,                           \
#                               0,                           \
#                               par,                           \
#                               0,                           \
#                               [0,1,2,3],               \
#                               [.2,.1,.05,.025,.0125,.0125/2,.0125/4,.0125/8]   \
#                                )

def c2():
    # Experiment 3
    # Fig 2.4, 2.5, 2.5, 2.7, 2.10
    ca.analyse_convergence("Euler_2ndOrder_Cons",    \
                           0,                           \
                           2,                           \
                           3,                           \
                           1,                           \
                           1,                           \
                           [0,1,2,3,4,5],               \
                           [.05,.025,.0125,0.00625,.003125,0.0015625]   \
                            )

def c3():
    # Fig 2.11
    ca.analyse_convergence("Euler_1stOrder_Cons",    \
                           0,                           \
                           2,                           \
                           3,                           \
                           1,                           \
                           0,                           \
                           [4],               \
                           [.0125/4]   \
                            )

    # Just meant for comparison for conserved variables
    ca.analyse_convergence("Euler_2ndOrder_NonCons",    \
                           0,                           \
                           2,                           \
                           3,                           \
                           1,                           \
                           0,                           \
                           [4],               \
                           [.0125/4]   \
                            )

    ca.analyse_convergence("Euler_1stOrder_NonCons",    \
                           0,                           \
                           2,                           \
                           3,                           \
                           1,                           \
                           0,                           \
                           [4],               \
                           [.0125/4]   \
                            )

def c4():
    # Experiment 4
    # Fig. 2.12, 2.13, 2.14
    # Taylor-Green in 3D
    ca.analyse_convergence("Euler_1stOrder_Cons",    \
                           0,                           \
                           2,                           \
                           6,                           \
                           0,                           \
                           0,                           \
                           [0,1,2,3],               \
                           [8*.0625,4*.0625,2*.0625,.0625]   \
                            )

    ca.analyse_convergence("Euler_2ndOrder_Cons",    \
                           0,                           \
                           2,                           \
                           6,                           \
                           0,                           \
                           0,                           \
                           [0,1,2,3],               \
                           [8*.0625,4*.0625,2*.0625,.0625]   \
                            )

    # Fig. 2.13, 2.14
    ca.analyse_convergence("Euler_1stOrder_NonCons",    \
                           0,                           \
                           2,                           \
                           6,                           \
                           0,                           \
                           0,                           \
                           [2],               \
                           [2*.0625]   \
                            )

    ca.analyse_convergence("Euler_2ndOrder_NonCons",    \
                           0,                           \
                           2,                           \
                           6,                           \
                           0,                           \
                           0,                           \
                           [2],               \
                           [2*.0625]   \
                            )


# Experiment 5
# Fig 2.15
def c5():
    ca.analyse_convergence("Euler_1stOrder_NonCons",    \
                           0,                           \
                           2,                           \
                           9,                           \
                           0,                           \
                           0,                           \
                           [1,2,3],               \
                           [.25,.125,.0625]   \
                            )

def c6():
    ca.analyse_convergence("Euler_2ndOrder_NonCons",    \
                           0,                           \
                           2,                           \
                           9,                           \
                           0,                           \
                           0,                           \
                           [1,2,3],               \
                           [.25,.125,.0625]   \
                            )


def c7():
    # Experiment 6
    # Fig 2.16
    ca.analyse_convergence("Euler_2ndOrder_NonCons",    \
                           0,                           \
                           2,                           \
                           21,                           \
                           0,                           \
                           1,                           \
                           [3],               \
                           [.01]   \
                            )

def c8 ():
    # 2D incompressible Euler with obstacles
    ca.analyse_convergence("Euler_1stOrder_NonCons", \
                           0, \
                           2, \
                           28, \
                           0.1, \
                           1, \
                           [0], \
                           [0.01] \
                            )

def c9 ():
    # 2D incompressible Euler with obstacles
    ca.analyse_convergence("Euler_1stOrder_Cons", \
                           0, \
                           2, \
                           28, \
                           0.1, \
                           1, \
                           [0], \
                           [0.01] \
                            )
def c10 ():
    # 2D incompressible Euler with obstacles
    ca.analyse_convergence("Euler_2ndOrder_NonCons", \
                           0, \
                           2, \
                           28, \
                           0.1, \
                           1, \
                           [0], \
                           [0.01] \
                            )

def c11 ():
    # 2D incompressible Euler with obstacles
    ca.analyse_convergence("Euler_2ndOrder_Cons", \
                           0, \
                           2, \
                           28, \
                           0.1, \
                           1, \
                           [0], \
                           [0.01] \
                            )






p1 = Process(target=c1)
p2 = Process(target=c2)
p3 = Process(target=c3)
p4 = Process(target=c4)
p5 = Process(target=c5)
p6 = Process(target=c6)
p7 = Process(target=c7)
p8 = Process(target=c8)
p9 = Process(target=c9)
p10 = Process(target=c10)
p11 = Process(target=c11)


p1.start()
p2.start()
p3.start()
p4.start()
p5.start()
p6.start()
p7.start()
p8.start()
p9.start()
p10.start()
p11.start()
