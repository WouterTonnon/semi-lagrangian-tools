import os, re, pandas
import numpy as np
import matplotlib.pyplot as plt

# Define what you would like to plot w.r.t to what for different simulations of the same batch.
# The options can be found in the .csv files in ../data/output
plots = [['mesh-width [h]','time [s]']]

# Define invariants that you would like to check.
conservation_plots = []#[['time [s]','L2 Norm u']]

root_dir='../data/output'
file_set = set()
file_set_backup = set()
dir_set = set()

# First, we scan all directories and files
for dir_, _, files in os.walk(root_dir):
    for file_name in files:
        rel_dir = os.path.relpath(dir_, root_dir)
        dir_set.add(rel_dir)
        rel_file = os.path.join(rel_dir, file_name)
        file_set.add(rel_file)
        file_set_backup.add(rel_file)

tot_problems = -1
# Loop over al directories (each containing results for a series of mesh/timestep refinements)
while(len(dir_set)!=0):
    name = dir_set.pop()
    if(re.search(r'\d+', name)):
        problem_num = int(re.search(r'\d+', name).group())
        path='./../data/output/' + name + "/"
        filenames = os.listdir(path)

        # Keep track of how many problems we consider
        if(problem_num>tot_problems):
            tot_problems = problem_num

        x = [0]*len(filenames)
        y = [0]*len(filenames)

        # Loop over all desired plots
        for j in range(len(plots)):

            # read the appropriate data
            for i in range(0, len(filenames)):
                filename = filenames[i]
                with open(path+filename) as csv_file:
                    data = pandas.read_csv(csv_file)
                    if(len(data[plots[j][0]])>0):
                        x[i]=data[plots[j][0]].iloc[-1]
                        y[i]=data[plots[j][1]].iloc[-1]

            # Sort (e.g. on decreasing mesh size)
            x_sorted = np.array(x)
            y_sorted = np.array(y)
            p=np.argsort(x_sorted)
            x_sorted = x_sorted[p]
            y_sorted = y_sorted[p]

            # Plot in a loglog plot
            plt.figure(problem_num*len(plots)+j+1)
            plt.loglog(x_sorted,y_sorted,label=name)
            ax = plt.gca()

            # Fix the aspect ratio
            # ax.set_aspect('equal', 'box')

            # Set gridlines
            plt.grid(b=True, which='major')
            plt.grid(b=True, which='minor')

            # Label the axes and give a title
            plt.xlabel(plots[j][0])
            plt.ylabel(plots[j][1])
            plt.title('problem '+str(problem_num))

            # Enable the legend
            plt.legend()

# First we find, from the file-names, the maximum amount of refinements that was performed
r_max = -1
while(len(file_set_backup)!=0):
    name = file_set_backup.pop()
    name = name.split('/')[1]

    if(re.search(r'\d+', name)):
        ints = [int(s) for s in name.split('_') if s.isdigit()]
        r = ints[3]
        if(r>r_max):
            r_max = r

# Now loop again over all files
while(len(file_set)!=0):
    name = file_set.pop()
    path='./../data/output/' + name

    # Split the file-name from the directory path
    name = name.split('/')[1]


    if(re.search(r'\d+', name)):
        # Obtain the values of all parameters from the file-name
        ints = [int(s) for s in name.split('_') if s.isdigit()]
        p = ints[0]
        V = ints[3]
        i = ints[4]
        r = ints[5]

        # Obtain the name of the used scheme from the file-name
        strs = name.split('_')
        par = strs[3]+"_"+strs[4]
        print(par)
        scheme_name = strs[5]+"_"+strs[6]+"_"+strs[7]

        # Loop over all plots requested by the user
        for j in range(0,len(conservation_plots)):

            # Open the csv file
            with open(path) as csv_file:
                data = pandas.read_csv(csv_file)
                # Check if there is any data in the file
                if(len(data[conservation_plots[j][0]])>1):
                    # Plot the results
                    plt.figure(p)
                    plt.plot(data[conservation_plots[j][0]],data[conservation_plots[j][1]],label=scheme_name+"_V_"+str(V)+"_i_"+str(i)+"_par_"+par)

                    # Write a grid
                    plt.grid(b=True, which='major')
                    plt.grid(b=True, which='minor')

                    # Label the axes and give a title to the plot
                    plt.xlabel(conservation_plots[j][0])
                    plt.ylabel(conservation_plots[j][1])
                    plt.title('problem ' + str(p) + ", refinement "+str(r))

                    # Enable the legend
                    plt.legend()

plt.show()
