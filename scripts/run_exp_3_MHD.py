import convergence_analysis as ca
import subprocess
import os, re, pandas
import numpy as np
import matplotlib
matplotlib.use("pgf")
matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})
import matplotlib.pyplot as plt

ca.analyse_convergence("MHD_scheme_A_E", \
                       1, \
                       2, \
                       22, \
                       0, \
                       1, \
                       [0,1,2,3,4], \
                       [.05,.025,.0125,0.00625,.003125,0.0015625,0.0015625/2] \
                       )

ca.analyse_convergence("MHD_scheme_A_E", \
                       0, \
                       2, \
                       22, \
                       0, \
                       1, \
                       [0,1,2,3,4], \
                       [.05,.025,.0125,0.00625,.003125,0.0015625,0.0015625/2] \
                       )

ca.analyse_convergence("MHD_scheme_B", \
                       0, \
                       2, \
                       22, \
                       0, \
                       1, \
                       [0,1,2,3,4], \
                       [.05,.025,.0125,0.00625,.003125,0.0015625,0.0015625/2] \
                       )

"""
# Define what you would like to plot w.r.t to what for different simulations of the same batch.
# The options can be found in the .csv files in ../data/output
plots = [['mesh-width [h]','L2 Error u']]

# Define dictionary with folders of convergence results and their desired names in the legend
dir_name_and_legend = {
    "p_22_par_0_0000_MHD_2ndOrder_NonCons_J3_V_0_i_2": "scheme A",
    "p_22_par_0_0000_MHD_2ndOrder_NonCons_J3_V_1_i_2": "scheme E",
    "p_22_par_0_0000_MHD_2ndOrder_NonCons_J3_exactJ_V_0_i_2": "scheme B"
}

# Loop over al directories (each containing results for a series of mesh/timestep refinements)
for item in dir_name_and_legend.items():
    print(item)
    name = item[0]
    ca.plot_problem_convergence(name, plots, dir_name_and_legend)

plt.close('all')




"""
