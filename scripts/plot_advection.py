import os, re, pandas
import numpy as np
import matplotlib
matplotlib.use("pgf")
matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})
import matplotlib.pyplot as plt
import convergence_analysis as ca


# Define what you would like to plot w.r.t to what for different simulations of the same batch.
# The options can be found in the .csv files in ../data/output
plots = [['mesh-width [h]','L2 Error u']]

# Define dictionary with folders of convergence results and their desired names in the legend
dir_name_and_legend = {
    "p_20_par_0_0000_Advection_1stOrder_NonCons_V_1_i_3": "first-order",
    "p_20_par_0_0000_Advection_2ndOrder_NonCons_V_1_i_3": "second-order",
    "p_7_par_0_0000_Advection_1stOrder_NonCons_V_1_i_3": "first-order",
    "p_7_par_0_0000_Advection_2ndOrder_NonCons_V_1_i_3": "second-order",
    "p_8_par_0_0000_Advection_1stOrder_NonCons_V_1_i_3": "first-order",
    "p_8_par_0_0000_Advection_2ndOrder_NonCons_V_1_i_3": "second-order"
}

# Loop over al directories (each containing results for a series of mesh/timestep refinements)
for item in dir_name_and_legend.items():
    name = item[0]
    ca.plot_problem_convergence(name, plots, dir_name_and_legend)
