import os, re, pandas
import numpy as np
import matplotlib
matplotlib.use("pgf")
matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})
import matplotlib.pyplot as plt
import convergence_analysis as ca
from multiprocessing import Process

def J3():
    for eps in [0.,.0001,.001,.01,.1,1.]:#.1,.01,.001]:
        ca.analyse_convergence("MHD_scheme_A_E", \
                               0, \
                               2, \
                               12, \
                               eps, \
                               0, \
                               [0,1,2], \
                               [.1,.05,.025] \
                               )

    for eps in [0.,.0001,.001,.01,.1,1.]:#.1,.01,.001]:
        ca.analyse_convergence("MHD_scheme_A_E", \
                               0, \
                               2, \
                               24, \
                               eps, \
                               0, \
                               [0,1,2], \
                               [.1,.05,.025] \
                               )

def J3_prescribed():
    for eps in [0.,.0001,.001,.01,.1,1.]:#.1,.01,.001]:
        ca.analyse_convergence("MHD_scheme_A_E", \
                               1, \
                               3, \
                               12, \
                               eps, \
                               0, \
                               [0,1,2], \
                               [.1,.05,.025] \
                               )

    for eps in [0.,.0001,.001,.01,.1,1.]:#.1,.01,.001]:
        ca.analyse_convergence("MHD_scheme_A_E", \
                               1, \
                               3, \
                               24, \
                               eps, \
                               0, \
                               [0,1,2], \
                               [.1,.05,.025] \
                               )

def J3_exactA():
    for eps in [0.,.0001,.001,.01,.1,1.]:#.1,.01,.001]:
        ca.analyse_convergence("MHD_scheme_C", \
                               0, \
                               2, \
                               12, \
                               eps, \
                               0, \
                               [0,1,2], \
                               [.1,.05,.025] \
                               )

    for eps in [0.,.0001,.001,.01,.1,1.]:#.1,.01,.001]:
        ca.analyse_convergence("MHD_scheme_C", \
                               0, \
                               2, \
                               24, \
                               eps, \
                               0, \
                               [0,1,2], \
                               [.1,.05,.025] \
                               )

def J3_exactJ():
    for eps in [0.,.0001,.001,.01,.1,1.]:#.1,.01,.001]:
        ca.analyse_convergence("MHD_scheme_B", \
                               0, \
                               2, \
                               12, \
                               eps, \
                               0, \
                               [0,1,2], \
                               [.1,.05,.025] \
                               )

    for eps in [0.,.0001,.001,.01,.1,1.]:#.1,.01,.001]:
        ca.analyse_convergence("MHD_scheme_B", \
                               0, \
                               2, \
                               24, \
                               eps, \
                               0, \
                               [0,1,2], \
                               [.1,.05,.025] \
                               )

p1 = Process(target=J3)
p2 = Process(target=J3_prescribed)
p3 = Process(target=J3_exactA)
p4 = Process(target=J3_exactJ)
p1.start()
p2.start()
p3.start()
p4.start()
