import os, re, pandas
import numpy as np
import matplotlib
matplotlib.use("pgf")
matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})
import matplotlib.pyplot as plt
import convergence_analysis as ca
from multiprocessing import Process

"""
for name_tag in ['MHD_scheme_A_E_V_1_i_3','MHD_scheme_A_E_V_0_i_2','MHD_scheme_B_V_0_i_2','MHD_scheme_C_V_0_i_2']:
    for ordinate in ['L2 Error u', 'L2 Error A', 'Linf Error u', 'Linf Error A']:
        # Define what you would like to plot w.r.t to what for different simulations of the same batch.
        # The options can be found in the .csv files in ../data/output
        plots = [['mesh-width [h]',ordinate]]

        # Define dictionary with folders of convergence results and their desired names in the legend
        dir_name_and_legend = {
            "p_11_par_0_0000_"+name_tag: "$\epsilon=0$",
            "p_11_par_0_0001_"+name_tag: "$\epsilon=10^{-4}$",
            "p_11_par_0_0010_"+name_tag: "$\epsilon=10^{-3}$",
            "p_11_par_0_0100_"+name_tag: "$\epsilon=10^{-2}$",
            "p_11_par_0_1000_"+name_tag: "$\epsilon=10^{-1}$",
            "p_11_par_1_0000_"+name_tag: "$\epsilon=10^{0}$"
        }

        # Loop over al directories (each containing results for a series of mesh/timestep refinements)
        for item in dir_name_and_legend.items():
            name = item[0]
            ca.plot_problem_convergence(name, plots, dir_name_and_legend)

        plt.close('all')
        # Define what you would like to plot w.r.t to what for different simulations of the same batch.
        # The options can be found in the .csv files in ../data/output
        plots = [['mesh-width [h]',ordinate]]

        # Define dictionary with folders of convergence results and their desired names in the legend
        dir_name_and_legend = {
            "p_25_par_0_0000_"+name_tag: "$\\nu=0$",
            "p_25_par_0_0001_"+name_tag: "$\\nu=10^{-4}$",
            "p_25_par_0_0010_"+name_tag: "$\\nu=10^{-3}$",
            "p_25_par_0_0100_"+name_tag: "$\\nu=10^{-2}$",
            "p_25_par_0_1000_"+name_tag: "$\\nu=10^{-1}$",
            "p_25_par_1_0000_"+name_tag: "$\\nu=10^{0}$"
        }

        # Loop over al directories (each containing results for a series of mesh/timestep refinements)
        for item in dir_name_and_legend.items():
            name = item[0]
            ca.plot_problem_convergence(name, plots, dir_name_and_legend)



        plt.close('all')

        # Define what you would like to plot w.r.t to what for different simulations of the same batch.
        # The options can be found in the .csv files in ../data/output
        plots = [['mesh-width [h]',ordinate]]

        # Define dictionary with folders of convergence results and their desired names in the legend
        dir_name_and_legend = {
            "p_12_par_0_0000_"+name_tag: "$\epsilon=0$",
            "p_12_par_0_0001_"+name_tag: "$\epsilon=10^{-4}$",
            "p_12_par_0_0010_"+name_tag: "$\epsilon=10^{-3}$",
            "p_12_par_0_0100_"+name_tag: "$\epsilon=10^{-2}$",
            "p_12_par_0_1000_"+name_tag: "$\epsilon=10^{-1}$",
            "p_12_par_1_0000_"+name_tag: "$\epsilon=10^{0}$"
        }

        # Loop over al directories (each containing results for a series of mesh/timestep refinements)
        for item in dir_name_and_legend.items():
            name = item[0]
            ca.plot_problem_convergence(name, plots, dir_name_and_legend)

        plt.close('all')
        # Define what you would like to plot w.r.t to what for different simulations of the same batch.
        # The options can be found in the .csv files in ../data/output
        plots = [['mesh-width [h]',ordinate]]

        # Define dictionary with folders of convergence results and their desired names in the legend
        dir_name_and_legend = {
            "p_24_par_0_0000_"+name_tag: "$\\nu=0$",
            "p_24_par_0_0001_"+name_tag: "$\\nu=10^{-4}$",
            "p_24_par_0_0010_"+name_tag: "$\\nu=10^{-3}$",
            "p_24_par_0_0100_"+name_tag: "$\\nu=10^{-2}$",
            "p_24_par_0_1000_"+name_tag: "$\\nu=10^{-1}$",
            "p_24_par_1_0000_"+name_tag: "$\\nu=10^{0}$"
        }

        # Loop over al directories (each containing results for a series of mesh/timestep refinements)
        for item in dir_name_and_legend.items():
            name = item[0]
            ca.plot_problem_convergence(name, plots, dir_name_and_legend)

        # Close all plots that have been written to the disk
        plt.close('all')

"""
# Define what you would like to plot w.r.t to what for different simulations of the same batch.
# The options can be found in the .csv files in ../data/output
plots = [['mesh-width [h]','L2 Error u']]

# Define dictionary with folders of convergence results and their desired names in the legend
dir_name_and_legend = {
    "p_22_par_0_0000_MHD_scheme_A_E_V_0_i_2": "scheme A",
    "p_22_par_0_0000_MHD_scheme_A_E_V_1_i_2": "scheme E",
    "p_22_par_0_0000_MHD_scheme_B_V_0_i_2": "scheme B"
}

# Loop over al directories (each containing results for a series of mesh/timestep refinements)
for item in dir_name_and_legend.items():
    print(item)
    name = item[0]
    ca.plot_problem_convergence(name, plots, dir_name_and_legend)

plt.close('all')
