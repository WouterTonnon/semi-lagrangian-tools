import subprocess
import os, re, pandas
import numpy as np
import matplotlib
matplotlib.use("pgf")
matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})
import matplotlib.pyplot as plt


plots = [['time [s]','Energy'],['time [s]','Energy Bound']]

dir_to_legend = {
    "p_22_par_0_0000_MHD_scheme_A_E_V_0_i_2/p_22_par_0_0000_MHD_scheme_A_E_V_0_i_2_r_4_dt_0_0031250000.csv": "second-order"
}

for item in dir_to_legend.items():
    print(item)
    file_name = item[0]

    path='./../data/output/' + file_name

    # Split the file-name from the directory path
    name = file_name.split('/')[1]


    if(re.search(r'\d+', name)):
        # Obtain the values of all parameters from the file-name
        ints = [int(s) for s in name.split('_') if s.isdigit()]
        p = ints[0]
        V = ints[3]
        i = ints[4]
        r = ints[5]

        trimmed_name = re.search('_par_(.*)_V_', name).group(1)
        trimmed_name = re.sub(r'^.*?_', '', trimmed_name)
        trimmed_name = re.sub(r'^.*?_', '', trimmed_name)
        trimmed_name = trimmed_name.replace('_2ndOrder','')
        trimmed_name = trimmed_name.replace('_1stOrder','')
        trimmed_name = trimmed_name.replace('_Cons','')
        trimmed_name = trimmed_name.replace('_NonCons','')

        # Obtain the name of the used scheme from the file-name
        strs = name.split('_')
        par = strs[3]+"_"+strs[4]
        print(par)
        scheme_name = strs[5]+"_"+strs[6]+"_"+strs[7]

        # Loop over all plots requested by the user
        for j in range(0,len(plots)):
            # Open the csv file
            with open(path) as csv_file:
                data = pandas.read_csv(csv_file)
                # Check if there is any data in the file
                if(len(data[plots[j][0]])>1):
                    # Plot the results
                    plt.figure(1,[6.4*5/8, 4.8*5/8])
                    plt.plot(data[plots[j][0]],data[plots[j][1]],label=plots[j][1])
                    ax = plt.gca()
                    ax.set_yscale('log')

                    # Write a grid
                    plt.grid(b=True, which='major')
                    plt.grid(b=True, which='minor')

                    # Label the axes and give a title to the plot
                    plt.xlabel(plots[j][0])
                    #plt.ylabel(plots[j][1])

                    # Enable the legend
                    plt.legend()

            # Shrink current axis by 20%
            box = ax.get_position()
            ax.set_position([box.x0, box.y0, box.width, box.height])


            # Find variable name
            var_name = plots[j][1]
            var_name = var_name.replace(' ','_')
            print(var_name)

            #ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
            plt.savefig('../data/figures/cons_p_'+str(p)+'_'+trimmed_name+'_V_'+str(V)+'_i_'+str(i)+'_'+var_name+'.pgf', bbox_inches='tight')
            plt.savefig('../data/figures/cons_p_'+str(p)+'_'+trimmed_name+'_V_'+str(V)+'_i_'+str(i)+'_'+var_name+'.png', bbox_inches='tight')
