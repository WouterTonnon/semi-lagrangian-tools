"""
Copyright 2021 W.R. Tonnon

This file is part of Semi-Lagrangian Tools
A class used for performing automated convergence analysis and plotting results.

Copyright (C) <2021>  <W.R. Tonnon>

Semi-Lagrangian Tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Semi-Lagrangian Tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import subprocess
import os, re, pandas
import numpy as np
import matplotlib
matplotlib.use("pgf")
matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})
import matplotlib.pyplot as plt

def analyse_convergence(implementation_name,    \
                        prescribed_velocity,    \
                        interpolation_type,     \
                        problem_number,         \
                        problem_parameter,      \
                        visualisation,          \
                        num_mesh_refinements,   \
                        timesteps               \
                        ):
        """
        Parameters
        ----------
        implementation_name : str
            Name of the executable of the implementation
        prescribed_velocity : int
            Toggle for using prescribed velocity (1) or approximated velocity (0)
        interpolation_type : int
            Interpolation type to perform pointwise evaluation on discontinuous finite-element spaces
        problem_number: int
            Problem number
        problem_parameter: double
            Parameter associated with the problem (e.g. viscosity parameter). Problem dependent.
        visualisation: int
            Store visualisation data if visualisation > 0. visualisation also gives the number of timesteps between storing visualisation information.
        num_mesh_refinements: list of int
            We run the given problem for different mesh-refinements. Here we define which mesh-refinements.
        timesteps: list of double
            We run the given problem for different timesteps. Here we define which timesteps.
        """

        file_folder = "p_" + str(problem_number) + "_par_" + "{:.{}f}".format( problem_parameter, 4 ).replace(".", "_") +"_"+implementation_name+"_V_" + str(prescribed_velocity) + "_i_" + str(interpolation_type)
        print(" * * processing "+file_folder+" * * ")

        try:
            # Try to make a directory
            os.makedirs('./../data/output/'+file_folder)
        except FileExistsError:
            # If the directory already exists, empty it
            filenames = os.listdir('./../data/output/'+file_folder)
            for file in filenames:
                os.remove('./../data/output/'+file_folder+"/"+file)

        # Loop over all refinements and save the data
        for i in range(0,len(num_mesh_refinements)):
            print('iteration '+str(i)+'..')

            # Create a config file
            file_name = file_folder + "_r_" + str(num_mesh_refinements[i]) + "_dt_" + "{:.{}f}".format( timesteps[i], 10 ).replace(".", "_")
            config_file = open("./../data/config/"+file_name+".ini","w")
            config_file.write("timestep="+str(timesteps[i])+"\n")
            config_file.write("parameter="+str(problem_parameter)+"\n")
            config_file.write("refinements="+str(num_mesh_refinements[i])+"\n")
            config_file.write("mesh-file="+""+"\n")
            config_file.write("problem-number="+str(problem_number)+"\n")
            config_file.write("prescribed-velocity="+str(prescribed_velocity)+"\n")
            config_file.write("interpolation="+str(interpolation_type)+"\n")
            config_file.write("output-file-name="+file_name+"\n")
            config_file.write("visualisation="+str(visualisation)+"\n")
            config_file.close()

            # Run the implementation with config file
            subprocess.call(["./../cmake-build-release/apps/"+implementation_name, "-c", "./../data/config/"+file_name+".ini"])

def plot_invariants(file_name,  \
                    plots,      \
                    dir_to_legend,  \
                    dir_to_exact_solution={}, \
                    dir_name_and_exact_solution_name={}):
    """
    Parameters
    ----------
    file_name : str
        file name relative to semi-lagrangian-tools/data/output
    plots : list of list of str
        plots[i] is a list, where plots[i][0] denotes the x-variable to be plotted and plots[i][1] denotes the y-variable to be plotted.
    dir_to_legend : dict of (str, str)
        dictionary linking file-names to names in the legend of the plot
    """
    path='./../data/output/' + file_name

    # Split the file-name from the directory path
    name = file_name.split('/')[1]

    if(re.search(r'\d+', name)):
        # Obtain the values of all parameters from the file-name
        ints = [int(s) for s in name.split('_') if s.isdigit()]
        p = ints[0]
        V = ints[3]
        i = ints[4]
        r = ints[5]

        trimmed_name = re.search('_par_(.*)_V_', name).group(1)
        trimmed_name = re.sub(r'^.*?_', '', trimmed_name)
        trimmed_name = re.sub(r'^.*?_', '', trimmed_name)
        trimmed_name = trimmed_name.replace('_2ndOrder','')
        trimmed_name = trimmed_name.replace('_1stOrder','')
        trimmed_name = trimmed_name.replace('_Cons','')
        trimmed_name = trimmed_name.replace('_NonCons','')

        # Obtain the name of the used scheme from the file-name
        strs = name.split('_')
        par = strs[3]+"_"+strs[4]
        print(par)
        scheme_name = strs[5]+"_"+strs[6]+"_"+strs[7]

        # Loop over all plots requested by the user
        for j in range(0,len(plots)):
            # Open the csv file
            with open(path) as csv_file:
                data = pandas.read_csv(csv_file)
                # Check if there is any data in the file
                if(len(data[plots[j][0]])>1):

                    # Open figure
                    plt.figure(p*len(plots)+j,[6.4*5/8, 4.8*5/8])
                    ax = plt.gca()

                    # Determine color used for lines
                    color = next(ax._get_lines.prop_cycler)['color']

                    # Plot the results
                    if(dir_to_exact_solution!={}):
                        plt.plot(data[plots[j][0]],[ dir_to_exact_solution[file_name](data[plots[j][0]][i]) for i in range(len(data[plots[j][0]])) ],label=dir_name_and_exact_solution_name[file_name],color=color)
                        plt.plot(data[plots[j][0]][::10],data[plots[j][1]][::10],label=dir_to_legend[file_name],marker='*',linestyle='None',color=color)
                    else:
                        plt.plot(data[plots[j][0]][:],data[plots[j][1]][:],label=dir_to_legend[file_name],color=color)

                    # Write a grid
                    plt.grid(b=True, which='major')
                    plt.grid(b=True, which='minor')

                    # Label the axes and give a title to the plot
                    plt.xlabel(plots[j][0])
                    plt.ylabel(plots[j][1])

                    # Enable the legend
                    plt.legend()

            # Shrink current axis by 20%
            box = ax.get_position()
            ax.set_position([box.x0, box.y0, box.width, box.height])


            # Find variable name
            var_name = plots[j][1]
            var_name = var_name.replace(' ','_')
            print(var_name)

            ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
            plt.savefig('../data/figures/cons_p_'+str(p)+'_'+trimmed_name+'_V_'+str(V)+'_i_'+str(i)+'_'+var_name+'.pgf', bbox_inches='tight',dpi=400)
            plt.savefig('../data/figures/cons_p_'+str(p)+'_'+trimmed_name+'_V_'+str(V)+'_i_'+str(i)+'_'+var_name+'.png', bbox_inches='tight',dpi=400)



def plot_problem_convergence(name,
                             plots,
                             dir_to_legend):
    """
    Parameters
    ----------
    name : str
        folder name relative to semi-lagrangian-tools/data/output of the folder containing the results for different mesh-sizes or time-steps.
    plots : list of list of str
        plots[i] is a list, where plots[i][0] denotes the x-variable to be plotted and plots[i][1] denotes the y-variable to be plotted.
    dir_to_legend : dict of (str, str)
        dictionary linking file-names to names in the legend of the plot
    """
    if(re.search(r'\d+', name)):
        # Obtain the values of all parameters from the file-name
        ints = [int(s) for s in name.split('_') if s.isdigit()]
        problem_num = ints[0]
        V = ints[3]
        I = ints[4]
        path='./../data/output/' + name + "/"
        filenames = os.listdir(path)

        trimmed_name = re.search('_par_(.*)_V_', name).group(1)
        trimmed_name = re.sub(r'^.*?_', '', trimmed_name)
        trimmed_name = re.sub(r'^.*?_', '', trimmed_name)
        trimmed_name = trimmed_name.replace('_2ndOrder','')
        trimmed_name = trimmed_name.replace('_1stOrder','')
        trimmed_name = trimmed_name.replace('_Cons','')
        trimmed_name = trimmed_name.replace('_NonCons','')

        x = [0]*len(filenames)
        y = [0]*len(filenames)

        # Loop over all desired plots
        for j in range(len(plots)):

            # read the appropriate data
            for i in range(0, len(filenames)):
                filename = filenames[i]
                with open(path+filename) as csv_file:
                    data = pandas.read_csv(csv_file)
                    if(len(data[plots[j][0]])>0):
                        x[i]=data[plots[j][0]].iloc[-1]
                        y[i]=data[plots[j][1]].iloc[-1]

            # Sort (e.g. on decreasing mesh size)
            x_sorted = np.array(x)
            y_sorted = np.array(y)
            p=np.argsort(x_sorted)
            x_sorted = x_sorted[p]
            y_sorted = y_sorted[p]

            # Plot in a loglog plot
            plt.figure(problem_num*len(plots)+j+1,[6.4*5/8, 4.8*5/8])
            plt.loglog(x_sorted,y_sorted,'--o',label=dir_to_legend[name])
            ax = plt.gca()

            # Label the axes and give a title
            plt.xlabel(plots[j][0])
            plt.ylabel(plots[j][1])


            # Set gridlines
            plt.grid(b=True, which='major')
            plt.grid(b=True, which='minor')

            # Set an appropriate number of tcks
            plt.setp(ax.xaxis.get_majorticklabels(), rotation = 45)
            plt.setp(ax.xaxis.get_minorticklabels(), rotation=45)



        # Shrink current axis by 20%
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width, box.height])

        # Find variable name
        var_name = plots[0][1]
        var_name = var_name.replace(' ','_')
        print(var_name)

        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        plt.savefig('../data/figures/p_'+str(problem_num)+'_'+trimmed_name+'_V_'+str(V)+'_i_'+str(I)+'_'+var_name+'.pgf', bbox_inches='tight',dpi=400)
        plt.savefig('../data/figures/p_'+str(problem_num)+'_'+trimmed_name+'_V_'+str(V)+'_i_'+str(I)+'_'+var_name+'.png', bbox_inches='tight',dpi=400)
